//
//  RipePredictionController.swift
//  AI Model Training
//
//  Created by offshore_mac_1 on 05/04/19.
//  Copyright © 2019 Offshore. All rights reserved.
//

import UIKit

var RipeProductArray = Array<[String:Any]>()

enum RipeHeadings : String {
	case plant = "plant"
	case ripe = "ripe"
	case unripe = "unripe"
}
var RipeHeadingsSelect : RipeHeadings?

var isPlant = Bool()
var isRipe = Bool()
var PlantScore = Float()
var RipeScore = Float()

class RipePredictionController: BaseController {
	
	var predictionImageName = String()
	
	@IBOutlet weak var resultImage: UIImageView!
	
	
	
	override func viewDidLoad() {
        super.viewDidLoad()
		resultImage.image = UIImage(named: predictionImageName)
//        ShowRipePredictionResult()
    }
	
	
	func ShowRipeResult () -> String {
		var RipeResult = RipeProductArray
		for i in 0..<RipeResult.count {
			if RipeResult[i]["name"] as! String == "plant" {
				PlantScore = (RipeResult[i]["score"] as! NSString).floatValue
				checkPrediction(RipeHeadings.plant)
			}
			else if RipeResult[i]["name"] as! String == "ripe" {
				RipeScore = (RipeResult[i]["score"] as! NSString).floatValue
				checkPrediction(RipeHeadings.ripe)
			}
			else if RipeResult[i]["name"] as! String == "unripe" {
				RipeScore = (RipeResult[i]["score"] as! NSString).floatValue
				checkPrediction(RipeHeadings.unripe)
			}
		}
		return CheckImageResult()
	}
	
	func checkPrediction (_ input: RipeHeadings) {
		
		switch input {
		case .plant:
			print("Plant")
			if PlantScore > 0.65 {
				isPlant = true
			}
			else {
				isPlant = false
			}
			break
			
		case .ripe:
			print("Ripe")
			if RipeScore > 0.80 {
				isRipe = true
			}
			else if RipeScore > 0.65 && RipeScore < 0.80 {
				isRipe = true
			}
			else {
				isRipe = false
				RipeScore = 1 - RipeScore
			}
			break
			
		case .unripe:
			print("Unripe")
			if RipeScore > 0.80 {
				isRipe = false
			}
			else if RipeScore > 0.65 && RipeScore < 0.80 {
				isRipe = false
			}
			else {
				isRipe = true
				RipeScore = 1 - RipeScore
			}
			break
			
			//		default:
			//			break
		}
		
	}
	
	func CheckImageResult () -> String {
		if isPlant {
			if isRipe {
				if RipeScore > 0.80 {
					print("Img Result = 100")
					return "Ripe-100"
					//img 100
				}
				else {
					print("Img Result = 70")
					return "Ripe-70"
					// img 70
				}
			}
			else {
				if RipeScore > 0.80 {
					print("Img Result = 20")
					return "Ripe-20"
					//img 20
				}
				else {
					print("Img Result = 40")
					return "Ripe-40"
					//img 40
				}
			}
		}
		else {
			self.showAlert(withTitle: "Alert!", message: "Try using a different image, it appears to our system that this is not a tomato!", okButton: "OK", cancelButton: "") { (alertResponse) in
				
			}
			return ""
		}
	}
	
	/*
	func ShowRipePredictionResult () {
		var RipeResult = RipeProductArray
		for i in 0..<RipeResult.count {
			if RipeResult[i]["name"] as! String == "plant" {
				PlantScore = (RipeResult[i]["score"] as! NSString).floatValue
				checkPrediction(RipeHeadings.plant)
			}
			else if RipeResult[i]["name"] as! String == "ripe" {
				RipeScore = (RipeResult[i]["score"] as! NSString).floatValue
				checkPrediction(RipeHeadings.ripe)
			}
			else if RipeResult[i]["name"] as! String == "unripe" {
				RipeScore = (RipeResult[i]["score"] as! NSString).floatValue
				checkPrediction(RipeHeadings.unripe)
			}
		}
		CheckImageResult()
	}
	
	func checkPrediction (_ input: RipeHeadings) {
		
		switch input {
		case .plant:
			print("Plant")
			if PlantScore > 0.65 {
				isPlant = true
			}
			else {
				isPlant = false
			}
			break
			
		case .ripe:
			print("Ripe")
			if RipeScore > 0.80 {
				isRipe = true
			}
			else if RipeScore > 0.65 && RipeScore < 0.80 {
				isRipe = true
			}
			else {
				isRipe = false
				RipeScore = 1 - RipeScore
			}
			break
			
		case .unripe:
			print("Unripe")
			if RipeScore > 0.80 {
				isRipe = false
			}
			else if RipeScore > 0.65 && RipeScore < 0.80 {
				isRipe = false
			}
			else {
				isRipe = true
				RipeScore = 1 - RipeScore
			}
			break
			
//		default:
//			break
		}
		
	}
	
	func CheckImageResult () {
		if isPlant {
			if isRipe {
				if RipeScore > 0.80 {
					print("Img Result = 100")
					resultImage.image = UIImage(named: "Ripe-100")
					//img 100
				}
				else {
					print("Img Result = 70")
					resultImage.image = UIImage(named: "Ripe-70")
					// img 70
				}
			}
			else {
				if RipeScore > 0.80 {
					print("Img Result = 20")
					resultImage.image = UIImage(named: "Ripe-20")
					//img 20
				}
				else {
					print("Img Result = 40")
					resultImage.image = UIImage(named: "Ripe-40")
					//img 40
				}
			}
		}
		else {
			self.showAlert(withTitle: "Alert!", message: "Try using a different image, it appears to our system that this is not a tomato!", okButton: "OK", cancelButton: "") { (alertResponse) in
				
			}
		}
	}*/
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
