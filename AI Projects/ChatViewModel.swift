//
//  ChatViewModel.swift
//  AI Projects
//
//  Created by Rajeev Lochan Ranga on 14/03/19.
//  Copyright © 2019 Rajeev Lochan Ranga. All rights reserved.
//

import Foundation
import SendBirdSDK

struct MessageDate {
    var date = "00"
    var month = "000"
    var year = "0000"
}

class OutgoingMessageModel: SBDBaseMessage {
    var message: String?
}

class ChatViewModel {
    var previousMessages: [SBDBaseMessage]?
    var channel: SBDGroupChannel?
    var user_id : String {
        get {
            let userId = UserDefaults.standard.value(forKey: Constants.UserId)
            return userId as! String
        }
    }
    var user: SBDUser?
    var agent: SBDUser?
    
    init() {
        self.loadMessages(onSuccess: { (prvMessages) in
            
        }) { (error) in
            
        }
        
    }
    
    func reconnect(onSuccess  success:@escaping(_ user: SBDUser) -> Void,onFailure  failure:@escaping(_ error: SBDError) -> Void) {
        SBDMain.connect(withUserId: self.user_id, accessToken: Constants.Sendbird_API_Token, completionHandler: { (user, error) in
            if let result = user {
                self.user = result
                print("success ")
                print("user \(String(describing: result))")
                success(result)
            }
            if let result = error {
                print("failure ")
                print("error \(String(describing: result))")
                failure(result)
            }
        })
    }
    
    func createChannel() {
        let userId =  UserDefaults.standard.value(forKey: Constants.UserId) as! String //"1e1e9df352b8fbf57809f8a30e1cc2b7"
        self.reconnect(onSuccess: { (usr) in
            
            SBDGroupChannel.createChannel(withUserIds: [userId, Constants.Agent_Id], isDistinct: true) { (supportChannel, error) in
                if supportChannel != nil {
                    self.channel = supportChannel
                    if let channel_url = supportChannel?.channelUrl {
                        UserDefaults.standard.set(channel_url, forKey: Constants.ChannelUrl)
                        UserDefaults.standard.synchronize()
                    }
                    self.getChannel(onSuccess: { (channel) in
                     
                    }, onFailure: { (error) in
                        
                    })
                    print("channel details url - \(String(describing: supportChannel))")
                }
            }
            
        }) { (err) in
            print("Error : - \(err) ")
        }

    }
    
  
    func getChannel(onSuccess  success:@escaping(_ channel: SBDGroupChannel?) -> Void, onFailure  failure:@escaping(_ error: SBDError) -> Void) {
        
        let ch_url = UserDefaults.standard.value(forKey: Constants.ChannelUrl) as! String
        SBDGroupChannel.getWithUrl(ch_url, completionHandler: { (supportChannel, error) in
            guard error == nil else {   // Error.
                failure(error!)
                return
            }
            if (supportChannel) != nil {
                self.channel = supportChannel
                success(supportChannel)
            }
            print("channel details - \(String(describing: supportChannel))")
        })
        
    }
    
    func getPreviousMessages(onSuccess  success:@escaping(_ previousMessage: [SBDBaseMessage]) -> Void, onFailure  failure:@escaping(_ error: SBDError) -> Void) {
    
        let previousMessageQuery = self.channel!.createPreviousMessageListQuery()
        previousMessageQuery?.loadPreviousMessages(withLimit: 30, reverse: true, completionHandler: { (messages, error) in
            guard error == nil else {   // Error.
                failure(error!)
                return
            }
            if (messages) != nil {
                self.previousMessages = messages?.reversed()
                success(self.previousMessages!)
            }
        })
    }
    func loadMessages(onSuccess  success:@escaping(_ prvMessage: [SBDBaseMessage]) -> Void, onFailure  failure:@escaping(_ error: SBDError) -> Void) {
        let userId =  UserDefaults.standard.value(forKey: Constants.UserId) as! String //"1e1e9df352b8fbf57809f8a30e1cc2b7"
        self.reconnect(onSuccess: { (usr) in
            
            SBDGroupChannel.createChannel(withUserIds: [userId, Constants.Agent_Id], isDistinct: true) { (supportChannel, error) in
                if supportChannel != nil {
                    self.channel = supportChannel
                    if let channel_url = supportChannel?.channelUrl {
                        UserDefaults.standard.set(channel_url, forKey: Constants.ChannelUrl)
                        UserDefaults.standard.synchronize()
                    }
                    self.getChannel(onSuccess: { (channel) in
                        self.getPreviousMessages(onSuccess: { (prevMessages) in
                            
                            success(self.previousMessages!)
                        }, onFailure: { (error) in
                            failure(error)
                        })
                    }, onFailure: { (error) in
                        
                    })
                    print("channel details url - \(String(describing: supportChannel))")
                }
            }
            
        }) { (err) in
            print("Error : - \(err) ")
        }
        
    }
    func sendMessage(message:String, onSuccess  success:@escaping(_ sentMessage: SBDUserMessage) -> Void, onFailure  failure:@escaping(_ error: SBDError) -> Void)  {
        
        DispatchQueue.main.async {
        self.channel?.sendUserMessage(message, completionHandler: { (resultMessage, error) in
                DispatchQueue.main.async {
                    if resultMessage != nil {
                        success(resultMessage!)
                        print("sent message: - \(String(describing: resultMessage))")
                    }
                    if error != nil {
                        failure(error!)
                        print("Error in sending message: - \(String(describing: error))")
                    }
                }
            })
        }
    }
    
    func sendAdminMessage(message:String, onSuccess  success:@escaping(_ sentMessage: Any) -> Void, onFailure  failure:@escaping(_ error: Any) -> Void)  {
        let ch_url = UserDefaults.standard.value(forKey: Constants.ChannelUrl) as! String

        NetworkHandler.sendAdminMessage(message: message, channel_url: ch_url, channel_type: "group_channels", onSuccess: { (resultMessage) in
            print("Result :- \(resultMessage)")
        }) { (error) in
            failure(error)
        }
        
    }
    
}
