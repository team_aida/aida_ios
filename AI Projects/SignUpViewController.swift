//
//  SignUpViewController.swift
//  AI Projects
//
//  Created by Rajeev Lochan Ranga on 15/02/19.
//  Copyright © 2019 Rajeev Lochan Ranga. All rights reserved.
//

import UIKit
import SendBirdSDK

class SignUpViewController: UIViewController {

    @IBOutlet weak var signUp_View: UIView!
    @IBOutlet weak var name_view: UIView!
    @IBOutlet weak var emailAddress_view: UIView!
    @IBOutlet weak var btnSignUp: UIButton!
    
    @IBOutlet weak var txtFld_email: UITextField!
    @IBOutlet weak var txtFld_name: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		 self.intializeUserInterface()
	}
    
    @IBAction func buttonSignUp_action(_ sender: UIButton) {
        
        if self.txtFld_name.text?.count == 0 || self.txtFld_email.text?.count == 0 {
            let alert = CommontMethods.showAlert(title: "", message: "Please fill the required fields", ok_button_title: "OK")
            self.present(alert, animated: true, completion: nil)
        }
        else if CommontMethods.isValidEmail(testStr: self.txtFld_email.text!) {
            self.view.endEditing(true)
			AppDelegate.shareInstance()?.showActivityIndicator()
			
            NetworkHandler.getSignUpRequest(name: self.txtFld_name.text!, emailId: self.txtFld_email.text!, success: { (successResponse) in
				AppDelegate.shareInstance()?.hideActivityIndicator()
                print(successResponse)
                if let result = successResponse as? [String:Any] {
                    if let user_id = result["id"] as? String  {
                        
                        UserDefaults.standard.setValue(self.txtFld_name.text, forKey: Constants.kName)
                        UserDefaults.standard.setValue(self.txtFld_name.text, forKey: Constants.kEmail)
                        UserDefaults.standard.set(user_id, forKey: Constants.UserId)
                        UserDefaults.standard.synchronize()
                        
                        SBDMain.connect(withUserId: user_id, accessToken: Constants.Sendbird_API_Token, completionHandler: { (user, error) in
                            
                            print("user \(String(describing: user))")
                            self.performSegue(withIdentifier: "NavigateToHomeScreen", sender: nil)

                        })
                    }
					else if (result["title"] as! String) == "Member Exists" {
						AppDelegate.shareInstance()?.showActivityIndicator()

						NetworkHandler.getMemberDetailsRequest(name: self.txtFld_name.text!, emailId: self.txtFld_email.text!, success: { (successResponse) in
							AppDelegate.shareInstance()?.hideActivityIndicator()
							print(successResponse)
							
							 if let result = successResponse as? [String:Any] {
								if let user_id = result["id"] as? String  {
									
									UserDefaults.standard.setValue(self.txtFld_name.text, forKey: Constants.kName)
									UserDefaults.standard.setValue(self.txtFld_name.text, forKey: Constants.kEmail)
									UserDefaults.standard.set(user_id, forKey: Constants.UserId)
									UserDefaults.standard.synchronize()
									
									SBDMain.connect(withUserId: user_id, accessToken: Constants.Sendbird_API_Token, completionHandler: { (user, error) in
										
										print("user \(String(describing: user))")
										self.performSegue(withIdentifier: "NavigateToHomeScreen", sender: nil)
										
									})
								}
							}
							
						}, failure: { (error) in

						})
					}
                }
            }) { (NetworkError) in
                AppDelegate.shareInstance()?.hideActivityIndicator()

                  print(NetworkError)
            }
        }
        else {
            let alert = CommontMethods.showAlert(title: "", message: "Enter valid email id", ok_button_title: "OK")
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    
    func intializeUserInterface() {
        
        self.signUp_View.layer.cornerRadius = 10
        self.signUp_View.layer.shadowColor = UIColor.lightGray.cgColor
        self.signUp_View.layer.shadowOpacity = 1
        self.signUp_View.layer.shadowOffset = CGSize.zero
        self.signUp_View.layer.shadowRadius = 5
        self.signUp_View.layer.shadowPath = UIBezierPath(rect: self.signUp_View.bounds).cgPath
       self.signUp_View.layer.shouldRasterize = true
        
        self.name_view.layer.cornerRadius = self.name_view.frame.size.height / 2
        
        self.name_view.layer.borderColor = #colorLiteral(red: 0.6136776805, green: 0.6141566038, blue: 0.6137518287, alpha: 1)
        self.name_view.layer.borderWidth = 1.0
        
        self.emailAddress_view.layer.borderColor = #colorLiteral(red: 0.6136776805, green: 0.6141566038, blue: 0.6137518287, alpha: 1)
        self.emailAddress_view.layer.borderWidth = 1.0
        
        self.emailAddress_view.layer.cornerRadius = self.emailAddress_view.frame.size.height / 2
        self.btnSignUp.layer.cornerRadius = self.btnSignUp.frame.size.height / 2
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SignUpViewController:UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        
    }
}
