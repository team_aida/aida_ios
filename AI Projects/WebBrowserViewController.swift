//
//  WebBrowserViewController.swift
//  AI Projects
//
//  Created by offshore_mac_1 on 26/02/19.
//  Copyright © 2019 Rajeev Lochan Ranga. All rights reserved.
//

import UIKit
import WebKit

class WebBrowserViewController: UIViewController {
    
    @IBOutlet weak var lbl_url: UILabel!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var progressView: UIProgressView!
    
    @IBOutlet weak var topLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnBack: UIButton!
    var url_str: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isPrivacyPolicy {
            self.lbl_url.isHidden = true
            self.url_str = Constants.URL_PrivacyPolicy
            self.topLayoutConstraint.constant = -32
           
            
            self.btnBack.setImage(#imageLiteral(resourceName: "menu_icon"), for: UIControl.State.normal)
            if revealViewController() != nil {
                self.btnBack.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: UIControl.Event.touchUpInside)
                view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            }
        }
        else {
            self.topLayoutConstraint.constant = 0
            self.lbl_url.isHidden = false
        }
        self.view.layoutIfNeeded()
        self.view.setNeedsLayout()
            
        self.loadWebView()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func buttonBack_action(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)

    }
    func loadWebView() {
        self.lbl_url.text = self.url_str
        self.webView.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil)
        let URL = NSURL(string: self.url_str)
        let urlRequest = NSURLRequest(url: URL! as URL)
        self.webView.load(urlRequest as URLRequest)
    }
    
    @IBAction func buttonDone_action(_ sender: UIButton) {
      
            self.dismiss(animated: true, completion: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if (keyPath == "estimatedProgress") {
            self.progressView.isHidden = webView.estimatedProgress == 1
            self.progressView.setProgress(Float(webView.estimatedProgress), animated: true)
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
