//
//  CameraController.swift
//  AR_AI_Demo
//
//  Created by offshore_mac_1 on 22/01/19.
//  Copyright © 2019 offshore_mac_1. All rights reserved.
//

import UIKit
import AVFoundation

class CameraController: BaseController, AVCapturePhotoCaptureDelegate {
	
	var binaryImageDataString = String()
	
	var usingFrontCamera = false
	
	var captureSession = AVCaptureSession()
	var sessionQueue: DispatchQueue!
	var captureDevice : AVCaptureDevice?
	var stillImageOutput : AVCapturePhotoOutput?
	var videoPreviewLayer : AVCaptureVideoPreviewLayer?
	
	var flashMode : AVCaptureDevice.FlashMode?
	
	var AnimationView: UIView!
	
	enum CameraControllerError: Swift.Error {
		case captureSessionAlreadyRunning
		case captureSessionIsMissing
		case inputsAreInvalid
		case invalidOperation
		case noCamerasAvailable
		case unknown
	}
	
	@IBOutlet weak var topBarView: UIView!
	@IBOutlet weak var previewView: UIView!
	@IBOutlet weak var captureImageView: UIImageView!
	
	@IBOutlet weak var rotateCameraButton: UIButton!
	@IBOutlet weak var captureButton: ActionButton!
	@IBOutlet weak var changeFlashButton: UIButton!
	
	
	//MARK:- View Did Load
	override func viewDidLoad() {
		super.viewDidLoad()
	}
	
	override func viewDidLayoutSubviews() {
		self.CornerRadius(captureButton, value: 0.5)
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		captureSession.stopRunning()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
//		setupCameraInput()
		setupCaptureSession()
		setupDevice()
		setupInput()
		setupPreviewLayer()
		startRunningCaptureSession()
	}
	
//	func showImageAnimation (_ input : Bool, topBar : UIView) {
//		print("Show Image Animation")
//
//		let oPoint = topBar.frame.origin.y + topBar.frame.size.height
//		AnimationView = UIView(frame: CGRect(x: 10, y: oPoint + 10, width: MAINSCREEN_WIDTH - 20, height: self.previewView.frame.size.height * 0.85 - oPoint))
//		AnimationView.backgroundColor = UIColor.clear
//		AnimationView.clipsToBounds = true
//
//		if input {
//			self.view.addSubview(AnimationView)
//			self.view.bringSubviewToFront(AnimationView)
//
//			AnimationView.twinkle()
//		}
//		else {
//			AnimationView.removeFromSuperview()
//		}
//
//	}
	
	func setupCaptureSession () {
//		if captureSession == nil {
//			captureSession = AVCaptureSession()
		captureSession.sessionPreset = .photo
//		}
		sessionQueue = DispatchQueue(label: "session queue")
	}
	
	func setupDevice () {
		sessionQueue.async {
//			self.captureDevice = self.usingFrontCamera ? self.getFrontCamera() : self.getBackCamera() else {
//			print("No Camera Access")
//			return
//			}
			
			let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: .video, position: .unspecified)
			let devices = deviceDiscoverySession.devices
			for device in devices {
				if self.usingFrontCamera && device.position == .front {
					self.captureDevice = device
				} else if device.position == .back {
					self.captureDevice = device
				}
			}
		}
		
	}
	
	func setupInput() {
		
		sessionQueue.async {
			do {
				let input = try AVCaptureDeviceInput(device: self.captureDevice!)
				self.stillImageOutput = AVCapturePhotoOutput()
				if #available(iOS 11.0, *) {
					self.stillImageOutput?.setPreparedPhotoSettingsArray([AVCapturePhotoSettings(format:[AVVideoCodecKey: AVVideoCodecType.jpeg])], completionHandler: nil)
				} else {
					// Fallback on earlier versions
				}
				if (self.captureSession.canAddInput(input)) && (self.captureSession.canAddOutput(self.stillImageOutput!)) {
					self.captureSession.addInput(input)
					self.captureSession.addOutput(self.stillImageOutput!)
//					self.setupPreviewLayer()
				}
			}
			catch let error  {
				print("Error Unable to initialize back camera:  \(error.localizedDescription)")
			}
		}
	}
	
	func getFrontCamera() -> AVCaptureDevice?{
		let videoDevices = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: .video, position: .unspecified)
		print("Get Front Camera")
		for device in videoDevices.devices {
			let device = device
			if device.position == .front {
				return device
			}
		}
		return nil
	}
	
	func getBackCamera() -> AVCaptureDevice{
		print("Get Back Camera")
		return AVCaptureDevice.default(for: AVMediaType.video)!
	}
	
	func setupPreviewLayer() {
		
		videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
		
		videoPreviewLayer!.videoGravity = .resizeAspectFill
		videoPreviewLayer!.connection?.videoOrientation = .portrait
		videoPreviewLayer!.frame = self.previewView.bounds
//		previewView.layer.addSublayer(videoPreviewLayer!)
		previewView.layer.insertSublayer(videoPreviewLayer!, at: 0)
		
		
//		DispatchQueue.global(qos: .userInitiated).async { //[weak self] in
//			self.captureSession?.startRunning()
//			DispatchQueue.main.async {
//				self.videoPreviewLayer!.frame = self.previewView.bounds
//			}
//		}
	}
	
	func startRunningCaptureSession() {
		captureSession.startRunning()
	}
	
	@IBAction func didTakePhoto(_ sender: UIButton) {
		
//		self.showImageAnimation(true)
		self.showImageAnimation(true, inputView: self.previewView, topBar: self.topBarView, animView: AnimationView)
		if #available(iOS 11.0, *) {
			let settings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
			stillImageOutput!.capturePhoto(with: settings, delegate: self)

		} else {
			// Fallback on earlier versions
		}
	}
	
	@IBAction func action_RotateCamera(_ sender: UIButton) {
		
		captureSession.beginConfiguration()
		
		if let inputs = captureSession.inputs as? [AVCaptureDeviceInput] {
			for input in inputs {
				captureSession.removeInput(input)
			}
		}
		
		usingFrontCamera = !usingFrontCamera
		print("Rotate Camera : ",usingFrontCamera)
		if usingFrontCamera {
			rotateCameraButton.setImage(#imageLiteral(resourceName: "camera-back"), for: .normal)
		}
		else {
			rotateCameraButton.setImage(#imageLiteral(resourceName: "camera-front"), for: .normal)
		}
		self.setupCaptureSession()
		setupDevice()
		setupInput()
		captureSession.commitConfiguration()
		startRunningCaptureSession()
	}
	
	@IBAction func action_ChangeFlashMode(_ sender: UIButton) {
		changeFlashValue()
	}
	
	func changeFlashValue () {
		if flashMode == .auto {
			flashMode = .on
			changeFlashButton.setImage(#imageLiteral(resourceName: "flash-on"), for: .normal)
		}
		else if flashMode == .on {
			flashMode = .off
			changeFlashButton.setImage(#imageLiteral(resourceName: "flash-off"), for: .normal)
		}
		else {
			flashMode = .auto
			changeFlashButton.setImage(#imageLiteral(resourceName: "flash-auto"), for: .normal)
		}
	}
	
	
	@available(iOS 11.0, *)
	func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
		
		guard let imageData = photo.fileDataRepresentation()
			else { return }
		
		let image = UIImage(data: imageData)
		captureImageView.image = image
		
//		binaryImageDataString = base64EncodeImage(image!)
//		self.performSegue(withIdentifier: "CameraToProductList", sender: nil)
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		
		if segue.identifier == "CameraToProductList" {
			let vc = segue.destination as? ProductListController
			vc?.imageBase64Str = binaryImageDataString
		}
	}
}
