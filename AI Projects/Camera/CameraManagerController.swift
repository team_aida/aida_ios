//
//  CameraManagerController.swift
//  AR_AI_Demo
//
//  Created by offshore_mac_1 on 21/02/19.
//  Copyright © 2019 offshore_mac_1. All rights reserved.
//

import UIKit
import CameraManager
import CropViewController
import CoreMotion

enum BackActionType {
    case Cross
    case Back
}

class CameraManagerController: BaseController, CropViewControllerDelegate {
    
    let cameraManager = CameraManager()
    var backAction: BackActionType?
    var binaryImageDataString = String()
    var ProductItems = Array<Any>()
    var LabelItems = Array<Any>()
    var TranslatedItems = Array<Any>()
    
    var AnimationView = UIView()
    var usingFrontCamera = false
    var usingFlashMode = CameraFlashMode.auto
    
    var multiObjectArr = [[String:Any]]()
    var predictionBtns = [UIButton]()
    
    var image = UIImage()
    var AccelerationDataProvider = [[String : String]]()
    var motionManager = CMMotionManager()
    var isSparkleView_Visible = false
    var isSurface_detected = false
    var isDetectionStarted = false
    
    @IBOutlet weak var topBarView: UIView!
    @IBOutlet weak var previewView: UIView!
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var Label_CameraViewTitle: UILabel!
    @IBOutlet weak var TrainModelButton: ActionButton!
    @IBOutlet weak var rotateCameraButton: UIButton!
    @IBOutlet weak var captureButton: ActionButton!
    @IBOutlet weak var changeFlashButton: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    
    var tapgesture = UITapGestureRecognizer()
    var sparkleView: RRSparklingEffectView?
    
    //MARK:- View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
   
        self.Label_CameraViewTitle.text = self.CameraScreenTitle()
        self.backAction = BackActionType.Back
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("OpenModelTraining"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.removePredictItemsButtons(notification:)), name: Notification.Name("RemovePredictItemsButtons"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.startObjectDetection(notification:)), name: Notification.Name("StartObjectDetection"), object: nil)
      
        cameraManager.shouldRespondToOrientationChanges = false
        cameraManager.addPreviewLayerToView(self.previewView)
        cameraManager.resumeCaptureSession()
        defaultCameraSetting()
        checkForScreentype()
        EnableButtons(input: true)
        
        
        self.detectmotion()
    }
    
    @objc func removePredictItemsButtons(notification: Notification) {
        self.removePredictButtons()
    }
    
  
    func removePredictButtons() {
        self.hideCapturedImage()
        for btn in self.predictionBtns {
            if self.view.subviews.contains(btn) {
                btn.removeFromSuperview()
            }
        }
    }
    @objc func methodOfReceivedNotification(notification: Notification) {
        
        self.PushViewController(toVC: "NewModelController", mine: self) //ModelListingController
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("OpenModelTraining"), object: nil)
        cameraManager.stopCaptureSession()
        EnableButtons(input: true)
        hideCapturedImage()
    }
    
    override func viewDidLayoutSubviews() {
        self.CornerRadius(captureButton, value: 0.5)
        self.CornerRadius(TrainModelButton, value: 0.5)
//        self.tapgesture = UITapGestureRecognizer(target: self, action: #selector(self.startDetection))
//        self.previewView.addGestureRecognizer(self.tapgesture)
    }
    
    func defaultCameraSetting () {
        cameraManager.cameraDevice = .back
        cameraManager.flashMode = usingFlashMode
        cameraManager.writeFilesToPhoneLibrary = false
    }
    
    func checkForScreentype () {
        switch SelectCameraMode {
        case .Prediction? :
            self.TrainModelButton.isHidden = true
            break
            
        case .AddImage? :
            self.TrainModelButton.isHidden = true
            break
            
        case .EditImage? :
            self.TrainModelButton.isHidden = true
            break
            
        case .FreshShopping? :
            self.TrainModelButton.isHidden = true
            break
            
        case .RipePrediction? :
            self.TrainModelButton.isHidden = true
            break
            
        case .Other?:
            self.TrainModelButton.isHidden = true
            break
            
        default:
            print("Nothing")
            break
        }
    }
    
    func EnableButtons (input : Bool) {
        DispatchQueue.main.async {
            for view in self.view.subviews {
                if view .isKind(of: UIButton.self) {
                    let btn = view as! UIButton
                    btn.isEnabled = input
                }
            }
        }
    }
    
    // MARK:- Sparfling Effect
    func showSparklingEffect() {
        
        let btnTrainModelFrame = self.TrainModelButton.frame
        let ref_frame = CGRect(x: 0, y:(btnTrainModelFrame.origin.y + btnTrainModelFrame.size.height), width: self.view.frame.size.width, height: changeFlashButton.frame.origin.y - (btnTrainModelFrame.origin.y + btnTrainModelFrame.size.height))
        self.sparkleView = RRSparklingEffectView(frame: ref_frame)
        self.sparkleView!.showSparklingEffect()
        self.view.addSubview(self.sparkleView!)
        self.view.bringSubviewToFront(self.sparkleView!)
        self.isSparkleView_Visible = true
    }
    
    func hideSparklingEffect() {
        self.isSparkleView_Visible = false
        
        self.sparkleView!.hideSparklingEffect()
    }
    
    //MARK:- Button Actions
    
    @IBAction func action_TrainModel(_ sender: Any) {
        
        self.PushViewController(toVC: "ModelListingController", mine: self)
    }
    
    @IBAction func action_RotateCamera(_ sender: UIButton) {
        print("Action Rotate Camera")
        usingFrontCamera = !usingFrontCamera
        cameraManager.cameraDevice = usingFrontCamera ? .front : .back
        if usingFrontCamera {
            rotateCameraButton.setImage(#imageLiteral(resourceName: "camera-back"), for: .normal)
        }
        else {
            rotateCameraButton.setImage(#imageLiteral(resourceName: "camera-front"), for: .normal)
        }
        
    }
    @objc func startDetection() {
        self.detectmotion()
    }
    @objc func startObjectDetection(notification: Notification) {
        self.detectmotion()
    }
    //MARK: - MotionManager methods
    
    func detectmotion() {
        //Getting the accelerometer data
        self.motionManager.accelerometerUpdateInterval = 0.1
        if motionManager.isAccelerometerAvailable {
            let queue = OperationQueue()
            motionManager.startAccelerometerUpdates(to: queue, withHandler:
                {data, error in
                    
                    DispatchQueue.main.async {
                        guard let data = data else {
                            return
                        }
                        
                     //   print("Error \(error)")
                        let X = (String(format: "%0.1f", data.acceleration.x))
                        let Y = (String(format: "%0.1f", data.acceleration.y))
                        let Z = (String(format: "%0.1f", data.acceleration.z))
                        
               //         print("Acceleration X = \(X)")
              //          print("Acceleration Y = \(Y)")
              //          print("Acceleration Z = \(Z)")
                        
                        
                        if SelectCameraMode == CameraMode.Prediction  && self.isDetectionStarted && self.isSurface_detected == false {
                            if self.isSparkleView_Visible == false {
                                self.showSparklingEffect()
                                print("Added Sparkling effect")
                            }
                        }

                        if self.AccelerationDataProvider.count == 0  {
                            self.AccelerationDataProvider = [[String : String]]()
                        }
                        
                        if self.AccelerationDataProvider.count < 10 {
                            self.AccelerationDataProvider.append(["X":X,"Y":Y,"Z":Z])
                        }
                        else {
                            if self.checkForCamStability() {
                                
                                self.captureImage()
                            }
                            else {
                                if SelectCameraMode == CameraMode.Prediction {
                                    if self.isSparkleView_Visible {
                                        print("hide Sparkling effect")
                                        self.hideSparklingEffect()
                                    }
                                }
                            }
                            self.AccelerationDataProvider.removeAll()
                        }
                        
                    }
            }
            )
        } else {
            print("Accelerometer is not available")
        }
    }
    
    func checkForCamStability() -> Bool {
        var result = false
        var matchCount = 0
        
        
        let obj1 = self.AccelerationDataProvider[0]
        for index in 1..<self.AccelerationDataProvider.count {
            let obj2 = self.AccelerationDataProvider[index]
            if obj1 == obj2 {
                matchCount =  matchCount + 1
                if matchCount >= 2 {
                    self.isDetectionStarted = true
                    if SelectCameraMode == CameraMode.Prediction  && self.isDetectionStarted && self.isSurface_detected == false {
                        if self.isSparkleView_Visible == false {
                            self.showSparklingEffect()
                            print("Added Sparkling effect")
                        }
                    }
                }
            }
        }
        print("MatchCount=  \(matchCount) \t \t AccelerationDataProvider Count =  \(self.AccelerationDataProvider.count - 1)")
        if matchCount == self.AccelerationDataProvider.count - 1 {
            if (obj1["X"] == "0.0" || obj1["X"] == "-0.0") && (obj1["Y"] == "0.0" || obj1["Y"] == "-0.0") && (obj1["Z"] == "1.0" || obj1["Z"] == "-1.0") {
                result = false
                self.isSurface_detected  = true
            }
            else {
                self.isSurface_detected  = false
                result = true
                self.motionManager.stopAccelerometerUpdates()
                print("Motion manager stopped")
            }
        }
        else {
            self.isSurface_detected  = false
            result = false
        }
        print("checkForCamStability = \(result)")
        print("======================================================================")
        
        return result
    }
    
    func captureImage() {
        print("Action Capture Image")
        
        EnableButtons(input: false)
        
        cameraManager.capturePictureWithCompletion { (image, error) in
            
            if SelectCameraMode == CameraMode.Other || SelectCameraMode == CameraMode.FreshShopping {
                
                self.showSparklingEffect()
            }
            
            if SelectCameraMode == CameraMode.Other || SelectCameraMode == CameraMode.FreshShopping {
                self.binaryImageDataString = self.base64EncodeImage(image!)
                self.LoadData()
            }
            else if SelectCameraMode == CameraMode.Prediction {
                self.showCapturedImage(capturedImage: image!)
                self.getPredictions(forImage: image!)
            }
            else {
                let cropViewController = CropViewController(image: image!)
                cropViewController.delegate = self
                self.present(cropViewController, animated: true, completion: nil)
            }
            
        }
    }
    
    
    @IBAction func action_captureImage(_ sender: UIButton) {
        print("Action Capture Image")
        
        EnableButtons(input: false)
        
        cameraManager.capturePictureWithCompletion { (image, error) in
            
            if SelectCameraMode == CameraMode.Other || SelectCameraMode == CameraMode.FreshShopping {
                
                self.showSparklingEffect()
            }
            
            if SelectCameraMode == CameraMode.Other || SelectCameraMode == CameraMode.FreshShopping {
                self.binaryImageDataString = self.base64EncodeImage(image!)
                self.LoadData()
            }
            else if SelectCameraMode == CameraMode.Prediction {
                self.showCapturedImage(capturedImage: image!)
                self.getPredictions(forImage: image!)
            }
            else {
                let cropViewController = CropViewController(image: image!)
                cropViewController.delegate = self
                self.present(cropViewController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction override func backButtonPressed () {
        if self.backAction == BackActionType.Cross {
            self.removePredictButtons()
            self.detectmotion()
        }
        else {
            GoBack(true)
        }
    }

    func showCapturedImage(capturedImage image:UIImage) {
        self.backAction = BackActionType.Cross
        self.btnBack.setImage(#imageLiteral(resourceName: "btn_cross"), for: .normal)
        self.imgView.isHidden = false
        self.imgView.image = image
    }
    
    func hideCapturedImage() {
        self.imgView.isHidden = true
        self.backAction = BackActionType.Back
        self.btnBack.setImage(#imageLiteral(resourceName: "back_icon"), for: .normal)
    }
    
    func getPredictions(forImage image: UIImage) {

        self.MethodSelection(img: image) { (response, Message, Result) in
            self.StopAnimation()
            if response {
                if SelectCameraMode == .Prediction {
                    self.multiObjectArr = Result
                    // self.plotPoints(inputDetails: Result)
                    self.showPopupPredictModel(data:  self.multiObjectArr)
                }
                else {
                    self.PopViewController(from: self)
                }
            }
            else {
                if SelectCameraMode == .Prediction {
                    if self.isSparkleView_Visible {
                        print("hide Sparkling effect")
                        self.hideSparklingEffect()
                    }
                    var alertmessage = String()
                    var okButton = String()
                    var cancelButton = String()
                    
                    if Result[0]["Type"] as! String == "202" {
                        alertmessage = Message
                        okButton = ""
                        cancelButton = "OK"
                    }
                    else {
                        alertmessage = Message //"No similar object found, do you want to create your own object!"
                        okButton = "OK"
                        cancelButton = ""
                    }
                    
                    self.showAlert(withTitle: "Alert!", message: alertmessage, okButton: okButton, cancelButton: cancelButton, completion: { (response) in
                        self.removePredictButtons()
                       
                        if response {
                        //    self.PushViewController(toVC: "NewModelController", mine: self)
                            self.detectmotion()
                        }
                        else {
                            self.EnableButtons(input: true)
                        }
                    })
                }
            }
        }
    }
    
    func plotPoints(inputDetails:  [[String: Any]]) {
        
        
        for indx in 0..<inputDetails.count {
            let item = inputDetails[indx]
            let predictItem = PredictionItem(dictionary: item)
            let btn  = self.createButton(atPoint: predictItem.getCentrePoint(), forPredictItemIndex: indx)
            if self.predictionBtns.contains(btn) ==  false {
                self.predictionBtns.append(btn)
            }
            self.view.addSubview(btn)
        }
        
    }
    
    func getButtonColor() -> UIColor {
        let colorsArr = [#colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1),#colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1),#colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1),#colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1),#colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1),#colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1),#colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1),#colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)]
        let indx = Int.random(in: 0..<colorsArr.count)
        let btnColor = colorsArr[indx]
        btnColor.withAlphaComponent(0.5)
        return btnColor
    }
    func createButton(atPoint centerPoint:CGPoint, forPredictItemIndex itemIndex:Int) -> UIButton {
        let btn = UIButton.init(type: .custom)
        btn.backgroundColor = self.getButtonColor()
        btn.frame = CGRect(x: 0, y: 0, width: 35, height: 35)
        btn.center = centerPoint
        btn.layer.cornerRadius = btn.frame.size.height/2
        btn.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        btn.layer.borderWidth = 2.0
        btn.tag = itemIndex
        btn.addTarget(self, action: #selector(selectPredictionItem_action(_:)), for: .touchUpInside)
        
        return btn
    }
    
    @objc func selectPredictionItem_action(_ sender:UIButton) {
        //  let predict_Item = PredictionItem(dictionary: self.multiObjectArr[sender.tag])
        self.showPopupPredictModel(data: self.multiObjectArr)
        
    }
    
    
    @IBAction func action_ChangeFlashMode(_ sender: UIButton) {
        print("Action Change Flash Mode")
        if usingFlashMode == .auto {
            usingFlashMode = .on
            changeFlashButton.setImage(#imageLiteral(resourceName: "flash-on"), for: .normal)
        }
        else if usingFlashMode == .on {
            usingFlashMode = .off
            changeFlashButton.setImage(#imageLiteral(resourceName: "flash-off"), for: .normal)
        }
        else {
            usingFlashMode = .auto
            changeFlashButton.setImage(#imageLiteral(resourceName: "flash-auto"), for: .normal)
        }
    }
    
    //MARK:- Cloud vision Request Method
    func LoadData () {
        
        Webservices.shared.CloudVisionRequest_POST(imageBase64: binaryImageDataString) { (responseStatus, responseData) in
            
            if responseStatus {
                
                self.AnalyzeCloudVisionResults(responseData, completion: { (status, message, resultData) in
                    print("Vision Result Status : ", status)
                    
                    if status {
                        
                        if resultData["labelAnnotations"].count != 0 {
                            print("Label Annotations : ", resultData["labelAnnotations"])
                            
                            self.AnalyzeLabelAnnotations(response: resultData, completion: { (responseStatus, responseResult, responseArray) in
                                
                                if responseStatus {
                                    
                                    print("Analyze Label Annotations - Labels : ", responseResult)
                                    
                                    Webservices.shared.ThuisProductSearch(products: responseArray.joined(separator: ","), page_offset: 0, language: USDF.value(forKey: "SelectedLanguage") as! String, completion: { (requestStatus, requestResponse) in
                                        
                                        print("Request - Status : \(requestStatus) Response : \(requestResponse)")
                                        if requestStatus {
                                            
                                            print("Request POST Response Result : ", requestResponse)
                                            // hide animation
                                            self.ProductItems = self.getProductDetail(requestResponse["Data"] as! [Any], ThuisSearch: true)//requestResponse["Data"] as! [Any]
                                            self.LabelItems = (requestResponse["Labels"] as! String).components(separatedBy: ",")
                                            self.TranslatedItems = (requestResponse["Translate"] as! String).components(separatedBy: ", ")
                                            print("Item Array : ", self.LabelItems)
                                            self.SegueToProduct()
                                        }
                                        else {
                                            if SelectCameraMode == CameraMode.Other {
                                                
                                                Webservices.shared.GoogleCustomSearch(paramString: responseArray.joined(separator: ","), completion: { (searchStatus, searchResponse) in
                                                    if searchStatus {
                                                        
                                                        self.ProductItems = self.getProductDetail(searchResponse, ThuisSearch: false)
                                                        self.LabelItems = responseArray//(requestResponse["Labels"] as! String).components(separatedBy: ",")
                                                        if requestResponse["Status"] as! Int != 99 {
                                                            self.TranslatedItems = (requestResponse["Translate"] as! String).components(separatedBy: ", ")
                                                        }
                                                        else {
                                                            self.TranslatedItems = []
                                                        }
                                                        //													print("Item Array : ", self.LabelItems)
                                                        self.SegueToProduct()
                                                    }
                                                })
                                            }
                                            else {
                                                if requestResponse["Status"] as! Int == 99 {
                                                    //self.showImageAnimation(false, inputView: self.previewView, topBar: self.topBarView, animView: self.AnimationView)
                                                    self.hideSparklingEffect()
                                                    self.EnableButtons(input: true)
                                                    self.showAlert(withTitle: "Alert!", message: "Host server connection error. Please try after sometime", okButton: "OK", cancelButton: "", completion: { (alertResponse) in
                                                        
                                                    })
                                                }
                                                else if requestResponse["Status"] as! Int == 50 {
                                                    //self.showImageAnimation(false, inputView: self.previewView, topBar: self.topBarView, animView: self.AnimationView)
                                                    self.hideSparklingEffect()
                                                    
                                                    self.EnableButtons(input: true)
                                                    self.showAlert(withTitle: "Alert!", message: "There seems to be some problem with the received result. Please try again", okButton: "OK", cancelButton: "", completion: { (alertResponse) in
                                                        
                                                    })
                                                }
                                            }
                                            
                                        }
                                    })
                                }
                            })
                        }
                    }
                    else {
                        print("Cloud Vision Result Message : ", message)
                    }
                })
            }
        }
    }
    
    func SegueToProduct () {
        //	self.showImageAnimation(false, inputView: self.previewView, topBar: self.topBarView, animView: self.AnimationView)
        self.hideSparklingEffect()
        self.performSegue(withIdentifier: "CameraManagerToProductList", sender: nil)
    }
    
    
    //MARK:- Object Training Methods
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        
        //self.StartAnimation(msg: "Uploading image")
        self.showSparklingEffect()
        
        self.MethodSelection(img: image) { (response, Message, Result) in
            self.StopAnimation()
            if response {
                cropViewController.dismiss(animated: true, completion: nil)
                if SelectCameraMode == .Prediction {
                    self.showPopupPredictModel(data: Result)
                }
                else if SelectCameraMode == .RipePrediction {
                    RipeProductArray = Result
                    self.ShowRipePredictionResult()
                }
                else if SelectCameraMode == .AddImage {
                    if GoingNewModel {
                        self.PushViewController(toVC: "ModelImageListController", mine: self)
                    }
                    else {
                        self.PopViewController(from: self)
                    }
                    
                }
                else {
                    self.PopViewController(from: self)
                }
            }
            else {
                if SelectCameraMode == .Prediction {
                    cropViewController.dismiss(animated: true, completion: nil)
                    var alertmessage = String()
                    if Result[0]["Type"] as! String == "202" {
                        alertmessage = Message
                    }
                    else {
                        alertmessage = "No similar object found, do you want to create your own object!"
                    }
                    
                    self.showAlert(withTitle: "Alert!", message: alertmessage, okButton: "YES", cancelButton: "NOT NOW", completion: { (response) in
                        self.removePredictButtons()
                        if response {
                            self.PushViewController(toVC: "NewModelController", mine: self)
                        }
                        else {
                            self.EnableButtons(input: true)
                        }
                    })
                }
            }
        }
    }
    
    //MARK:- Method Selection
    func MethodSelection (img: UIImage, completion: @escaping(_ status: Bool,_ message : String,_ result: [[String:Any]]) -> Void) {
        switch SelectCameraMode {
        case .Prediction? :
            self.ModelPredictMethod(image: img) { (response, Message, Result) in
                response ? completion(true, Message, Result) : completion(false, Message, Result)
            }
            break
            
        case .AddImage? :
            self.StartAnimation(msg: "Uploading image")
            self.AddImageMethod(image: img) { (response) in
                response ? completion(true, "", [[:]]) : completion(false, "", [[:]])
            }
            break
            
        case .EditImage? :
            self.StartAnimation(msg: "Uploading image")
            self.EditImageMethod(image: img) { (response) in
                response ? completion(true, "", [[:]]) : completion(false, "", [[:]])
            }
            break
            
        case .RipePrediction? :
            self.ModelRipePredictMethod(image: img) { (response, Result) in
                response ? completion(true, "", Result) : completion(false, "", [[:]])
            }
            break
            
        case .FreshShopping? :
            break
            
        default:
            print("Nothing")
            completion(false, "", [[:]])
            break
        }
    }
    
    //MARK:- Camera Modes Methods
    func ModelPredictMethod (image: UIImage, completion:@escaping(_ status: Bool,_ message : String,_ result: [[String:Any]]) -> Void) {
        Webservices.shared.ModelPredict(location: MultiObjectPredic_URL, imageFile: image, completion: { (status, Message, Result) in
            self.hideSparklingEffect()
            if status == 1 {
                
                completion(true, Message, Result)
            }
            else if status == 2 {
                completion(false, Message, [["Type":"202"]])
            }
            else {
                completion(false, Message, [["Type":"20"]])
                
            }
        })
    }
    
    func ModelRipePredictMethod (image: UIImage, completion:@escaping(_ status: Bool,_ result: [[String:Any]]) -> Void) {
        Webservices.shared.ModelRipePredict(imageFile: image, completion: { (status, Result) in
            //			self.showImageAnimation(false, inputView: self.previewView, topBar: self.topBarView, animView: self.AnimationView)
            self.hideSparklingEffect()
            if status == 1 {
                
                completion(true, Result)
            }
            else {
                completion(false, [[:]])
                
            }
        })
    }
    
    func AddImageMethod (image: UIImage, completion:@escaping(_ status: Bool) -> Void) {
        
        Webservices.shared.ModelAddImage(ImageName: "Image", productId: "\(Product?.p_id ?? 0)", imageFile: image) { (Status, Result) in
            
            if Status == 1 {
                completion(true)
            }
            else {
                completion(false)
                //				print("Model Add Image Status = 0 <Non 200 response from API>")
            }
        }
    }
    
    func EditImageMethod (image : UIImage, completion:@escaping(_ status: Bool) -> Void) {
        
        Webservices.shared.ModelEditImage(ImageName: "Image", imageId: "\(ProductImage?.img_id ?? 0)", imageFile: image) { (Status, Result) in
            
            if Status == 1 {
                completion(true)
            }
            else {
                completion(false)
                //				print("Model Edit Image Status = 0 <Non 200 response from API>")
            }
        }
    }
    
    
    func showPopupPredictModel (data : [[String : Any]]) {
        
        EnableButtons(input: true)
        
        let me = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PredictModelController") as! PredictModelController
        me.ModelPredictData = data
        me.modalPresentationStyle = .overCurrentContext
        me.modalTransitionStyle = .coverVertical
        
        self.present(me, animated: true, completion: nil)
    }
    
    func ShowRipePredictionResult () {
        EnableButtons(input: true)
        if RipePredictionController().ShowRipeResult() != "" {
            let me = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RipePredictionController") as! RipePredictionController
            me.predictionImageName = RipePredictionController().ShowRipeResult()
            self.navigationController?.pushViewController(me, animated: true)
        }
    }
    
    //MARK: - Ripe Prediction Checking Methods
    
    //	func ShowRipeResult () -> String {
    //		var RipeResult = RipeProductArray
    //		for i in 0..<RipeResult.count {
    //			if RipeResult[i]["name"] as! String == "plant" {
    //				PlantScore = (RipeResult[i]["score"] as! NSString).floatValue
    //				checkPrediction(RipeHeadings.plant)
    //			}
    //			else if RipeResult[i]["name"] as! String == "ripe" {
    //				RipeScore = (RipeResult[i]["score"] as! NSString).floatValue
    //				checkPrediction(RipeHeadings.ripe)
    //			}
    //			else if RipeResult[i]["name"] as! String == "unripe" {
    //				RipeScore = (RipeResult[i]["score"] as! NSString).floatValue
    //				checkPrediction(RipeHeadings.unripe)
    //			}
    //		}
    //		return CheckImageResult()
    //	}
    //
    //	func checkPrediction (_ input: RipeHeadings) {
    //
    //		switch input {
    //		case .plant:
    //			print("Plant")
    //			if PlantScore > 0.65 {
    //				isPlant = true
    //			}
    //			else {
    //				isPlant = false
    //			}
    //			break
    //
    //		case .ripe:
    //			print("Ripe")
    //			if RipeScore > 0.80 {
    //				isRipe = true
    //			}
    //			else if RipeScore > 0.65 && RipeScore < 0.80 {
    //				isRipe = true
    //			}
    //			else {
    //				isRipe = false
    //				RipeScore = 1 - RipeScore
    //			}
    //			break
    //
    //		case .unripe:
    //			print("Unripe")
    //			if RipeScore > 0.80 {
    //				isRipe = false
    //			}
    //			else if RipeScore > 0.65 && RipeScore < 0.80 {
    //				isRipe = false
    //			}
    //			else {
    //				isRipe = true
    //				RipeScore = 1 - RipeScore
    //			}
    //			break
    //
    //			//		default:
    //			//			break
    //		}
    //
    //	}
    //
    //	func CheckImageResult () -> String {
    //		if isPlant {
    //			if isRipe {
    //				if RipeScore > 0.80 {
    //					print("Img Result = 100")
    //					return "Ripe-100"
    //					//img 100
    //				}
    //				else {
    //					print("Img Result = 70")
    //					return "Ripe-70"
    //					// img 70
    //				}
    //			}
    //			else {
    //				if RipeScore > 0.80 {
    //					print("Img Result = 20")
    //					return "Ripe-20"
    //					//img 20
    //				}
    //				else {
    //					print("Img Result = 40")
    //					return "Ripe-40"
    //					//img 40
    //				}
    //			}
    //		}
    //		else {
    //			self.showAlert(withTitle: "Alert!", message: "Try using a different image, it appears to our system that this is not a tomato!", okButton: "OK", cancelButton: "") { (alertResponse) in
    //
    //			}
    //			return ""
    //		}
    //	}
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "CameraManagerToProductList" {
            let vc = segue.destination as? ProductListController
            vc?.imageBase64Str = binaryImageDataString
            vc?.ProductListArray = ProductItems
            vc?.LabelItemArray = LabelItems
            vc?.TranslatedItemArray = TranslatedItems
        }
    }
    
    
}
