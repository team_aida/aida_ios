//
//  SourceMessageTableViewCell.swift
//  AI Projects
//
//  Created by offshore_mac_1 on 01/03/19.
//  Copyright © 2019 Rajeev Lochan Ranga. All rights reserved.
//

import UIKit

class SourceMessageTableViewCell: UITableViewCell {
	@IBOutlet weak var lbl_languageName: UILabel!
	@IBOutlet weak var lbl_message: UILabel!
	@IBOutlet weak var view_message: UIView!
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
