//
//  TranslationMessage.swift
//  AI Projects
//
//  Created by offshore_mac_1 on 01/03/19.
//  Copyright © 2019 Rajeev Lochan Ranga. All rights reserved.
//

import Foundation

enum TranslationLanguageType {
	case Source
	case Target
}

class TranslationMessage {
    
	var message :String
	var translationLanguageType : TranslationLanguageType?
	var targetLanguage :String
	var sourceLanguage :String
	
	init(message:String?, translationLanguageType: TranslationLanguageType?,targetLanguage:String?, sourceLanguage:String?) {
		
		self.message = message!
		self.translationLanguageType = translationLanguageType
		self.targetLanguage = targetLanguage!
		self.sourceLanguage =  sourceLanguage!
	}
}
