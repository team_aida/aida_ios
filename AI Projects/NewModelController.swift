//
//  NewModelController.swift
//  AI Model Training
//
//  Created by Rajeev Lochan Ranga on 27/02/19.
//  Copyright © 2019 Offshore. All rights reserved.
//

import UIKit

class NewModelController: BaseController {
    
	@IBOutlet weak var ScreenHeading: UILabel!
	
	@IBOutlet weak var modelScrollView: UIScrollView!
	@IBOutlet weak var nextView: UIView!
	
	@IBOutlet weak var text_Name: TextField!
	@IBOutlet weak var text_URL: TextField!
	@IBOutlet weak var text_Description: TextView!
	@IBOutlet weak var text_Manual: TextView!
	
	@IBOutlet weak var button_AttachFile: UIButton!
	@IBOutlet weak var button_Next: UIButton!
	
	
	//MARK:- View Will Appear
	override func viewWillAppear(_ animated: Bool) {
		RoundOffViews()
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		var contentRect = CGRect.zero
		PDFFileURL = ""
		
		for view in modelScrollView.subviews {
			contentRect = contentRect.union(view.frame)
		}
		modelScrollView.contentSize = contentRect.size
		
		switch ModelScreenMode {
		case .AddModel?:
			ScreenHeading.text = "Create New Model"
			break
			
		case .EditModel?:
			ScreenHeading.text = "Edit Model"
			LoadDataForEditing()
			break
			
		default:
			break
		}
		
    }
	
	func LoadDataForEditing () {
		
		self.text_Name.text = Product?.setupDisplayDetail().0["name"] as? String
		self.text_URL.text = Product?.setupDisplayDetail().0["website_url"] as? String
		self.text_Description.text = Product?.setupDisplayDetail().0["description"] as? String
		self.text_Manual.text = Product?.setupDisplayDetail().0["manual"] as? String
	}
	
	override func viewDidLayoutSubviews() {
		
	}
	
	func checkEmptyValidation () -> Bool {
		
		if text_Name.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
			self.showAlert(withTitle: "Alert!", message: "Please enter model name", okButton: "Ok", cancelButton: "") { (response) in
				
			}
			return false
		}
		else if (text_URL.text?.count != 0) {
			if !(text_URL.text?.isValidURL)! {
				self.showAlert(withTitle: "Alert!", message: "Please enter valid reference URL", okButton: "Ok", cancelButton: "") { (response) in
					
				}
				return false
			}
			return true
		}
		else {
			return true
		}
	}
	
	func RoundOffViews () {
//		for (index, view) in modelScrollView.subviews.enumerated() {
//			print("View : ", index)
//			if view .isKind(of: UIView.self) {
//				for (index, txt) in view.subviews.enumerated() {
//					print("Txt : ", index)
//					if txt .isKind(of: UITextField.self) {
//						print("Text Fields Found")
//					}
//					if txt .isKind(of: UITextView.self) {
//						print("Text Views Found")
//					}
//				}
//
//			}
//		}
		
		self.CornerRadius(text_Name, value: 0.5)
		self.CornerRadius(text_URL, value: 0.5)
		self.CornerRadius(text_Description, value: 0.1)
		self.CornerRadius(text_Manual, value: 0.1)
		
		self.CornerRadius(button_AttachFile, value: 0.5)
		self.CornerRadius(button_Next, value: 0.5)
	}
	
	@IBAction func action_AttachFile (_ sender : UIButton) {
		self.openDocumentPicker()
	}
	
	@IBAction func action_Next (_ sender : UIButton) {
		
		if checkEmptyValidation() {
			print("Go to Webservice")
			
			
			switch ModelScreenMode {
			case .AddModel?:
				self.StartAnimation(msg: "Creating Model")
				Webservices.shared.ModelAddProductWithPDF(name: self.text_Name.text!, manual: self.text_Manual.text!, description: self.text_Description.text!, website_url: self.text_URL.text!, pdfFile: PDFFileURL) { (Status, Result) in
					
					self.StopAnimation()
					if Status == 1 {
						print("Model Add Product : ", Result)
                        Product = ProductDetail.init(dictionary: Result)
                        SelectCameraMode = CameraMode.AddImage
                        self.PushViewController(toVC: "CameraManagerController", mine: self)
//                        self.PopMultipleViewControllers(2, from: self)
                        
					}
				}
				break
				
			case .EditModel?:
				self.StartAnimation(msg: "Updating Model")
				Webservices.shared.ModelEditProductWithPDF(name: self.text_Name.text!, manual: self.text_Manual.text!, description: self.text_Description.text!, id: "\(Product?.p_id ?? 0)", website_url: self.text_URL.text!, pdfFile: PDFFileURL) { (Status, Result) in
					
					self.StopAnimation()
					if Status == 1 {
						print("Model Edit Product : ", Result)
						self.PopMultipleViewControllers(3, from: self)
					}
				}
				break
				
			default:
				break
			}
		}
		else {
			print("Empty Fields")
		}
	}

}

extension NewModelController: UITextFieldDelegate, UITextViewDelegate {
	
	func textFieldDidBeginEditing(_ textField: UITextField) {
		if textField == text_URL {
			if textField.text == "" {
				textField.text = "https://"
			}
		}
	}
	
	func textFieldDidEndEditing(_ textField: UITextField) {
		if textField == text_Name {
			
		}
		if textField == text_URL {
			if textField.text == "https://" || textField.text == "" {
				textField.text = ""
				self.RemoveKeyboardResponse()
			}
			else if !(textField.text?.isValidURL)! {
				self.showAlert(withTitle: "Alert!", message: "This is not valid URL", okButton: "OK", cancelButton: "") { (response) in
					if response {
						
					}
				}
			}
		}
	}
	
	func textViewDidEndEditing(_ textView: UITextView) {
		if textView == text_Manual {
			
		}
		if textView == text_Description {
			
		}
	}
	
}
