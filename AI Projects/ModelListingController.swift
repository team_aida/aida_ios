//
//  ModelListingController.swift
//  AI Model Training
//
//  Created by Rajeev Lochan Ranga on 27/02/19.
//  Copyright © 2019 Offshore. All rights reserved.
//

import UIKit

class ModelListingController: BaseController {
    
    var ModelListArray = Array<Any>()
    @IBOutlet weak var ModelListingCollection: UICollectionView!
    @IBOutlet weak var addModelButton: UIButton!
    
    @IBOutlet weak var btnStartTraining: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
		
		self.ModelListingCollection.register(UINib(nibName: "ModelListCell", bundle: nil), forCellWithReuseIdentifier: "ModelListCell")
        self.btnStartTraining.layer.cornerRadius = self.btnStartTraining.frame.size.height / 2
    }
	
	override func viewWillAppear(_ animated: Bool) {
		SelectCameraMode = CameraMode.Prediction
		Product?.clearProductDetails()
		callListFromWebsite()
        if self.checkIfTrainingIsInProgress() {
            self.btnStartTraining.isEnabled = false
            self.btnStartTraining.setTitle("Training is in progress", for: .normal)
        }
        else {
            self.btnStartTraining.isEnabled = true
            self.btnStartTraining.setTitle("Start Training", for: .normal)
        }
        
	}
	
	override func viewDidLayoutSubviews() {
		self.CornerRadius(self.addModelButton, value: 0.5)
		self.ModelListingCollection.reloadData()
	}
    
    func callListFromWebsite () {
        self.StartAnimation(msg: "Getting list of existing models...")
        Webservices.shared.ModelProductList(limit: 10, offset: 0) { (status, Response) in
            self.StopAnimation()
			if status == 1 {
				self.ModelListArray = Response
//				for i in Response {
//					self.ModelListArray.insert(<#T##newElement: Any##Any#>, at: <#T##Int#>)
//				}
				
				self.ModelListingCollection.reloadData()
			}
        }
    }
    
    func checkIfTrainingIsInProgress() -> Bool {
        var result = false
        
        if let trainingTimeStamp_str = UserDefaults.standard.value(forKey: "TrainingTimeStamp") as? String {
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let trainingTimeStamp = format.date(from: trainingTimeStamp_str)
        let currentTimeStamp = Date()
        let elapsedTime = currentTimeStamp.minutes(from: trainingTimeStamp!)
        
        result = (elapsedTime < 40) ? true : false
        }
        else {
            result = false
        }
        return result
    }
    
    @IBAction func buttonStartTraining_action(_ sender: UIButton) {
        
        Webservices.shared.Alamofire_POST(Location: SaveModelForStartTraining_URL, param: [:]) { (status, Message, Response) in
            if status == 0 {
                self.view.makeToast("Something went wrong at server side. Start training Again")
                self.btnStartTraining.isEnabled = true
                self.btnStartTraining.setTitle("Start Training", for: .normal)
            } else {
                let timeStamp = Date()
                let format = DateFormatter()
                format.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let formattedDate = format.string(from: timeStamp)
                UserDefaults.standard.set(formattedDate, forKey: "TrainingTimeStamp")
                UserDefaults.standard.synchronize()
                print(formattedDate)
            }
           
        }
        
    }
    
    @IBAction func action_AddModel(_ sender: UIButton) {
		
        GoingNewModel = true
		ModelScreenMode = NewModelScreen.AddModel
        self.PushViewController(toVC: "NewModelController", mine: self)
    }
	
	@objc func action_OpenImages(_ sender : UIButton) {
		
		let cellPosition = sender.convert(CGPoint.zero, to: self.ModelListingCollection)
		let indexpath = self.ModelListingCollection.indexPathForItem(at: cellPosition)
		
        GoingNewModel = false
        
		let Item = ModelListArray[indexpath!.row] as! [String: Any]
		Product = ProductDetail.init(dictionary: Item)
		
		self.PushViewController(toVC: "ModelImageListController", mine: self)
	}
    
}

extension ModelListingController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ModelListArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let wide = MAINSCREEN_WIDTH * 0.95
		var height : CGFloat = 0.0
		let Item = ModelListArray[indexPath.row] as! [String: Any]
		
		if Item["attachment"] as? String == "" && Item["manual"] as? String == "" && Item["website_url"] as? String == "" {
			height = (CURRENT_DEVICE == .pad) ? wide/4 : wide/3.5
		}
		else {
			height = (CURRENT_DEVICE == .pad) ? wide/3 : wide/2.54
		}
		return CGSize(width: wide, height: height)
		
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
		let cell: ModelListCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ModelListCell", for: indexPath) as! ModelListCell
		
        cell.mainView.layer.borderColor = UIColor.lightGray.cgColor
        cell.mainView.layer.borderWidth = 0.5
        
        let Item = ModelListArray[indexPath.row] as! [String: Any]
		
		cell.inputData = Item
        
        cell.modelImage.sd_setImage(with: URL(string: "\(MODEL_BASE_URL)\(Item["img_path"] ?? "")"), placeholderImage: UIImage(named: "ai-image"))
        cell.modelName.text = Item["name"] as? String
        cell.modelDescription.text = Item["description"] as? String
		
		cell.modelImageCollection.addTarget(self, action: #selector(action_OpenImages), for: .touchUpInside)
		
		cell.modelAttachment.tag = indexPath.row
		cell.modelManual.tag = indexPath.row
		cell.modelURL.tag = indexPath.row
		
		cell.modelAttachment.addTarget(self, action: #selector(action_AttachmentButton(_:)), for: .touchUpInside)
		cell.modelManual.addTarget(self, action: #selector(action_ManualButton(_:)), for: .touchUpInside)
		cell.modelURL.addTarget(self, action: #selector(action_URLButton(_:)), for: .touchUpInside)
		
        return cell
    }
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		
		let Item = ModelListArray[indexPath.row] as! [String: Any]
		Product = ProductDetail.init(dictionary: Item)
		self.PushViewController(toVC: "ProductDetailController", mine: self)
		
	}
	
	@IBAction func action_AttachmentButton (_ sender: UIButton) {
		print("Attachment Button Action")
		
		let Item = ModelListArray[sender.tag] as! [String: Any]
		self.openDocumentFile(fileLocation: "\(MODEL_BASE_URL)\(Item["attachment"] as! String)", Show: true)
	}
	
	@IBAction func action_ManualButton (_ sender: UIButton) {
		print("Manual Button Action")
		
		let Item = ModelListArray[sender.tag] as! [String: Any]
		Product = ProductDetail.init(dictionary: Item)
		self.PushViewController(toVC: "ProductDetailController", mine: self)
	}
	
	@IBAction func action_URLButton (_ sender: UIButton) {
		print("URL Button Action")
		
		let Item = ModelListArray[sender.tag] as! [String: Any]
		self.openDocumentFile(fileLocation: Item["website_url"] as! String, Show: true)
	}
    
}

extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
}
