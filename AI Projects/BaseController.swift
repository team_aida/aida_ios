//
//  BaseController.swift
//  AR_AI_Demo
//
//  Created by offshore_mac_1 on 17/01/19.
//  Copyright © 2019 offshore_mac_1. All rights reserved.
//

import UIKit
import Photos
import SwiftyJSON
import Alamofire
import JWT
import SDWebImage
import NVActivityIndicatorView
import MobileCoreServices
import WebKit

let EmptyString = ""

class BaseController: UIViewController, NVActivityIndicatorViewable, UINavigationControllerDelegate {
	
	let docView = UIView()
	
	var FromVC = String()
	var PDFFileURL = String()
	
	let activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: MAINSCREEN_WIDTH * 0.4, y: MAINSCREEN_WIDTH * 0.5, width: MAINSCREEN_WIDTH * 0.2, height: MAINSCREEN_WIDTH * 0.2), type:.ballSpinFadeLoader, color: SPINNER_COLOR_DARK, padding: MAINSCREEN_WIDTH * 0.05)
	//	override var prefersStatusBarHidden: Bool { return true }
	
    override func viewDidLoad() {
        super.viewDidLoad()
		HideNavigationBar(true)
    }
	
	func HideNavigationBar (_ input : Bool) {
		self.navigationController?.navigationBar.isHidden = input
	}
	
	@IBAction func backButtonPressed () {
		GoBack(true)
	}
	
	func GoBack (_ input : Bool) {
		self.navigationController?.popViewController(animated: input)
	}
	
	func RemoveKeyboardResponse () {
		UIApplication.shared.sendAction(#selector(self.resignFirstResponder), to: nil, from: nil, for: nil)
	}
	
	func StartAnimation (msg : String) {
		
		let width = (CURRENT_DEVICE == .pad) ? MAINSCREEN_WIDTH * 0.1 : MAINSCREEN_WIDTH * 0.2
		let size = CGSize(width: width, height: width)
		startAnimating(size, message: msg, type: .circleStrokeSpin, fadeInAnimation: nil)
	}
	
	func StopAnimation () {
		self.stopAnimating(nil)
	}
	
	func showImageAnimation (_ input : Bool, inputView : UIView, topBar : UIView, animView : UIView) {
		print("Show Image Animation")
		
		let oPoint = topBar.frame.origin.y + topBar.frame.size.height
		animView.frame = CGRect(x: 10, y: oPoint + 10, width: MAINSCREEN_WIDTH - 20, height: inputView.frame.size.height * 0.85 - oPoint)
		animView.backgroundColor = UIColor.clear
		animView.clipsToBounds = true
		
		if input {
			self.view.addSubview(animView)
			self.view.bringSubviewToFront(animView)
			
			animView.twinkle()
		}
		else {
			animView.removeFromSuperview()
		}
		
	}
}

extension BaseController : UIDocumentPickerDelegate, UIDocumentInteractionControllerDelegate {
	
	func openDocumentPicker(){
		
		let importMenu = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF)], in: .import)
		//		let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF)], in: .import)
		importMenu.delegate = self
		importMenu.modalPresentationStyle = .formSheet
		self.present(importMenu, animated: true, completion: nil)
	}
	
	public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
		let myURL = url as URL
		PDFFileURL = myURL.absoluteString
		print("File Import String result : \(PDFFileURL)")
	}
	
	func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
		print("view was cancelled")
		dismiss(animated: true, completion: nil)
	}
	
	func openDocumentFile(fileLocation: String, Show: Bool) {
		
		docView.frame = CGRect(x: 0, y: 0, width: MAINSCREEN_WIDTH, height: MAINSCREEN_HEIGHT)
		docView.backgroundColor = .white
		
		if Show {
			let closeButton = UIButton(frame: CGRect(x: MAINSCREEN_WIDTH * 0.05, y: 20, width: MAINSCREEN_WIDTH * 0.25, height: MAINSCREEN_WIDTH * 0.15))
			closeButton.setTitle("Close", for: .normal)
			closeButton.setTitleColor(SPINNER_COLOR_DARK, for: .normal)
			closeButton.addTarget(self, action: #selector(closeDocumentFile(_:)), for: .touchUpInside)
			docView.addSubview(closeButton)
			
			let FileWebView = WKWebView()
			FileWebView.frame = CGRect(x: 0, y: closeButton.frame.origin.y + closeButton.frame.size.height , width: MAINSCREEN_WIDTH, height: MAINSCREEN_HEIGHT-(closeButton.frame.origin.y + closeButton.frame.size.height))
			docView.addSubview(FileWebView)
			
			
			let url: URL! = URL(string: "\(fileLocation)")
			let myRequest = URLRequest(url: url)
			FileWebView.load(myRequest)
			
			self.view.addSubview(docView)
			self.view.bringSubviewToFront(docView)
		}
		else {
			docView.removeFromSuperview()
		}
		
	}
	
	@IBAction func closeDocumentFile (_ sender: UIButton) {
		openDocumentFile(fileLocation: "", Show: false)
	}
	
}
