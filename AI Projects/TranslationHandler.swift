//
//  TranslationHandler.swift
//  AI Projects
//
//  Created by offshore_mac_1 on 01/03/19.
//  Copyright © 2019 Rajeev Lochan Ranga. All rights reserved.
//

import UIKit

class TranslationHandler {
	
	static let sharedInstance = TranslationHandler()
	var translatedMessage : TranslationMessage?
	var translatedMessageRecord = [TranslationMessage]()
    
	var sourceLanguageCode: String {
		set(newSourceLanguageCode) {
			UserDefaults.standard.set(newSourceLanguageCode, forKey: Constants.SourceLanguage)
			UserDefaults.standard.synchronize()
		}
		get {
			 var langCode = UserDefaults.standard.value(forKey: Constants.SourceLanguage)
			
			if langCode == nil {
				langCode = "en"
			}
			
			return langCode as! String
		}
	}
	
	var targetLanguageCode: String {
		set(targetLanguageCode) {
			UserDefaults.standard.set(targetLanguageCode, forKey: Constants.TargetLanguage)
			UserDefaults.standard.synchronize()
		}
		get {
			var langCode = UserDefaults.standard.value(forKey: Constants.TargetLanguage)
			
			if langCode == nil {
				langCode = "nl"
			}
			return langCode as! String
		}
	}
	
	func addMessage(message: TranslationMessage) {
		self.translatedMessageRecord.append(message)
	}
	
	func clearMessageHistory() {
		self.translatedMessageRecord.removeAll()
		UserDefaults.standard.removeObject(forKey: Constants.TranslationMessagesHistory)
		UserDefaults.standard.synchronize()
	}
	
	func translate(transMessage: TranslationMessage, success: @escaping (Any) -> Void, failure: @escaping (NetworkError) -> Void)  {
		
		// var translatedMessage : TranslationMessage?
		let param = [Constants.TargetLang : transMessage.targetLanguage, Constants.SourceLang : transMessage.sourceLanguage,
								  Constants.translation_text : transMessage.message]
			
		
		NetworkHandler.postTranslationRequest(parameters: param, success: { (successResponse) in
			
			self.translatedMessage = TranslationMessage.init(message: successResponse as? String, translationLanguageType: TranslationLanguageType.Target, targetLanguage: transMessage.targetLanguage, sourceLanguage: transMessage.sourceLanguage)
			success(successResponse)
			print("result of translation")
		}) { (NetworkError) in
			failure(NetworkError)
		}
	//	return translatedMessage!
	}
}
