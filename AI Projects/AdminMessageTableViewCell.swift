//
//  AdminMessageTableViewCell.swift
//  AI Projects
//
//  Created by Rajeev Lochan Ranga on 27/03/19.
//  Copyright © 2019 Rajeev Lochan Ranga. All rights reserved.
//

import UIKit

class AdminMessageTableViewCell: UITableViewCell {

    @IBOutlet weak var lbl_message: UILabel!
    @IBOutlet weak var messageView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
