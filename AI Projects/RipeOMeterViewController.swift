//
//  RipeOMeterViewController.swift
//  AI Projects
//
//  Created by offshore_mac_1 on 23/05/19.
//  Copyright © 2019 Rajeev Lochan Ranga. All rights reserved.
//

import UIKit

class RipeOMeterViewController: BaseController {
	
	@IBOutlet weak var btnStartDemo: UIButton!
	@IBOutlet weak var menuButton: UIBarButtonItem!
	
	
	
    override func viewDidLoad() {
        super.viewDidLoad()

		if revealViewController() != nil {
			self.menuButton.target = revealViewController()
			self.menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
//			view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
		}
		
	self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
		self.navigationController?.navigationBar.shadowImage = UIImage()
		self.navigationController?.navigationBar.isTranslucent = true
		self.navigationController?.view.backgroundColor = .clear
    }
	
	override func viewWillAppear(_ animated: Bool) {
		self.HideNavigationBar(false)
	}
	
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		
		
		self.btnStartDemo.layer.shadowColor = #colorLiteral(red: 0.3176470697, green: 0.07450980693, blue: 0.02745098062, alpha: 1)
		self.btnStartDemo.layer.shadowOpacity = 1
		self.btnStartDemo.layer.shadowOffset = CGSize(width: 0, height: 15)
		self.btnStartDemo.layer.shadowRadius = self.btnStartDemo.frame.size.height / 2
		self.btnStartDemo.layer.shadowPath = UIBezierPath(rect: CGRect.init(x: 5, y: 10, width: self.btnStartDemo.frame.size.width-10, height: self.btnStartDemo.frame.size.height-20)).cgPath
		self.btnStartDemo.layer.shouldRasterize = true
		self.btnStartDemo.layer.cornerRadius = self.btnStartDemo.frame.size.height/2
	}
	
	@IBAction func buttonChat_action(_ sender: UIButton) {
		self.performSegue(withIdentifier: "NavigateToChatViewController", sender: nil)
	}
	
	@IBAction func action_StartDemo(_ sender: UIButton) {
		SelectCameraMode = CameraMode.RipePrediction
		self.PushViewController(toVC: "CameraManagerController", mine: self)
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "NavigateToChatViewController" {
			let destVC = segue.destination as! ChatSupportViewController
			destVC.shouldShowBackButton = true
		}
	}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
