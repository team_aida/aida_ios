//
//  VoiceTranslationDetailViewController.swift
//  AI Projects
//
//  Created by offshore_mac_1 on 01/03/19.
//  Copyright © 2019 Rajeev Lochan Ranga. All rights reserved.
//

import UIKit
import Speech

class VoiceTranslationDetailViewController: UIViewController, SFSpeechRecognizerDelegate {
    
    @IBOutlet weak var countryListTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var view_mic_outer: UIView!
    @IBOutlet weak var view_mic_inner: UIView!
    @IBOutlet weak var btnMicrophone: UIButton!
    @IBOutlet weak var translationTableView: UITableView!
    @IBOutlet weak var countryListTableView: UITableView!
    @IBOutlet weak var view_Detectlang: UIView!
    @IBOutlet weak var view_TranslateLang: UIView!
    
    @IBOutlet weak var lblTranslatedLang: UILabel!
    @IBOutlet weak var lblSourceLang: UILabel!
    @IBOutlet weak var view_countryList: UIView!
    @IBOutlet weak var imgView_sourceLangFlag: UIImageView!
    @IBOutlet weak var imgView_TargetLangFlag: UIImageView!
    
    var animDuration : CGFloat = 1.5
    var maxGlowSize : CGFloat = 10
    var minGlowSize : CGFloat = 0
    var selectedLangType: TranslationLanguageType?
    
    
    let speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))!
    
    var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    var recognitionTask: SFSpeechRecognitionTask?
    let audioEngine = AVAudioEngine()
    var recordTimer = Timer()
    var timerStartCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.translationTableView.rowHeight = UITableView.automaticDimension
        speechRecognizer.delegate = (self as SFSpeechRecognizerDelegate)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.countryListTopConstraint.constant =  self.view.bounds.size.height
        self.view.layoutIfNeeded()
        self.view.setNeedsLayout()
        
        self.view_TranslateLang.layer.cornerRadius = self.view_TranslateLang.bounds.size.height/2
        self.view_TranslateLang.layer.borderColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        self.view_TranslateLang.layer.borderWidth = 1.0
        
        self.view_Detectlang.layer.cornerRadius = self.view_Detectlang.bounds.size.height/2
        self.view_Detectlang.layer.borderColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        self.view_Detectlang.layer.borderWidth = 1.0
        
        self.lblSourceLang.text = CommontMethods.getlanguageNameForLanguageCode(languageCode: TranslationHandler.sharedInstance.sourceLanguageCode)
        
        let sourceflagName = CommontMethods.getCountryCodeForLanguageCOde(languageCode: TranslationHandler.sharedInstance.sourceLanguageCode)
        
        self.imgView_sourceLangFlag.image =  UIImage.init(named: sourceflagName)
        
        self.lblTranslatedLang.text = CommontMethods.getlanguageNameForLanguageCode(languageCode: TranslationHandler.sharedInstance.targetLanguageCode)
        
        let targetflagName = CommontMethods.getCountryCodeForLanguageCOde(languageCode: TranslationHandler.sharedInstance.targetLanguageCode)
        
        self.imgView_TargetLangFlag.image =  UIImage.init(named: targetflagName)
        
    }
    
    @IBAction func buttonBack_action(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonTargetLanguage_action(_ sender: UIButton) {
        self.selectedLangType = TranslationLanguageType.Target
        self.showLanguageList(show: true)
    }
    @IBAction func buttonSourceLanguage_action(_ sender: UIButton) {
        self.selectedLangType = TranslationLanguageType.Source
        self.showLanguageList(show: true)
        
        
    }
    @IBAction func buttonDone_action(_ sender: UIButton) {
        self.showLanguageList(show: false)
        
    }
    
    
    
    
    @IBAction func buttonMicrophone_action(_ sender: UIButton) {
        if self.lblSourceLang.text !=  self.lblTranslatedLang.text {
            if audioEngine.isRunning {
                self.startAnimation(show: false)
                audioEngine.stop()
                recognitionRequest?.endAudio()
                self.btnMicrophone.isEnabled = false
            } else {
                self.startAnimation(show: true)
                startRecording()
            }
        }
        else {
            let alert = CommontMethods.showAlert(title: "", message: "Source language must be different from tranlation language", ok_button_title: "OK")
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.view_mic_inner.layer.cornerRadius = self.view_mic_inner.frame.size.height/2
        
        self.setupButton()
        
    }
    
    
    func startTimer() {
        self.timerStartCount = 0
        self.recordTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.stopRecording), userInfo: "record Timer", repeats: true)
        print("Timer fired")
        self.recordTimer.fire()
        
    }
    
    func resetTimer() {
        print("Recording Timer reset")
        
        self.recordTimer.invalidate()
        self.startTimer()
    }
    
    @objc func stopRecording(timer: Timer) {
       
        
        if  self.timerStartCount >= 1 {
            
            self.recordTimer.invalidate()
            self.startAnimation(show: false)
            audioEngine.stop()
            recognitionRequest?.endAudio()
        }
        self.timerStartCount = self.timerStartCount + 1
        
    }
    

    
    func setupButton()
    {
        self.view_mic_outer.layer.cornerRadius = self.view_mic_outer.frame.size.height/2
        
        self.view_mic_outer.layer.shadowColor = #colorLiteral(red: 0.3176470697, green: 0.07450980693, blue: 0.02745098062, alpha: 1)
        self.view_mic_outer.layer.shadowOpacity = 1
        self.view_mic_outer.layer.shadowOffset = CGSize.zero
        self.view_mic_outer.layer.shadowRadius = minGlowSize
        
        let circlePath = UIBezierPath(ovalIn: self.view_mic_outer.bounds)
        
        self.view_mic_outer.layer.shadowPath = circlePath.cgPath
        self.view_mic_outer.layer.shouldRasterize = true
        maxGlowSize = self.view_mic_outer.frame.size.height/2
        
    }
    
    func startAnimation(show: Bool)
    {
        let layerAnimation = CABasicAnimation(keyPath: "shadowRadius")
        layerAnimation.fromValue = minGlowSize
        layerAnimation.toValue = maxGlowSize
        layerAnimation.autoreverses = true
        layerAnimation.isAdditive = false
        layerAnimation.duration = CFTimeInterval(animDuration/2)
        layerAnimation.fillMode = CAMediaTimingFillMode.forwards
        layerAnimation.isRemovedOnCompletion = false
        layerAnimation.repeatCount = .infinity
        if show {
            self.view_mic_outer.layer.add(layerAnimation, forKey: "glowingAnimation")
        }
        else {
            
            self.view_mic_outer.layer.removeAllAnimations()
            self.view_mic_outer.layer.shadowRadius = minGlowSize
        }
        
    }
    
    
    func startRecording() {
        
        if recognitionTask != nil {  //1
            recognitionTask?.cancel()
            recognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()  //2
        do {
            try audioSession.setCategory(AVAudioSession.Category.playAndRecord, mode: AVAudioSession.Mode.spokenAudio, options: AVAudioSession.CategoryOptions.defaultToSpeaker)
            try audioSession.setMode(AVAudioSession.Mode.measurement)
            try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()  //3
        
        let inputNode = audioEngine.inputNode
        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        } //5
        
        recognitionRequest.shouldReportPartialResults = true  //6
        
        recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in  //7
            
            var isFinal = false  //8
            if result != nil {
                
                let textSpoken = result?.bestTranscription.formattedString  //9
                //                self.lblStatus.text = textSpoken
                isFinal = (result?.isFinal)!
          
                
                print("result =   \(String(describing: result))")
                print("isFinal =   \(isFinal)")
                
                if isFinal  {
                    if textSpoken!.count > 0 {
                        self.getTrnslation(textSpoken: textSpoken!)
                    }
                }
                else {
                    if self.recordTimer.isValid {
                        self.resetTimer()
                    }
                    else {
                        self.startTimer()
                    }
                }
            }
            
            
            if error != nil || isFinal {  //10
                print("error =   \(String(describing: error))")
                
                self.audioEngine.stop()
                
                self.startAnimation(show: false)
                //        self.lblStatus.text = "TAP HERE TO START RECORDING"
                
                inputNode.removeTap(onBus: 0)
                self.recognitionRequest?.endAudio()
                self.recognitionRequest = nil
                self.recognitionTask = nil
                
                self.btnMicrophone.isEnabled = true
            }
            
        })
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)  //11
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()  //12
        
        do {
            try audioEngine.start()
        } catch {
            print("audio Engine couldn't start because of an error.")
        }

    }
    
    
    func getTrnslation(textSpoken: String) {
        let sourceMessage = TranslationMessage.init(message: textSpoken, translationLanguageType: TranslationLanguageType.Source, targetLanguage: TranslationHandler.sharedInstance.targetLanguageCode, sourceLanguage: TranslationHandler.sharedInstance.sourceLanguageCode)
        
        TranslationHandler.sharedInstance.translate(transMessage: sourceMessage, success: { (result) in
            print("result = \(result)")
            TranslationHandler.sharedInstance.addMessage(message: sourceMessage)
            TranslationHandler.sharedInstance.addMessage(message: TranslationHandler.sharedInstance.translatedMessage!)
            print(TranslationHandler.sharedInstance.translatedMessageRecord.count)
            self.translationTableView.reloadData()
            
        }, failure: { (err) in
            
        })
    }
    
    
    func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        
        if available {
            self.btnMicrophone.isEnabled = true
        } else {
            self.btnMicrophone.isEnabled = false
        }
    }
    
    func showLanguageList(show: Bool) {
        if show {
            UIView.animate(withDuration: 0.3) {
                
                self.view_countryList.center = CGPoint.init(x: self.view.center.x ,y: self.view.center.y + 20 )
            }
        }
        else {
            UIView.animate(withDuration: 0.3) {
                self.view_countryList.center = CGPoint.init(x: self.view.center.x, y: (self.view.bounds.size.height * 1.5))
            }
        }
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension VoiceTranslationDetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.translationTableView{
            return TranslationHandler.sharedInstance.translatedMessageRecord.count
        }
        else {
            return LanguageNames.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView ==  self.translationTableView {
            if indexPath.row % 2 == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SourceMessageTableViewCell") as! SourceMessageTableViewCell
                let msg = TranslationHandler.sharedInstance.translatedMessageRecord[indexPath.row]
                cell.lbl_message.text = msg.message
                let lngName = CommontMethods.getlanguageNameForLanguageCode(languageCode: msg.sourceLanguage)
                cell.lbl_languageName.text = "Detected Language: " + lngName
                cell.view_message.layer.cornerRadius = cell.view_message.frame.height/2
                
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "TranslatedMessageTableViewCell") as! TranslatedMessageTableViewCell
                cell.delegate = self
                let msg = TranslationHandler.sharedInstance.translatedMessageRecord[indexPath.row]
                cell.lbl_message.text = msg.message
                let lngName = CommontMethods.getlanguageNameForLanguageCode(languageCode: msg.targetLanguage)
                cell.lbl_languageName.text = "Translated to: " + lngName
                cell.view_message.layer.cornerRadius = cell.view_message.frame.height/2
                
                return cell
            }
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CountryListTableViewCell") as! CountryListTableViewCell
            cell.delegate = self
            let langName = LanguageNames[indexPath.row]
            cell.lbl_languageNamw.text = langName
            let flagName = CommontMethods.getCountryCodeForLanguageCOde(languageCode: LanguageCodes[indexPath.row])
            cell.imgView_flag.image =  UIImage.init(named: flagName)
            return cell
        }
        
    }
    
    
}

extension VoiceTranslationDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height = 0
        if tableView ==  self.translationTableView {
            if indexPath.row % 20 == 0 {
                height = 68
            }
            else {
                height = 101
            }
        }
        else {
            height = 45
        }
        return CGFloat(height)
    }
    
}

extension VoiceTranslationDetailViewController: CountryListTableViewCellDelegate {
    func buttonSelectLanguageTapped(forSender sender: UIButton, forCell cell: CountryListTableViewCell) {
        let indexpath = self.countryListTableView.indexPath(for: cell)
        let langName =  LanguageNames[(indexpath?.row)!]
        if self.selectedLangType == TranslationLanguageType.Source {
            self.lblSourceLang.text = langName
            let flagName = CommontMethods.getCountryCodeForLanguageCOde(languageCode: LanguageCodes[indexpath!.row])
            self.imgView_sourceLangFlag.image =  UIImage.init(named: flagName)
            TranslationHandler.sharedInstance.sourceLanguageCode = CommontMethods.getlanguageCodeForLanguageName(languageName: langName)
        }
        else if self.selectedLangType == TranslationLanguageType.Target {
            TranslationHandler.sharedInstance.targetLanguageCode = CommontMethods.getlanguageCodeForLanguageName(languageName: langName)
            
            self.lblTranslatedLang.text = langName
            let flagName = CommontMethods.getCountryCodeForLanguageCOde(languageCode: LanguageCodes[indexpath!.row])
            self.imgView_TargetLangFlag.image =  UIImage.init(named: flagName)
        }
    }
}

extension VoiceTranslationDetailViewController: TranslatedMessageTableViewCellDelegate {
    func buttonCopyTapped(forSender sender: UIButton, forCell cell: TranslatedMessageTableViewCell) {
        UIPasteboard.general.string = cell.lbl_message.text
    }
    func buttonSpeakerTapped(forSender sender: UIButton, forCell cell: TranslatedMessageTableViewCell) {
        let indexpath = self.translationTableView.indexPath(for: cell)
        
        let translatedMsg = TranslationHandler.sharedInstance.translatedMessageRecord[(indexpath?.row)!]
        let uttrance = AVSpeechUtterance(string: translatedMsg.message)
        uttrance.voice = AVSpeechSynthesisVoice(language: TranslationHandler.sharedInstance.targetLanguageCode)
        uttrance.rate = AVSpeechUtteranceDefaultSpeechRate
        
        let audioSynthesizer = AVSpeechSynthesizer()
        audioSynthesizer.speak(uttrance)
        
    }
}
