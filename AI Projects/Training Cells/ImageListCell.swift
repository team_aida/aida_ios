//
//  ImageListCell.swift
//  AI Model Training
//
//  Created by Rajeev Lochan Ranga on 01/03/19.
//  Copyright © 2019 Offshore. All rights reserved.
//

import UIKit

class ImageListCell: UICollectionViewCell {

	@IBOutlet weak var ModelProductImage: UIImageView!
	@IBOutlet weak var EditImageButton: ActionButton!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
	
	override func layoutSubviews() {
		self.contentView.frame = CGRect(x: 0, y: 0, width: MAINSCREEN_WIDTH * 0.45, height: MAINSCREEN_WIDTH * 0.45)
		
		self.ModelProductImage.layer.borderWidth = 3.0
		self.ModelProductImage.layer.borderColor = UIColor.white.cgColor
		
		self.CornerRadius(EditImageButton, value: 0.5)
		self.EditImageButton.clipsToBounds = true
		
		self.EditImageButton.layer.borderWidth = 1.0
		self.EditImageButton.layer.borderColor = UIColor.white.cgColor
	}

}
