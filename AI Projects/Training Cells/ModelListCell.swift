//
//  ModelListCell.swift
//  AI Model Training
//
//  Created by Rajeev Lochan Ranga on 01/03/19.
//  Copyright © 2019 Offshore. All rights reserved.
//

import UIKit

class ModelListCell: UICollectionViewCell {
    
    @IBOutlet weak var mainView: UIView!
	
	var inputData : [String : Any]?
    
    @IBOutlet weak var modelImage: UIImageView!
    @IBOutlet weak var modelName: UILabel!
    @IBOutlet weak var modelDescription: UILabel!
    
    @IBOutlet weak var buttonsStack: UIStackView!
    @IBOutlet weak var modelAttachment: UIButton!
    @IBOutlet weak var modelManual: UIButton!
    @IBOutlet weak var modelURL: UIButton!
    
    @IBOutlet weak var modelImageCollection: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        let wide = MAINSCREEN_WIDTH * 0.95
		
        self.modelImage.layer.borderColor = UIColor.orange.cgColor
        self.modelImage.layer.borderWidth = 1.0
		
		let height = (CURRENT_DEVICE == .pad) ? wide/3 : wide/2.54
		
        self.contentView.frame = CGRect(x: 0, y: 0, width: wide, height: height)
        self.mainView.frame = CGRect(x: 0, y: 0, width: wide, height: height)
		
		self.CornerRadius(modelImage, value: 0.5)
		self.CornerRadius(modelAttachment, value: 0.5)
		self.CornerRadius(modelManual, value: 0.5)
		self.CornerRadius(modelURL, value: 0.5)
		
		self.setupStackView()
    }
	
	func setupStackView () {
		
		self.inputData?["attachment"] as? String == "" ? self.modelAttachment.BlendWithBack(hide: true) : self.modelAttachment.BlendWithBack(hide: false)
		self.inputData?["manual"] as? String == "" ? self.modelManual.BlendWithBack(hide: true) : self.modelManual.BlendWithBack(hide: false)
		self.inputData?["website_url"] as? String == "" ? self.modelURL.BlendWithBack(hide: true) : self.modelURL.BlendWithBack(hide: false)
	}
}
