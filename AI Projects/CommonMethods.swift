//
//  CommonMethods.swift
//  AI Projects
//
//  Created by Rajeev Lochan Ranga on 21/02/19.
//  Copyright © 2019 Rajeev Lochan Ranga. All rights reserved.
//

import Foundation
import UIKit
import CommonCrypto

class CommontMethods: NSObject {
    
    class func showAlertWithOkCancel(title alert_title:String, message alert_message:String, ok_button_title btn_title:String, cancel_button_title cancelButtonTitle:String) -> UIAlertController {
        let alert = UIAlertController.init(title: alert_title, message: alert_message, preferredStyle: UIAlertController.Style.alert)
        let okBtn = UIAlertAction.init(title: btn_title, style: .default) { (UIAlertAction) in }
        let cancelBtn = UIAlertAction.init(title: cancelButtonTitle, style: .cancel) { (UIAlertAction) in }
        
        alert.addAction(okBtn)
        alert.addAction(cancelBtn)
        return alert
    }
    
    class func showAlert(title alert_title:String, message alert_message:String, ok_button_title btn_title:String) -> UIAlertController {
        let alert = UIAlertController.init(title: alert_title, message: alert_message, preferredStyle: UIAlertController.Style.alert)
        let okBtn = UIAlertAction.init(title: btn_title, style: .cancel) { (UIAlertAction) in }
        
        alert.addAction(okBtn)
        
        return alert
    }
    
    class func trimString(str: String) -> String {
        let result = str.trimmingCharacters(in: .whitespacesAndNewlines)
        return result
    }
    
    class func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
	class func getlanguageNameForLanguageCode(languageCode: String) -> String {
		let index = LanguageCodes.firstIndex(of: languageCode)
		let result = LanguageNames[index!]
		return result
	}
	
	class func getlanguageCodeForLanguageName(languageName: String) -> String {
		let index = LanguageNames.firstIndex(of: languageName)
		let result = LanguageCodes[index!]
		return result
	}
	
	class func getCountryCodeForLanguageCOde(languageCode: String) -> String {
		let index = LanguageCodes.firstIndex(of: languageCode)
		let result = CountryCodes[index!]
		return result
	}
    class func timeStampFromInt(value: Int) -> MessageDate {
        
        let myDate = Date(timeIntervalSince1970: Double(value) / 1000.0)
        
        var messageDate = MessageDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd"
        messageDate.date = formatter.string(from: myDate)
        
        formatter.dateFormat = "MMM"
        messageDate.month = formatter.string(from: myDate)
        
        formatter.dateFormat = "yyyy"
        messageDate.year = formatter.string(from: myDate)
        
        return messageDate
    }
    class func timeStamp(forDate dateValue: Int) -> String {
        var result = ""
        let myDate = Date(timeIntervalSince1970: Double(dateValue))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        
        result = dateFormatter.string(from: myDate)
        return result
    }
}

extension Data {
	func castToCPointer<T>() -> T {
		return self.withUnsafeBytes { $0.pointee }
	}
}
