//
//  SideMenuViewController.swift
//  AI Projects
//
//  Created by Rajeev Lochan Ranga on 08/03/19.
//  Copyright © 2019 Rajeev Lochan Ranga. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lbl_userName: UILabel!
	@IBOutlet weak var versionLabel: UILabel!
	
    var menuOptions: [String]!

    override func viewDidLoad() {
		
        super.viewDidLoad()
        self.lbl_userName.text = "Hi " + (UserDefaults.standard.value(forKey: Constants.kName) as! String)
        self.tableView.estimatedRowHeight = 28
        self.tableView.rowHeight = UITableView.automaticDimension
        self.menuOptions = ["Home",
                           // "Object Recognition",
                            "Offshore Software Solutions",
                           // "Voice Translation",
                            "Nederland.ai",
                           // "Fresh Shopping",
                            "Object Recognition & Training",
                        //    "Ripe-o-meter",
							"Chat",
                            "Privacy Policy"
                            ]
		
        versionLabel.text = self.appVersionNumber() //self.appBuildNumber()
    }
    
    //MARK: - UITableViewDataSource Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menuOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:SideMenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell") as! SideMenuTableViewCell
        cell.selectionStyle = .none
        cell.lbl_menuTitle.text = self.menuOptions[indexPath.row]
        return cell
    }
    
    //MARK: - UITableViewDelegate Methods
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            self.performSegue(withIdentifier: "NavigateToHomeViewController", sender: nil)
            break
//        case 1:
//            self.performSegue(withIdentifier: "NavigateToObjectRecognitionViewController", sender: nil)
 //           break
        case 1:
            self.performSegue(withIdentifier: "NavigateToOffshoreViewController", sender: nil)
            break
//        case 2:
//            self.performSegue(withIdentifier: "NavigateToVoiceTranslatorViewController", sender: nil)
//            break
        case 2:
            self.performSegue(withIdentifier: "NavigateToNederlandAIViewController", sender: nil)
            break
        
//        case 3:
//            self.performSegue(withIdentifier: "NavigateToFreshShoppingViewController", sender: nil)
//            break
        case 3:
            self.performSegue(withIdentifier: "NavigateToObjectRecognitionTrainingViewController", sender: nil)
            break
//        case 4:
//            self.performSegue(withIdentifier: "NavigateToRipeMeterController", sender: nil)
//            break
        case 4:
            self.performSegue(withIdentifier: "NavigateToChatSupportViewController", sender: nil)
            break
        case 5:
            isPrivacyPolicy = true
        self.performSegue(withIdentifier: "NavigateToPrivacyPolicyController", sender: nil)
        break
        default:
            print("Nothing")
            break
        }
    }
}

