//
//  ModelImageListController.swift
//  AI Model Training
//
//  Created by offshore_mac_1 on 08/03/19.
//  Copyright © 2019 Offshore. All rights reserved.
//

import UIKit

class ModelImageListController: BaseController {
	
	var ListArray = [Any]()
	
	@IBOutlet weak var Label_ProductName: UILabel!
	@IBOutlet weak var cameraIconButton: UIButton!
	@IBOutlet weak var ImageListCollection: UICollectionView!
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		self.Label_ProductName.text = Product?.name
		
    }
	
	override func viewWillAppear(_ animated: Bool) {
		ProductImage = ImageDetail.init(dictionary: [:])
		LoadImagesForProduct()
		
		if ListArray.count < 20 {
			cameraIconButton.isHidden = false
		}
		else {
			cameraIconButton.isHidden = true
		}
	}
	
	func LoadImagesForProduct () {
		Webservices.shared.ModelImageList(limit: 20, offset: 0, modelId: "\(Product?.p_id ?? 0)") { (Status, Result) in
			if Status == 1 {
				print("Load Images for Product :", Result)
				self.ListArray = Result
				self.ImageListCollection.reloadData()
			}
			else {
				self.showAlert(withTitle: "", message: "No Images found for this object, would you like to add?", okButton: "Yes", cancelButton: "Not Now", completion: { (response) in
					if response {
						self.AddImage()
					}
					else {
						self.PopViewController(from: self)
					}
				})
			}
		}
	}
	
	@IBAction func action_EditImage(_ sender: UIButton) {
		SelectCameraMode = CameraMode.EditImage
		ProductImage = ImageDetail.init(dictionary: ListArray[sender.tag] as! [String : Any])
		self.PushViewController(toVC: "CameraManagerController", mine: self)
	}
	
	
	@IBAction func action_ImageUploadCamera(_ sender: UIButton) {
		if ListArray.count < 20 {
			AddImage()
		}
	}
	
	func AddImage () {
		SelectCameraMode = CameraMode.AddImage
		self.PushViewController(toVC: "CameraManagerController", mine: self)
	}
    
    @IBAction func backBtnAction(_ sender: UIButton) {
        var navigationArray = self.navigationController?.viewControllers //To get all UIViewController stack as Array
        for viewController in navigationArray!  {
            if viewController is CameraManagerController || viewController is NewModelController {
                let index = (navigationArray?.index(of: viewController))!
                navigationArray?.remove(at: index)
            }
        }
        self.navigationController?.viewControllers = navigationArray!
        self.PopViewController(from: self)
    }
    
}

extension ModelImageListController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
	
	//MARK:- Collection View Methods
	
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return 1
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return ListArray.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell: ImageListCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageListCell", for: indexPath) as! ImageListCell
		let Item = ListArray[indexPath.row] as! [String:Any]
		
		cell.ModelProductImage.sd_setImage(with: URL(string: "\(MODEL_BASE_URL)\(Item["image_path"] ?? "")"), placeholderImage: UIImage(named: ""))
		cell.EditImageButton.tag = indexPath.row
		cell.EditImageButton.addTarget(self, action: #selector(action_EditImage(_:)), for: .touchUpInside)
		
		return cell
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		let wide = MAINSCREEN_WIDTH * 0.45
		return CGSize(width: wide, height: wide)
	}
	
//	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//		<#code#>
//	}
	
}
