//
//  SparkleView.swift
//  SparklingAnimation
//
//  Created by Lions Of Mirzapur on 15/05/19.
//  Copyright © 2019 Lions Of Mirzapur. All rights reserved.
//

import UIKit



class SparkleView: UIView {
    let xsize = 5.0

    lazy var circleView : UIView = {
        let view = UIView()
        view.frame.size = CGSize(width: xsize, height: xsize)
        view.center = self.center
        view.layer.cornerRadius = CGFloat(xsize/2)
        view.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        return view
    }()
    
    lazy var ringView : UIView = {
        let view = UIView()
        view.frame.size = CGSize(width: xsize, height: xsize)
        view.center = self.center
        view.layer.cornerRadius = CGFloat(xsize/2)
        view.layer.borderWidth = 1.0
        view.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        view.backgroundColor = UIColor.clear
        view.alpha = 1.0
        return view
    }()
    

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
       
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.setUpView()
    }
    
    func setUpView() {
        self.addSubview(ringView)
        self.addSubview(circleView)
        self.isHidden = false
    }
    
    func show() {
        UIView.animate(withDuration: 0.5, animations: {
            self.ringView.transform = CGAffineTransform(scaleX: 4.0, y: 4.0)
            self.ringView.alpha = 0
            
        }) { (val) in
            
        }
        
        UIView.animate(withDuration: 0.5, delay: 0.7,options: UIView.AnimationOptions.curveEaseOut,animations: {
            self.circleView.transform = CGAffineTransform(scaleX: 0.0001, y: 0.0001)
            
        },completion: { (value) in
            self.ringView.removeFromSuperview()
            self.ringView.alpha = 1.0
            self.ringView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            
            self.circleView.removeFromSuperview()
            self.circleView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            
            self.isHidden = true
        })

    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}



