//
//  ProductDetailController.swift
//  AI Model Training
//
//  Created by offshore_mac_1 on 13/03/19.
//  Copyright © 2019 Offshore. All rights reserved.
//

import UIKit

class ProductDetailController: BaseController {
	
	@IBOutlet weak var button_ImageCollection: ActionButton!
	@IBOutlet weak var ProductDetailTable: UITableView!
	
	let HeadingsKeys = ["name", "description", "manual", "website_url"]
	let HeadingValues = ["Product Name", "Description", "Product Manual", "Reference URL"]
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		ProductDetailTable.tableFooterView = UIView(frame: .zero)
		print("Product Detail Controller : ", Product?.setupDisplayDetail().0.keys ?? 0)
		let img = UIImage(named: "image_collections")?.withRenderingMode(.alwaysTemplate)
		button_ImageCollection.setImage(img, for: .normal)
		
		
    }
	
	@IBAction func action_ViewImageCollection(_ sender: UIButton) {
		self.PushViewController(toVC: "ModelImageListController", mine: self)
	}
	
	@IBAction func action_NextButton () {
		ModelScreenMode = NewModelScreen.EditModel
		self.PushViewController(toVC: "NewModelController", mine: self)
	}
	
}

extension ProductDetailController : UITableViewDelegate, UITableViewDataSource {
	
	//MARK:- Table View Methods
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		
		if indexPath.row < (Product?.setupDisplayDetail().1.count)! {
//			return MAINSCREEN_WIDTH * 0.2
			return UITableView.automaticDimension
		}
		else {
			return MAINSCREEN_WIDTH * 0.25
		}
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return (Product?.setupDisplayDetail().1.count)! + 1
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		if indexPath.row < (Product?.setupDisplayDetail().0.count)! {
			let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCell", for: indexPath)
			
			let Heading : UILabel = cell.viewWithTag(1) as! UILabel
			let Detail : UILabel = cell.viewWithTag(2) as! UILabel
			
//			print("Index Path : \(indexPath.row) and \(Array((Product?.sretupDisplayDetail().keys)!))")
//			Heading.text = HeadingsKeys[Array((Product?.setupDisplayDetail().keys)!)[indexPath.row]]
			Heading.text = Array((Product?.setupDisplayDetail().1)!)[indexPath.row]
			Detail.text = Array((Product?.setupDisplayDetail().2)!)[indexPath.row]
			
			return cell
			
		}
		else {
			let cell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell", for: indexPath)
			
			let ActionButton : ActionButton = cell.viewWithTag(1) as! ActionButton
			ActionButton.addTarget(self, action: #selector(action_NextButton), for: .touchUpInside)
			self.CornerRadius(ActionButton, value: 0.5)
			return cell
		}
	}
	
}
