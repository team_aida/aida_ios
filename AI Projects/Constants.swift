//
//  Constants.swift
//  AI Projects
//
//  Created by Rajeev Lochan Ranga on 21/02/19.
//  Copyright © 2019 Rajeev Lochan Ranga. All rights reserved.
//

import Foundation

struct Constants {
    static let URLRegister = "https://us20.api.mailchimp.com/3.0/lists/e99cd57087/members/"
	static let URLGetMemberDetails = "https://us20.api.mailchimp.com/3.0/lists/e99cd57087/members/"
	static let URL_Nederland_AI = "https://nederland.ai/" //"https://www.offshoresoftware.solutions/artificial-inteligence/"
	static let URL_OffshoreWebsite = "https://www.offshoresoftware.solutions/" //"https://www.offshoresoftware.solutions/nlweb/"
	static let URL_detectLanguage = "https://translation.googleapis.com/language/translate/v2/detect"
	static let URL_getTranslationText = "https://www.thuis.today/dev/api/chatbot/getTranslatedText"
	static let URL_PrivacyPolicy = "https://nederland.ai/privacy-policy-2/"
    static let Sendbird_AppId = "E614291F-CBD8-44A9-87EA-AC9A7DD0E0E6"
    static let Sendbird_API_Token = "7834a151632fcb2cf03ca25aeb669edc05d23c46"
    static let Sendbird_API = "https://api.sendbird.com"
    
    static let placeholderString = "Type here"
    static let ChannelUrl = "ChannelUrl"
    static let UserId = "UserId"
	static let kName = "name"
	static let kEmail = "email"
	static let SourceLanguage = "SourceLanguage"
	static let TargetLanguage = "TargetLanguage"
	static let TranslationMessagesHistory = "TranslationMessagesHistory"
	static let translation_text = "text"
	static let SourceLang = "origin_lang"
	static let TargetLang = "target_lang"
	static let API_DetectLanguage = "AIzaSyBSYmq5qYmub4GH86n9LNLP4_7zng6mqZ0"
    
    static let SendbirdConnectionDelegate = "SendbirdConnectionDelegate"
    static let SendbirdChannelDelegate = "SendbirdChannelDelegate"
    static let AgentEmail = "agent@offshoresolutions.nl"
    static let AgentName = "Agent"
    static let Agent_Id = "a57cef9ad1c702358acde0b6068772cb"
    
    static let kPredictionItemId = "id"
    static let kPredictionItem_TopCordinate = "top"
    static let kPredictionItem_LeftCordinate = "left"
    static let kPredictionItem_BottomCordinate = "bottom"
    static let kPredictionItem_RightCordinate = "right"
    static let kPredictionItem_Name =  "name"
    static let kPredictionItem_description = "description"
    static let kPredictionItem_attachment = "attachment"
    static let kPredictionItem_manual = "manual"
    static let kPredictionItem_website_url = "website_url"
    static let kPredictionItem_img_path = "img_path"

}

var isPrivacyPolicy = false
let BASE_IMAGE_URL = "https://www.thuis.today/dev/uploads/images/"

let jwtSecurityKey =  "try@!tfgh5673"
//	var googleAPIKey = "AIzaSyDKCBb7hRvlfZ00BLvDIEddaVXQ1BvPw2g"
let VisionAPIKey = "AIzaSyBrgpqG42Dnl47OLa43rv0RteLisOJQV3U" //Key For ARDemo Project from Tanjot id

let CustomSearchAPIKey = "AIzaSyBSYmq5qYmub4GH86n9LNLP4_7zng6mqZ0" // Android Key

let ThuisProductSearch_URL = "https://www.thuis.today/dev/api/chatbot/getSearchProducts"

let GoogleCloudVision_URL = "https://vision.googleapis.com/v1/images:annotate?key=\(VisionAPIKey)"

let GoogleCustomSearch_URL = "https://www.googleapis.com/customsearch/v1"//?key=\(VisionAPIKey)&cx=017567005641165239815:tlstso0odrs&q="
