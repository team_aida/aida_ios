//
//  CountryListTableViewCell.swift
//  AI Projects
//
//  Created by offshore_mac_1 on 01/03/19.
//  Copyright © 2019 Rajeev Lochan Ranga. All rights reserved.
//

import UIKit

protocol CountryListTableViewCellDelegate {
	
	func buttonSelectLanguageTapped(forSender sender:UIButton, forCell cell:CountryListTableViewCell)
}

class CountryListTableViewCell: UITableViewCell {

	@IBOutlet weak var imgView_flag: UIImageView!
	@IBOutlet weak var lbl_languageNamw: UILabel!
	
	var delegate: CountryListTableViewCellDelegate?
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

	@IBAction func buttonSelectLanguage_action(_ sender: UIButton) {
		
		self.delegate?.buttonSelectLanguageTapped(forSender: sender, forCell: self)
	}
	override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
