//
//  TranslatedMessageTableViewCell.swift
//  AI Projects
//
//  Created by offshore_mac_1 on 01/03/19.
//  Copyright © 2019 Rajeev Lochan Ranga. All rights reserved.
//

import UIKit

protocol TranslatedMessageTableViewCellDelegate {
	
	func buttonSpeakerTapped(forSender sender:UIButton, forCell cell:TranslatedMessageTableViewCell)
	func buttonCopyTapped(forSender sender:UIButton, forCell cell:TranslatedMessageTableViewCell)

}

class TranslatedMessageTableViewCell: UITableViewCell {
	
	@IBOutlet weak var lbl_languageName: UILabel!
	@IBOutlet weak var lbl_message: UILabel!
	@IBOutlet weak var view_message: UIView!
	@IBOutlet weak var btnSpeaker: UIButton!
	@IBOutlet weak var btnCopy: UIButton!
	
	var delegate:TranslatedMessageTableViewCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

	@IBAction func buttonSpeaker_action(_ sender: UIButton) {
		self.delegate?.buttonSpeakerTapped(forSender: sender, forCell: self)
	}
	
	@IBAction func buttonCopy_action(_ sender: UIButton) {
		self.delegate?.buttonCopyTapped(forSender: sender, forCell: self)
	}
}
