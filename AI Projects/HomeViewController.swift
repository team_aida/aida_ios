//
//  HomeViewController.swift
//  AI Projects
//
//  Created by Rajeev Lochan Ranga on 31/01/19.
//  Copyright © 2019 Rajeev Lochan Ranga. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    @IBOutlet weak var menuButton: UIBarButtonItem!

    
	@IBOutlet weak var chat_View: UIView!
    @IBOutlet weak var view_swipeDemos: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if revealViewController() != nil {
            self.menuButton.target = revealViewController()
            self.menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
    self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        
        self.chat_View.layer.cornerRadius = self.chat_View.frame.size.height / 2
         self.view_swipeDemos.layer.cornerRadius = self.view_swipeDemos.frame.size.height / 2
        // Do any additional setup after loading the view.
    }
    
    @IBAction func buttonChat_action(_ sender: UIButton) {
        self.performSegue(withIdentifier: "NavigateToChatViewController", sender: nil)
    }
    
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "NavigateToChatViewController" {
            let destVC = segue.destination as! ChatSupportViewController
            destVC.shouldShowBackButton = true
        }
    }

    @IBAction func buttonSwipeDemos_action(_ sender: UIButton) {
       // self.performSegue(withIdentifier: "NavigateToRevealViewController", sender: nil)
    }
	
	@IBAction func unwindToHome_action(segue:UIStoryboardSegue) {
		
	}
}
