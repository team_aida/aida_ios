//
//  PredictProductTableViewCell.swift
//  AI Projects
//
//  Created by Lions Of Mirzapur on 06/06/19.
//  Copyright © 2019 Rajeev Lochan Ranga. All rights reserved.
//

import UIKit

class PredictProductTableViewCell: UITableViewCell {
    @IBOutlet weak var PredictModelImage: UIImageView!
    @IBOutlet weak var PredictModelName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
