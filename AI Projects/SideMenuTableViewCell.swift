//
//  SideMenuTableViewCell.swift
//  AI Projects
//
//  Created by Rajeev Lochan Ranga on 11/03/19.
//  Copyright © 2019 Rajeev Lochan Ranga. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var lbl_menuTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
