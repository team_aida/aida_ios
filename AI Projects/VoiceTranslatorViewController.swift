//
//  VoiceTranslatorViewController.swift
//  AI Projects
//
//  Created by offshore_mac_1 on 26/02/19.
//  Copyright © 2019 Rajeev Lochan Ranga. All rights reserved.
//

import UIKit
import Speech


class VoiceTranslatorViewController: UIViewController, SFSpeechRecognizerDelegate {
	
	@IBOutlet weak var mic_view: UIView!
	@IBOutlet weak var mic_inner_view: UIView!
	@IBOutlet weak var btnMicrophone: UIButton!
	@IBOutlet weak var lblStatus: UILabel!
	
	var animDuration : CGFloat = 1.5
	var maxGlowSize : CGFloat = 10
	var minGlowSize : CGFloat = 0
	var sourceLang : String?
	var textSpoken: String?
	
	 let speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))!
	
	 var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
	 var recognitionTask: SFSpeechRecognitionTask?
	 let audioEngine = AVAudioEngine()
	 var recordTimer = Timer()
    var timerStartCount = 0
    
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.btnMicrophone.isEnabled = false
		
		speechRecognizer.delegate = (self as SFSpeechRecognizerDelegate)
		
		SFSpeechRecognizer.requestAuthorization { (authStatus) in
			
			var isButtonEnabled = false
			
			switch authStatus {
			case .authorized:
				isButtonEnabled = true
				
			case .denied:
				isButtonEnabled = false
				print("User denied access to speech recognition")
				
			case .restricted:
				isButtonEnabled = false
				print("Speech recognition restricted on this device")
				
			case .notDetermined:
				isButtonEnabled = false
				print("Speech recognition not yet authorized")
			}
			
			OperationQueue.main.addOperation() {
				self.btnMicrophone.isEnabled = isButtonEnabled
			}
		}
	}
	

	@IBAction func buttonBack_action(_ sender: UIBarButtonItem) {
		self.navigationController?.popViewController(animated: true)
	}
	
    
	@IBAction func buttonMicrophone_action(_ sender: UIButton) {
		if audioEngine.isRunning {
			self.startAnimation(show: false)
			audioEngine.stop()
			recognitionRequest?.endAudio()
			self.lblStatus.text = "TAP HERE TO START RECORDING"
		} else {
			self.startAnimation(show: true)
			startRecording()
			self.lblStatus.text = "Listening . . ."
		}
	}
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		
		self.mic_inner_view.layer.cornerRadius = self.mic_inner_view.frame.size.height/2
		
		self.setupButton()
		
	}
    
    func startTimer() {
        self.timerStartCount = 0
        self.recordTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.stopRecording), userInfo: "record Timer", repeats: true)
        print("Timer fired")
        self.recordTimer.fire()

    }
    
    func resetTimer() {
        print("Recording Timer reset")

        self.recordTimer.invalidate()
        self.startTimer()
    }
    
    @objc func stopRecording(timer: Timer) {
        print("Time interval = \(timer.timeInterval) ")
        print("tolarance Time interval = \(timer.tolerance) ")
        
        if  self.timerStartCount >= 1 {
            print("Recording stopped after \(timer.timeInterval) seconds")
            
            self.recordTimer.invalidate()
            self.startAnimation(show: false)
            audioEngine.stop()
            recognitionRequest?.endAudio()
            self.lblStatus.text = "TAP HERE TO START RECORDING"
        }
        self.timerStartCount = self.timerStartCount + 1
        
    }

	func setupButton()
	{
		self.mic_view.layer.cornerRadius = self.mic_view.frame.size.height/2

		self.mic_view.layer.shadowColor = #colorLiteral(red: 0.3176470697, green: 0.07450980693, blue: 0.02745098062, alpha: 1)
		self.mic_view.layer.shadowOpacity = 1
		self.mic_view.layer.shadowOffset = CGSize.zero
		self.mic_view.layer.shadowRadius = minGlowSize
		
		let circlePath = UIBezierPath(ovalIn: self.mic_view.bounds)
		
		self.mic_view.layer.shadowPath = circlePath.cgPath
		self.mic_view.layer.shouldRasterize = true
		maxGlowSize = self.mic_view.frame.size.height/2

	}
	
	func startAnimation(show: Bool)
	{
		let layerAnimation = CABasicAnimation(keyPath: "shadowRadius")
		layerAnimation.fromValue = minGlowSize
		layerAnimation.toValue = maxGlowSize
		layerAnimation.autoreverses = true
		layerAnimation.isAdditive = false
		layerAnimation.duration = CFTimeInterval(animDuration/2)
		layerAnimation.fillMode = CAMediaTimingFillMode.forwards
		layerAnimation.isRemovedOnCompletion = false
		layerAnimation.repeatCount = .infinity
		if show {
			self.mic_view.layer.add(layerAnimation, forKey: "glowingAnimation")
		}
		else {
			
			self.mic_view.layer.removeAllAnimations()
			self.mic_view.layer.shadowRadius = minGlowSize
		}
		
		
	}
	func startRecording() {
		
		if recognitionTask != nil {  //1
			recognitionTask?.cancel()
			recognitionTask = nil
		}
		
		let audioSession = AVAudioSession.sharedInstance()  //2
		do {
			try audioSession.setCategory(AVAudioSession.Category.playAndRecord, mode: AVAudioSession.Mode.spokenAudio, options: AVAudioSession.CategoryOptions.defaultToSpeaker)
			try audioSession.setMode(AVAudioSession.Mode.measurement)
			try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
		} catch {
			print("audioSession properties weren't set because of an error.")
		}
		
		recognitionRequest = SFSpeechAudioBufferRecognitionRequest()  //3
		
		let inputNode = audioEngine.inputNode
		guard let recognitionRequest = recognitionRequest else {
			fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
		} //5
		
		recognitionRequest.shouldReportPartialResults = true  //6
		
		recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in  //7
			
			var isFinal = false  //8
			if result != nil {
				
				self.textSpoken = result?.bestTranscription.formattedString  //9
				self.lblStatus.text = self.textSpoken
				isFinal = (result?.isFinal)!
	
				print("result =   \(String(describing: result))")
				print("isFinal =   \(isFinal)")
				
				if isFinal  {
                    if self.recordTimer.isValid {
                        self.recordTimer.invalidate()
                    }
					if self.textSpoken!.count > 0 {
						NetworkHandler.postDetectLanguageRequest(text: self.textSpoken!, success: { (successresponse) in
							
							self.sourceLang = (successresponse as? String)!
							print("successresponse \(successresponse)")
							self.getTranslation()
							
							
						}, failure: { (NetworkError) in
							print("NetworkError \(NetworkError)")

						})
					}
				}
                else {
                    if self.recordTimer.isValid {
                        self.resetTimer()
                    }
                    else {
                        self.startTimer()
                    }
                }
			}
			
			
			if error != nil || isFinal {  //10
				print("error =   \(String(describing: error))")

				self.audioEngine.stop()
				
				self.startAnimation(show: false)
				self.lblStatus.text = "TAP HERE TO START RECORDING"

				inputNode.removeTap(onBus: 0)
				
				self.recognitionRequest = nil
				self.recognitionTask = nil
				
				self.btnMicrophone.isEnabled = true
			}
			
		})
		
		let recordingFormat = inputNode.outputFormat(forBus: 0)  //11
		inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
			self.recognitionRequest?.append(buffer)
		}
		
		audioEngine.prepare()  //12
		
		do {
			try audioEngine.start()
		} catch {
			print("audioEngine couldn't start because of an error.")
		}
		
		self.lblStatus.text = "Say something, I'm listening!"
		
	}
	
	@objc func getTranslation() {
		let sourceMessage = TranslationMessage.init(message: textSpoken, translationLanguageType: TranslationLanguageType.Source, targetLanguage: TranslationHandler.sharedInstance.targetLanguageCode, sourceLanguage: sourceLang)
        TranslationHandler.sharedInstance.sourceLanguageCode = self.sourceLang!

		TranslationHandler.sharedInstance.translate(transMessage: sourceMessage, success: { (result) in
			print("result = \(result)")
			TranslationHandler.sharedInstance.addMessage(message: sourceMessage)
			TranslationHandler.sharedInstance.addMessage(message: TranslationHandler.sharedInstance.translatedMessage!)
			print(TranslationHandler.sharedInstance.translatedMessageRecord.count)
			self.performSegue(withIdentifier: "NavigateToVoiceTranslationDetailViewController", sender: nil)
		}, failure: { (err) in
			
		})
	}
	func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
		
		if available {
			self.btnMicrophone.isEnabled = true
		} else {
			self.btnMicrophone.isEnabled = false
		}
	}
	
	func addlayers() {
		let xWidth = self.view.bounds.size.width/2
		let xHeight = self.view.bounds.size.height/2
		let path1 = UIBezierPath(rect: self.view.bounds)
		let path2 = UIBezierPath(rect: CGRect.init(x: xWidth/2, y: xHeight/2, width: xWidth, height: xHeight))
		path1.append(path2)
		
		let xlayer = CAShapeLayer.init()
		xlayer.path = path1.cgPath
		xlayer.fillRule = CAShapeLayerFillRule.evenOdd
		xlayer.fillColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
		xlayer.opacity = 0.5
		self.view.layer.addSublayer(xlayer)
	}
	
	/*
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using segue.destination.
	// Pass the selected object to the new view controller.
	}
	*/
	
}
