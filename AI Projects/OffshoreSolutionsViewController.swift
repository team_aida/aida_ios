//
//  OffshoreSolutionsViewController.swift
//  AI Projects
//
//  Created by offshore_mac_1 on 26/02/19.
//  Copyright © 2019 Rajeev Lochan Ranga. All rights reserved.
//

import UIKit

class OffshoreSolutionsViewController: BaseController {
	
	@IBOutlet weak var btnGoToWebsite: UIButton!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
	override func viewDidLoad() {
		super.viewDidLoad()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        if revealViewController() != nil {
            self.menuButton.target = revealViewController()
            self.menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
		// Do any additional setup after loading the view.
	}
	
	override func viewWillAppear(_ animated: Bool) {
		self.HideNavigationBar(false)
	}
	
    @IBAction func buttonChat_action(_ sender: UIButton) {
        self.performSegue(withIdentifier: "NavigateToChatViewController", sender: nil)
    }
    
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		self.btnGoToWebsite.layer.shadowColor = #colorLiteral(red: 0.3176470697, green: 0.07450980693, blue: 0.02745098062, alpha: 1)
		self.btnGoToWebsite.layer.shadowOpacity = 1
		self.btnGoToWebsite.layer.shadowOffset = CGSize(width: 0, height: 15)
		self.btnGoToWebsite.layer.shadowRadius = self.btnGoToWebsite.frame.size.height / 2
		self.btnGoToWebsite.layer.shadowPath = UIBezierPath(rect: CGRect.init(x: 5, y: 10, width: self.btnGoToWebsite.frame.size.width-10, height: self.btnGoToWebsite.frame.size.height-20)).cgPath
		self.btnGoToWebsite.layer.shouldRasterize = true
		self.btnGoToWebsite.layer.cornerRadius = self.btnGoToWebsite.frame.size.height/2
	}
	
	@IBAction func swipeRight_action(_ sender: UISwipeGestureRecognizer) {
		self.performSegue(withIdentifier: "NavigateToVoiceTranslatorStartViewController", sender: nil)
	}
	
	
	@IBAction func swipeLeft_action(_ sender: UISwipeGestureRecognizer) {
		self.navigationController?.popViewController(animated: true)
	}
	@IBAction func buttonForward_action(_ sender: UIButton) {
		self.performSegue(withIdentifier: "NavigateToVoiceTranslatorStartViewController", sender: nil)
	}
	@IBAction func buttonBack_action(_ sender: UIButton) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func buttonGoToWebSite_action(_ sender: UIButton) {
		self.performSegue(withIdentifier: "PresentWebBrowserViewController", sender: nil)
	}
	
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "PresentWebBrowserViewController" {
			let webViewController = segue.destination as? WebBrowserViewController
			webViewController?.url_str = Constants.URL_OffshoreWebsite
		}
		else if segue.identifier == "NavigateToChatViewController" {
			let destVC = segue.destination as! ChatSupportViewController
			destVC.shouldShowBackButton = true
		}
	}
}
