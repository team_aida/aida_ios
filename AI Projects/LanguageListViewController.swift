//
//  LanguageListViewController.swift
//  AR_AI_Demo
//
//  Created by offshore_mac_1 on 06/02/19.
//  Copyright © 2019 offshore_mac_1. All rights reserved.
//

import UIKit

class LanguageListViewController: BaseController {

	@IBOutlet weak var LanguageTable: UITableView!
	
	override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
	@IBAction func action_CancelButton(_ sender: UIButton) {
		self.navigationController?.popViewController(animated: false)
	}
	
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LanguageListViewController : UITableViewDelegate, UITableViewDataSource {
	//MARK:- Table View Methods
	func numberOfSections(in tableView: UITableView) -> Int {
		
		return 1
	}
	
	//	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
	//		<#code#>
	//	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		
		return LanguageNames.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let cell = tableView.dequeueReusableCell(withIdentifier: "LanguageListCell", for: indexPath)
		
		let LanguageName : UILabel = cell.viewWithTag(2) as! UILabel
		let LanguageCode : UILabel = cell.viewWithTag(3) as! UILabel
		
		LanguageName.text = LanguageNames[indexPath.row]
		LanguageCode.text = LanguageCodes[indexPath.row]
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		print("Language Selected : ", LanguageNames[indexPath.row])
		
		USDF.set("\(LanguageNames[indexPath.row])", forKey: "SelectedLanguageName")
		USDF.set("\(LanguageCodes[indexPath.row])", forKey: "SelectedLanguage")
		
		self.navigationController?.popViewController(animated: false)
	}
}
