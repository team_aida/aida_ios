//
//  ProductListController.swift
//  AR_AI_Demo
//
//  Created by offshore_mac_1 on 28/01/19.
//  Copyright © 2019 offshore_mac_1. All rights reserved.
//

import UIKit

class ProductListController: BaseController {
	
	var imageBase64Str = String()
	var ProductListArray = Array<Any>()
	var LabelItemArray = Array<Any>()
	var TranslatedItemArray = Array<Any>()

	@IBOutlet weak var productListCollection: UICollectionView!
	@IBOutlet weak var productListHeaderView: UIView!
	
	
	//MARK:- View Did Load
	override func viewDidLoad() {
        super.viewDidLoad()
		
//		LoadData()
    }
	
	override func viewWillAppear(_ animated: Bool) {
		
		self.productListCollection.reloadData()
	}
	
	override func viewDidLayoutSubviews() {

	}
	/*
	func LoadData () {
//		self.StartAnimation()
		Webservices.shared.CloudVisionRequest_POST(imageBase64: imageBase64Str) { (responseStatus, responseData) in
//			self.StopAnimation()
			if responseStatus {
//				self.StartAnimation()
				self.AnalyzeCloudVisionResults(responseData, completion: { (status, message, resultData) in
					print("Vision Result Status : ", status)
//					self.StopAnimation()
					if status {
						
						if resultData["labelAnnotations"].count != 0 {
							print("Label Annotations : ", resultData["labelAnnotations"])
//							self.StartAnimation()
							self.AnalyzeLabelAnnotations(response: resultData, completion: { (responseStatus, responseResult, responseArray) in
//								self.StopAnimation()
								if responseStatus {
									
									print("Labels : ", responseResult)
//									self.StartAnimation()
									
									Webservices.shared.ThuisProductSearch(products: responseArray.joined(separator: ","), page_offset: 0, language: UserDefaults.standard.value(forKey: "SelectedLanguage") as! String, completion: { (requestStatus, requestResponse) in
//										self.StopAnimation()
										if requestStatus {

											print("Request POST Response Result : ", requestResponse)
											
											self.ProductListArray = self.getProductDetail(requestResponse["Data"] as! [Any], ThuisSearch: true)//requestResponse["Data"] as! [Any]
											self.LabelItemArray = (requestResponse["Translate"] as! String).components(separatedBy: ", ")
											print("Item Array : ", self.LabelItemArray)
											self.productListCollection.reloadData()
										}
										else {
//											self.showAlert(withTitle: "Alert!", message: "No Matching Products Found", okButton: "OK", cancelButton: "", completion: { (AlertResponse) in
//												if AlertResponse {
//
//												}
//											})
//											self.StartAnimation()
											Webservices.shared.GoogleCustomSearch(paramString: responseArray.joined(separator: ","), completion: { (searchStatus, searchResponse) in
												if searchStatus {
//													self.StopAnimation()
													self.ProductListArray = self.getProductDetail(searchResponse, ThuisSearch: false)
													self.LabelItemArray = (requestResponse["Translate"] as! String).components(separatedBy: ", ")
													print("Item Array : ", self.LabelItemArray)
													self.productListCollection.reloadData()
												}
											})
										}
									})
								}
							})
						}
					}
					else {
						print("Cloud Vision Result Message : ", message)
					}
				})
			}
		}
	}*/
	
	@IBAction func action_LanguageSelection () {
		
		let goToVC = self.storyboard?.instantiateViewController(withIdentifier: "LanguageListViewController") as! LanguageListViewController
		self.navigationController?.pushViewController(goToVC, animated: false)
	}
}


extension ProductListController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
	
	//MARK:- Collection View Methods
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return 1
	}
	
	func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {

		var header = UICollectionReusableView()
		if collectionView == productListCollection {

			header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "ProductListHeader", for: indexPath)
			let mainView = header.viewWithTag(1)!

			let topPartView = mainView.viewWithTag(2)!
			let SearchPartView = mainView.viewWithTag(3)!

			let languageButton = topPartView.viewWithTag(4) as! ActionButton
			let tryAgainCamera = topPartView.viewWithTag(5) as! UIButton

			let labelListCollection = SearchPartView.viewWithTag(6) as! UICollectionView
			
			let TranslatedPartView = mainView.viewWithTag(21)!
			let TranslateListCollection = TranslatedPartView.viewWithTag(22) as! UICollectionView
			
			mainView.layer.borderColor = UIColor.lightGray.cgColor
			mainView.layer.borderWidth = 1.0
			
			topPartView.layer.borderColor = UIColor.lightGray.cgColor
			SearchPartView.layer.borderColor = UIColor.lightGray.cgColor
			TranslatedPartView.layer.borderColor = UIColor.lightGray.cgColor
			
			languageButton.addTarget(self, action: #selector(action_LanguageSelection), for: .touchUpInside)
			languageButton.setTitle(USDF.value(forKey: "SelectedLanguageName") as? String, for: .normal)
			self.CornerRadius(languageButton, value: 0.5)
			self.AddDropImage(languageButton)
			
			tryAgainCamera.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
			
			labelListCollection.delegate = self
			labelListCollection.dataSource = self
			
			labelListCollection.reloadData()
			
			TranslateListCollection.delegate = self
			TranslateListCollection.dataSource = self
			
			TranslateListCollection.reloadData()
		}
		return header
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {

		if collectionView == productListCollection {
			return CGSize(width: MAINSCREEN_WIDTH, height: MAINSCREEN_HEIGHT * 0.19)
		}
		else {
			return CGSize.zero
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		if collectionView == productListCollection {
			print("Product List Array Count : ", self.ProductListArray.count)
			return self.ProductListArray.count
		}
		else {
			
			if collectionView.tag == 22 {
				print("Label Item Array Count : ", self.TranslatedItemArray.count)
				return self.TranslatedItemArray.count
			}
			else {
				print("Label Item Array Count : ", self.LabelItemArray.count)
				return self.LabelItemArray.count
			}
		}
		
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		
		if collectionView == productListCollection {
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductListCell", for: indexPath)
			
			print("Index Path Row : ", indexPath.row)
			let mainView : UIView = cell.viewWithTag(1)!
			let productImage : UIImageView = mainView.viewWithTag(2) as! UIImageView
			let productName : UILabel = mainView.viewWithTag(3) as! UILabel
			let productDescription : UILabel = mainView.viewWithTag(4) as! UILabel
			
			mainView.layer.borderColor = UIColor.lightGray.cgColor
			mainView.layer.borderWidth = 1.0
			
//			productImage.sd_setImage(with: URL(string: "\(BASE_IMAGE_URL)\((ProductListArray[indexPath.row] as? NSDictionary)?.value(forKey: "meal_image") as! String)"), placeholderImage: UIImage(named: ""))
//			productName.text = (ProductListArray[indexPath.row] as? NSDictionary)?.value(forKey: "meal_name_ar") as? String
//			productDescription.text = (ProductListArray[indexPath.row] as? NSDictionary)?.value(forKey: "meal_description_ar") as? String
			
			let Item = ProductListArray[indexPath.row] as! [String: Any]
			
			productImage.sd_setImage(with: URL(string: "\(Item["ThumbnailImage"] ?? "")"), placeholderImage: UIImage(named: ""))
			productName.text = Item["Title"] as? String
			productDescription.text = Item["Snippet"] as? String
			
			return cell
		}
		else {
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LabelItem", for: indexPath)
			
			print("Label Item Row : ", indexPath.row)
			let labelItem : UILabel = cell.viewWithTag(1) as! UILabel
			if collectionView.tag == 22 {
				labelItem.text = TranslatedItemArray[indexPath.row] as? String
			}
			else {
				labelItem.text = LabelItemArray[indexPath.row] as? String
			}
			
			return cell
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		if collectionView == productListCollection {
			return CGSize(width: MAINSCREEN_WIDTH * 0.45, height: MAINSCREEN_WIDTH * 0.55)
		}
		else {
			
			var wid : CGFloat = 0.0
			if collectionView.tag == 22 {
				wid = self.calculateLabelWidth(string: TranslatedItemArray[indexPath.row] as? String)
			}
			else {
				wid = self.calculateLabelWidth(string: LabelItemArray[indexPath.row] as? String)
			}
			
			return CGSize(width: wid + 30, height: collectionView.frame.size.height)
		}
		
	}
	
//	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//		<#code#>
//	}
}
