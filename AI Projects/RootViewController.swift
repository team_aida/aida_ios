//
//  RootViewController.swift
//  AI Projects
//
//  Created by Rajeev Lochan Ranga on 15/02/19.
//  Copyright © 2019 Rajeev Lochan Ranga. All rights reserved.
//

import UIKit

class RootViewController: UIViewController {
    
    @IBOutlet weak var view_ai_agile: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.createGradientLabel()
        
        self.perform(#selector(self.loadFirstView), with: nil, afterDelay: 0.5)
    }
    
    @objc func loadFirstView() {
		if UserDefaults.standard.value(forKey: Constants.kName) != nil && UserDefaults.standard.value(forKey: Constants.kEmail) != nil {
            
            //NavigateToHomeScreen
			self.performSegue(withIdentifier: "NavigateToRevealViewController", sender: nil)

		}
		else {
			self.performSegue(withIdentifier: "NavigateToSignUpViewController", sender: nil)

		}
    }
    
    func createGradientLabel() {
        // Create a gradient layer
        let gradient = CAGradientLayer()
        
        let color1 = UIColor.init(red: (117.0/255.0), green: (46.0/255.0), blue: (25.0/255.0), alpha: 1.0).cgColor
        
        // gradient colors in order which they will visually appear
        gradient.colors = [color1, UIColor.orange.cgColor]
        
        // Gradient from left to right
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        // set the gradient layer to the same size as the view
        gradient.frame = view.bounds
        
        // add the gradient layer to the views layer for rendering
        self.view_ai_agile.layer.addSublayer(gradient)
        
        let label = UILabel(frame: self.view_ai_agile.bounds)
        label.text = "THE AI AGILE"
        label.font = UIFont.boldSystemFont(ofSize: 30)
        label.textAlignment = .center
        self.view_ai_agile.addSubview(label)
        
        // Tha magic! Set the label as the views mask
        self.view_ai_agile.mask = label
        
    }
}
