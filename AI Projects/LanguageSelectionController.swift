//
//  LanguageSelectionController.swift
//  AR_AI_Demo
//
//  Created by offshore_mac_1 on 17/01/19.
//  Copyright © 2019 offshore_mac_1. All rights reserved.
//

import UIKit
import SwiftyJSON

class LanguageSelectionController: BaseController {
	
	@IBOutlet weak var MainView: UIView!
	@IBOutlet weak var selectLanguage: ActionButton!
	
	@IBOutlet weak var topView: UIView!
	
	
	let URLsession = URLSession.shared
	
	var binaryImageDataString = String()
	
	//MARK:- View Did Load
	override func viewDidLoad() {
		super.viewDidLoad()
		
//		RemoveStoredLanguageValues()
		self.checkCameraAutorization { (authResponse) in
			
		}
		
	}
	
	override func viewDidLayoutSubviews() {
		self.CornerRadius(MainView, value: 0.01)
		
		for view in self.topView.subviews {
			if view .isKind(of: ActionButton.self) {
				self.CornerRadius(view, value: 0.5)
			}
		}
		self.AddDropImage(selectLanguage)
	}
	
	override func viewWillAppear(_ animated: Bool) {
		if USDF.value(forKey: "SelectedLanguageName") != nil {
			selectLanguage.setTitle(USDF.value(forKey: "SelectedLanguageName") as? String, for: .normal)
		}
	}
	
	//MARK:- Button Actions
	@IBAction func action_OpenCamera(_ sender: UIButton) {
		if USDF.value(forKey: "SelectedLanguage") != nil {
			getImagesFromGallery()
		}
		else {
			self.showAlert(withTitle: "Alert!", message: "Please select a language", okButton: "OK", cancelButton: "") { (response) in
				if response {
					
				}
			}
		}
	}
	
	@IBAction func openLanguageSelection(_ sender: UIButton) {
		
		let goToVC = self.storyboard?.instantiateViewController(withIdentifier: "LanguageListViewController") as! LanguageListViewController
		self.navigationController?.pushViewController(goToVC, animated: false)
	}
	
	@IBAction func action_GoToHome(_ sender: UIButton) {
		//self.performSegue(withIdentifier: "UnwindToHome", sender: nil)
		self.GoBack(true)
//		self.navigationController?.popToRootViewController(animated: true)
//		self.StartAnimation()
//		self.perform(#selector(StopAnimation()), with: nil, afterDelay: 3.0)
	}
	
	//MARK:- Image Picker Controller Method
	func getImagesFromGallery () {
		
		self.checkCameraAutorization(completion: { (status) in
			if status == true {
				self.performSegue(withIdentifier: "HomeToCameraManager", sender: nil)
			}
			else {
				print("Camera Response is negative")
			}
		})
	}
	
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		
		if segue.identifier == "HomeToProductList" {
			let vc = segue.destination as? ProductListController
			vc?.imageBase64Str = binaryImageDataString
		}
	}
}



// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
	switch (lhs, rhs) {
	case let (l?, r?):
		return l < r
	case (nil, _?):
		return true
	default:
		return false
	}
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
	switch (lhs, rhs) {
	case let (l?, r?):
		return l > r
	default:
		return rhs < lhs
	}
}
