//
//  RRSparklingEffectView.swift
//  SparklingAnimation
//
//  Created by Lions Of Mirzapur on 17/05/19.
//  Copyright © 2019 Lions Of Mirzapur. All rights reserved.
//

import UIKit

class RRSparklingEffectView: UIView {
    
    var positions : [CGPoint]?
    var Sparkles : [SparkleView]?
    var animationTimer: Timer?
    
    
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
        self.frame = super.frame
        self.center = super.center
        
        self.animationTimer = Timer.scheduledTimer(timeInterval: 2.3, target: self, selector: #selector(self.startAnimation), userInfo: nil, repeats: true)
        self.animationTimer?.fire()

    }
    
    public final func showSparklingEffect() {
        self.isHidden = false
    }
    
    public final func hideSparklingEffect() {
        self.isHidden = true
        self.stopAnimation()
        self.removeFromSuperview()
    }
    
    func stopAnimation() {
        self.animationTimer?.invalidate()
    }
    @objc func startAnimation() {
        self.preparePattern()
        self.Sparkles = [SparkleView]()
        for point in self.positions! {
            self.createSparkleView(atPoint: point)
        }
        self.perform(#selector(self.animationFirst), with: nil, afterDelay: 1)
        
        self.perform(#selector(self.animationSecond), with: nil, afterDelay: 1.6)
        
        self.perform(#selector(self.animationthird), with: nil, afterDelay: 2.2)
  
    }
    
    @objc func animationFirst() {
        self.show(sparkles: Array(self.Sparkles![0...4]))
    }
    @objc func animationSecond() {
        
        self.show(sparkles: Array(self.Sparkles![5...8]))
        
        
    }
    @objc func animationthird() {
        self.show(sparkles: Array(self.Sparkles![9...14]))
        
    }
    func show(sparkles: [SparkleView]) {
        for sView in sparkles {
            
            self.addSubview(sView)
            sView.show()
        }
    }
    func createSparkleView(atPoint point:CGPoint)  {
        let sView = SparkleView()
        sView.center = point
        self.Sparkles?.append(sView)
    }
    func preparePattern() {
        let positionNumber = Int.random(in: 15..<21) + 1
        let xMin = 30
        let xMax = Int(self.bounds.size.width - 30)
        let yMin = 30
        let yMax = Int(self.bounds.size.height - 30)
        self.positions = [CGPoint]()
        for i in 0..<positionNumber {
            let xPos = Int.random(in: xMin..<xMax) + 1
            let yPos = Int.random(in: yMin..<yMax) + 1
            
            let point = CGPoint(x: CGFloat(xPos), y: CGFloat(yPos))
            self.positions?.append(point)
        }
    }
}
