//
//  ObjectRecognitionStartDemoViewController.swift
//  AI Projects
//
//  Created by Rajeev Lochan Ranga on 22/02/19.
//  Copyright © 2019 Rajeev Lochan Ranga. All rights reserved.
//

import UIKit
//import FLAnimatedImage

class ObjectRecognitionStartDemoViewController: BaseController {

    @IBOutlet weak var view_helpInfo: UIView!
    @IBOutlet weak var btnStartDemo: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var view_popUp: UIView!
    @IBOutlet weak var view_blurEffect: UIVisualEffectView!
	@IBOutlet weak var imgView_help: FLAnimatedImageView!
    @IBOutlet weak var menuButton: UIBarButtonItem!
	
	
	//MARK:- View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
		
        if revealViewController() != nil {
            self.menuButton.target = revealViewController()
            self.menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        let path1 : String = Bundle.main.path(forResource: "object", ofType: "gif")!
        let url = URL(fileURLWithPath: path1)
        let gifData = try? Data.init(contentsOf: url)
        let imageData1 =  FLAnimatedImage(animatedGIFData: gifData)
        self.imgView_help.animatedImage = imageData1
        
        if revealViewController() != nil {
            self.menuButton.target = revealViewController()
            self.menuButton.action = "revealToggle:"
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
	
	override func viewWillAppear(_ animated: Bool) {
		
		self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
		self.navigationController?.navigationBar.shadowImage = UIImage()
		self.navigationController?.navigationBar.isTranslucent = true
		self.navigationController?.view.backgroundColor = .clear
		
		self.HideNavigationBar(false)
	}
    
    
    @IBAction func buttonChat_action(_ sender: UIButton) {
        self.performSegue(withIdentifier: "NavigateToChatViewController", sender: nil)
    }
    
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		self.view_helpInfo.layer.cornerRadius = 10
		
		self.btnClose.layer.cornerRadius = self.btnClose.frame.size.height/2
		
		self.btnStartDemo.layer.shadowColor = #colorLiteral(red: 0.3176470697, green: 0.07450980693, blue: 0.02745098062, alpha: 1)
		self.btnStartDemo.layer.shadowOpacity = 1
		self.btnStartDemo.layer.shadowOffset = CGSize(width: 0, height: 15)
		self.btnStartDemo.layer.shadowRadius = self.btnStartDemo.frame.size.height / 2
		self.btnStartDemo.layer.shadowPath = UIBezierPath(rect: CGRect.init(x: 5, y: 10, width: self.btnStartDemo.frame.size.width-10, height: self.btnStartDemo.frame.size.height-20)).cgPath
		self.btnStartDemo.layer.shouldRasterize = true
		self.btnStartDemo.layer.cornerRadius = self.btnStartDemo.frame.size.height/2
	}
	
    @IBAction func buttonBack_action(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
	
	@IBAction func swipeLeft_action(_ sender: UISwipeGestureRecognizer) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func buttonForward_action(_ sender: UIButton) {
		self.performSegue(withIdentifier: "NavigateToFreshShoping", sender: nil)
	}
	
	@IBAction func swipeRight_action(_ sender: UISwipeGestureRecognizer) {
		self.performSegue(withIdentifier: "NavigateToFreshShoping", sender: nil)
	}
	
	@IBAction func action_StartDemo(_ sender: UIButton) {
		SelectCameraMode = CameraMode.Other
	}
	
    @IBAction func buttonClose_action(_ sender: UIButton) {
       
        UIView.animate(withDuration: 0.33, animations: {
            self.view_blurEffect.alpha = 0
        }, completion: { (completed) in
            
        })
        UIView.animate(withDuration: 0.33, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 10, options: UIView.AnimationOptions(rawValue: 0), animations: {

             self.view_helpInfo.center = CGPoint(x: self.view_popUp.center.x, y: self.view_popUp.frame.size.height + self.view_helpInfo.frame.size.height/2)

        }, completion: { (completed) in
            self.view_popUp.isHidden = true
            self.view_helpInfo.isHidden = true
        })
    }
    
    @IBAction func buttonInfo_action(_ sender: UIBarButtonItem) {
        self.view_blurEffect.alpha = 0.0
        self.view_helpInfo.center = CGPoint(x: self.view_popUp.center.x, y: self.view_popUp.frame.size.height + self.view_helpInfo.frame.size.height/2)
        self.view_popUp.isHidden = false
        self.view_helpInfo.isHidden = false
        
        UIView.animate(withDuration: 0.33) {
            self.view_blurEffect.alpha = 0.8
        }
        UIView.animate(withDuration: 0.33, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 10, options: UIView.AnimationOptions(rawValue: 0), animations: {
            self.view_helpInfo.center  = CGPoint(x: self.view_popUp.center.x, y: self.view_popUp.center.y - self.view_helpInfo.frame.size.height/4)
            
        }, completion: { (completed) in
            
        })
    }
	
	// MARK: - Navigation
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "NavigateToChatViewController" {
			let destVC = segue.destination as! ChatSupportViewController
			destVC.shouldShowBackButton = true
		}
	}


}

