//
//  ObjectRecognitionTrainingStartDemoViewController.swift
//  AI Projects
//
//  Created by Rajeev Lochan Ranga on 25/02/19.
//  Copyright © 2019 Rajeev Lochan Ranga. All rights reserved.
//

import UIKit

class ObjectRecognitionTrainingStartDemoViewController: BaseController {
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var btnStartDemo: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var view_blurEffect: UIVisualEffectView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if revealViewController() != nil {
            self.menuButton.target = revealViewController()
            self.menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
		
    self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        // Do any additional setup after loading the view.
    }
	
	override func viewWillAppear(_ animated: Bool) {
		self.HideNavigationBar(false)
	}
    
    @IBAction func buttonChat_action(_ sender: UIButton) {
        self.performSegue(withIdentifier: "NavigateToChatViewController", sender: nil)
    }
    override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		
		
		self.btnStartDemo.layer.shadowColor = #colorLiteral(red: 0.3176470697, green: 0.07450980693, blue: 0.02745098062, alpha: 1)
		self.btnStartDemo.layer.shadowOpacity = 1
		self.btnStartDemo.layer.shadowOffset = CGSize(width: 0, height: 15)
		self.btnStartDemo.layer.shadowRadius = self.btnStartDemo.frame.size.height / 2
		self.btnStartDemo.layer.shadowPath = UIBezierPath(rect: CGRect.init(x: 5, y: 10, width: self.btnStartDemo.frame.size.width-10, height: self.btnStartDemo.frame.size.height-20)).cgPath
		self.btnStartDemo.layer.shouldRasterize = true
		self.btnStartDemo.layer.cornerRadius = self.btnStartDemo.frame.size.height/2
	}
	
	@IBAction func swipeLeft_action(_ sender: UISwipeGestureRecognizer) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func buttonForward_action(_ sender: UIButton) {
		self.performSegue(withIdentifier: "NavigateToObjectRecognition", sender: nil)
	}
	
	@IBAction func swipeRight_action(_ sender: UISwipeGestureRecognizer) {
		self.performSegue(withIdentifier: "NavigateToObjectRecognition", sender: nil)
	}
	@IBAction func buttonBack_action(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
	@IBAction func action_StartDemo(_ sender: UIButton) {
		SelectCameraMode = CameraMode.Prediction
		self.PushViewController(toVC: "CameraManagerController", mine: self)
	}
	
	// MARK: - Navigation
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "NavigateToChatViewController" {
			let destVC = segue.destination as! ChatSupportViewController
			destVC.shouldShowBackButton = true
		}
	}

    
}
