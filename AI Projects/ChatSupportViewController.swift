//
//  ChatSupportViewController.swift
//  AI Projects
//
//  Created by Rajeev Lochan Ranga on 07/03/19.
//  Copyright © 2019 Rajeev Lochan Ranga. All rights reserved.
//

import UIKit
import SendBirdSDK


class ChatSupportViewController: UIViewController {

    @IBOutlet weak var tableView_chat: UITableView!
    @IBOutlet weak var txtView_chat: UITextView!
	
    @IBOutlet weak var chatViewHeightConstaint: NSLayoutConstraint!
    @IBOutlet weak var bottomMargin: NSLayoutConstraint!
    
    var previousMessages = [SBDBaseMessage]()
    
    var lastMessageHeight: CGFloat = 0
    var scrollLock: Bool = false
    var stopMeasuringVelocity: Bool = true
    var lastOffset: CGPoint = CGPoint(x: 0, y: 0)
    var lastOffsetCapture: TimeInterval = 0
    var isScrollingFast: Bool = false
    var initialLoading: Bool = true
	var shouldShowBackButton: Bool?
	
    private var tapGesture: UITapGestureRecognizer?
    private var keyboardShown: Bool = false
	@IBOutlet weak var btnMenu: UIBarButtonItem!
	@IBOutlet weak var btnBack: UIButton!
	

    
    var viewModel = ChatViewModel.init()
    
    func loadchat() {
        if !isViewLoaded {
            return
        }
     
        
        self.viewModel.loadMessages(onSuccess: { (prvMessages) in
            self.previousMessages = prvMessages
            self.tableView_chat.reloadData()

        }, onFailure: { (error) in
            
        })
       
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadchat()
        SBDMain.add(self as SBDConnectionDelegate, identifier: Constants.SendbirdConnectionDelegate)
        SBDMain.add(self as SBDChannelDelegate, identifier: Constants.SendbirdConnectionDelegate)

        self.tableView_chat.rowHeight = UITableView.automaticDimension
        self.tableView_chat.estimatedRowHeight = 56
		
		if shouldShowBackButton == true {
			self.btnBack.setImage(#imageLiteral(resourceName: "orange_back"), for: UIControl.State.normal)
			self.btnBack.addTarget(self, action: #selector(self.backButton_action), for: UIControl.Event.touchUpInside)
		} else {
			self.btnBack.setImage(#imageLiteral(resourceName: "menu_icon"), for: UIControl.State.normal)
			if revealViewController() != nil {
				self.btnBack.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: UIControl.Event.touchUpInside)
				view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
			}
		}
		
    self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
		
		 
        self.lastMessageHeight = 0
        self.txtView_chat.contentInset = UIEdgeInsets(top: 12, left: 8, bottom: 12, right: 8)
        self.txtView_chat.layer.cornerRadius = self.txtView_chat.frame.size.height/2

    }
	
	@objc func backButton_action() {
		self.navigationController?.popViewController(animated: true)
	}
	
    func updateChat(message: String) {
        let sentMessage = OutgoingMessageModel()
        
        sentMessage.createdAt = Int64(NSDate().timeIntervalSince1970 * 1000)
        sentMessage.message = message
        
        self.previousMessages.append(sentMessage)
        self.viewModel.previousMessages?.append(sentMessage)

        DispatchQueue.main.async {
            self.tableView_chat.reloadData()
            DispatchQueue.main.async {
                self.scrollChatViewToBottom(force: true)
            }
        }
        
    }
    @IBAction func buttonSendMessage_action(_ sender: UIButton) {
        let msg = CommontMethods.trimString(str: self.txtView_chat.text)
        self.updateChat(message:msg)
        self.txtView_chat.text = ""

        self.viewModel.sendMessage(message: msg, onSuccess: { (sentMessage) in
            if self.previousMessages.count <= 1 {
                self.viewModel.sendAdminMessage(message: "Please wait, we are assigning an agent for you", onSuccess: { (result) in
                    self.reloadData()
                }, onFailure: { (error) in
                    print("Error while sending Admin message :- \(error)")
                })
            }
            else {
                self.reloadData()
            }
            

        }) { (error) in
            
        }
    }
    func reloadData() {
        self.viewModel.getPreviousMessages(onSuccess: { (prevMeessages) in
            self.previousMessages = prevMeessages
            self.tableView_chat.reloadData()

        }) { (error) in
            print("Error in sending message: - \(String(describing: error))")

        }
    }
    @IBAction func buttonBack_action(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Keyboard Notification methods
    @objc private func keyboardDidShow(notification: Notification) {
        self.keyboardShown = true
        let keyboardInfo = notification.userInfo
        let keyboardFrameBegin = keyboardInfo?[UIResponder.keyboardFrameEndUserInfoKey]
        let keyboardFrameBeginRect = (keyboardFrameBegin as! NSValue).cgRectValue

            let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber
        self.bottomMargin.constant = -keyboardFrameBeginRect.size.height

            UIView.animate(withDuration: TimeInterval(duration?.doubleValue ?? 0.0), delay: 0, options: .curveEaseInOut, animations: {
                self.view.layoutIfNeeded()
                self.stopMeasuringVelocity = true
                self.scrollChatViewToBottom(force: false)
            }) { finished in
            }
        if !(self.tapGesture != nil) {
                self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
            
            }
            
        self.view.addGestureRecognizer(self.tapGesture!)
        
    }
    
    @objc private func keyboardDidHide(notification: Notification) {
        self.keyboardShown = false
        self.bottomMargin.constant = 0

        let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber
        UIView.animate(withDuration: TimeInterval(duration?.doubleValue ?? 0.0), delay: 0, options: .curveEaseInOut, animations: {
        self.view.layoutIfNeeded()
            
        }) { finished in
        }
        self.view.removeGestureRecognizer(self.tapGesture!)

    }
    
    func hideKeyboardWhenFastScrolling() {
        if self.keyboardShown == false {
            return
        }
        
        DispatchQueue.main.async {
            self.bottomMargin.constant = 0
            self.view.layoutIfNeeded()
            self.scrollChatViewToBottom(force: false)
        }
        self.view.endEditing(true)
    }

    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    func adjustFrames() {
        
        var textFrame: CGRect = self.txtView_chat.frame
        if (self.txtView_chat.contentSize.height >= 35) && (self.txtView_chat.contentSize.height <= (0.25 * self.view.frame.size.height)) {
            textFrame.size.height = self.txtView_chat.contentSize.height

        self.txtView_chat.frame = textFrame
        
        self.chatViewHeightConstaint.constant = textFrame.size.height + 16
            self.txtView_chat.layer.cornerRadius = 17.5
        }
        else { //if (self.txtView_chat.frame.size.height < 35){
            self.txtView_chat.layer.cornerRadius = self.txtView_chat.frame.size.height/2
            textFrame.size.height = 35
            self.txtView_chat.frame = textFrame
             self.chatViewHeightConstaint.constant = 50
        }
        self.view.layoutIfNeeded()
        self.view.setNeedsLayout()
    }
    
    
    func scrollChatViewToBottom(force: Bool) {
        if self.previousMessages.count == 0 {
            return
        }
        
        if self.scrollLock == true && force == false {
            return
        }
        
        self.tableView_chat.scrollToRow(at: IndexPath.init(row: self.previousMessages.count - 1, section: 0), at: UITableView.ScrollPosition.bottom, animated: false)
    }
    
    func scrollToPosition(position: Int) {
        if self.previousMessages.count == 0 {
            return
        }
        
        self.tableView_chat.scrollToRow(at: IndexPath.init(row: position, section: 0), at: UITableView.ScrollPosition.top, animated: false)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}
//MARK: - UITableViewDataSource
extension ChatSupportViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.previousMessages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let msg = self.previousMessages[indexPath.row]
        var shouldShowDateView = false

        let currentMsgDate = CommontMethods.timeStampFromInt(value: Int(msg.createdAt))
        if indexPath.row > 0 {
        let prevMsg = self.previousMessages[indexPath.row - 1]
        let prevMsgDate = CommontMethods.timeStampFromInt(value: Int(prevMsg.createdAt))
            
            
            if prevMsgDate.date != currentMsgDate.date || prevMsgDate.month != currentMsgDate.month || prevMsgDate.year != currentMsgDate.year {
                shouldShowDateView = true
            }
            else {
                shouldShowDateView = false
            }
        }
        else {
            shouldShowDateView = true

        }
       
        if msg is SBDUserMessage {
            
            let message = msg as! SBDUserMessage
            let sender = message.sender
            if sender?.userId == SBDMain.getCurrentUser()?.userId {
                let cell  = tableView.dequeueReusableCell(withIdentifier: "UserMessageTableViewCell") as! UserMessageTableViewCell
                
                cell.chatContainerView.layer.cornerRadius = 12
                cell.lbl_message.text = message.message
                cell.lbl_timeStamp.text = CommontMethods.timeStamp(forDate: Int(message.createdAt))
                cell.lbl_MessageDate.text = currentMsgDate.date + " " + currentMsgDate.month + ", " + currentMsgDate.year
                
                cell.dateView.layer.cornerRadius = cell.dateView.frame.size.height/2

                
                if shouldShowDateView {
                    cell.dateView.isHidden = false
                    cell.dateView_heightConstraint.constant = 20
                }
                else {
                    cell.dateView.isHidden = true
                    cell.dateView_heightConstraint.constant = 0
                }
                cell.contentView.setNeedsLayout()
                cell.contentView.layoutIfNeeded()
                return cell
            }
            else {
                let message = msg as! SBDUserMessage
                
                let cell  = tableView.dequeueReusableCell(withIdentifier: "AgentMessageTableViewCell") as! AgentMessageTableViewCell
                cell.chatContainerView.layer.cornerRadius = 12
                cell.lbl_message.text = message.message
                cell.lbl_timeStamp.text = CommontMethods.timeStamp(forDate: Int(message.createdAt))
                cell.dateView.layer.cornerRadius = cell.dateView.frame.size.height/2

                cell.lbl_MessageDate.text = currentMsgDate.date + " " + currentMsgDate.month + ", " + currentMsgDate.year
                if shouldShowDateView {
                    cell.dateView.isHidden = false
                    cell.dateView_heightConstraint.constant = 20
                }
                else {
                    cell.dateView.isHidden = true
                    cell.dateView_heightConstraint.constant = 0
                }
                cell.contentView.setNeedsLayout()
                cell.contentView.layoutIfNeeded()

                return cell
            }
        }
        else if msg is SBDAdminMessage {
            let message = msg as! SBDAdminMessage

            let cell  = tableView.dequeueReusableCell(withIdentifier: "AdminMessageTableViewCell") as! AdminMessageTableViewCell
            cell.lbl_message.text = message.message
            cell.messageView.layer.cornerRadius = cell.messageView.frame.size.height/2
            return cell
        }
        else if msg is OutgoingMessageModel {
            
            let messageModel = msg as! OutgoingMessageModel
            
            let cell  = tableView.dequeueReusableCell(withIdentifier: "UserMessageTableViewCell") as! UserMessageTableViewCell
            
            cell.chatContainerView.layer.cornerRadius = 12
            cell.lbl_message.text = messageModel.message
            cell.lbl_timeStamp.text = CommontMethods.timeStamp(forDate: Int(messageModel.createdAt))
            cell.lbl_MessageDate.text = currentMsgDate.date + " " + currentMsgDate.month + ", " + currentMsgDate.year
            
            cell.dateView.layer.cornerRadius = cell.dateView.frame.size.height/2
            
            
            if shouldShowDateView {
                cell.dateView.isHidden = false
                cell.dateView_heightConstraint.constant = 20
            }
            else {
                cell.dateView.isHidden = true
                cell.dateView_heightConstraint.constant = 0
            }
            cell.contentView.setNeedsLayout()
            cell.contentView.layoutIfNeeded()
            return cell
            
        }
        return UITableViewCell()
    }
}

//MARK: - UITableViewDelegate
extension ChatSupportViewController: UITableViewDelegate {
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        let height: CGFloat = 56.0
//        if self.previousMessages.count > 0 && self.previousMessages.count - 1 == indexPath.row {
//            self.lastMessageHeight = height
//        }
//
//        return height
//    }
}

//MARK: - SBDConnectionDelegate
extension ChatSupportViewController: SBDConnectionDelegate {
    
    func didStartReconnection() {
        
    }
    func didSucceedReconnection() {
        
    }
    
    func didFailReconnection() {
        self.viewModel.reconnect(onSuccess: { (user) in
            self.viewModel.user = user
        }) { (error) in
         //   if error {
                print("Error in connection : - \(error.description)")

          //  }
        }
    }
    
    func didCancelReconnection() {
        
    }
}

//MARK: - SBDChannelDelegate
extension ChatSupportViewController: SBDChannelDelegate {
   
    func channel(_ sender: SBDBaseChannel, didReceive message: SBDBaseMessage) {
        if message is SBDUserMessage {
            // Do something when the received message is a UserMessage.
            DispatchQueue.main.async {
                UIView.setAnimationsEnabled(false)
                self.viewModel.previousMessages?.append(message)
                self.previousMessages.append(message)
                self.tableView_chat.reloadData()
                UIView.setAnimationsEnabled(true)
                DispatchQueue.main.async {
                    self.scrollChatViewToBottom(force: false)
                }
            }
            
        }
        else if message is SBDFileMessage {
            // Do something when the received message is a FileMessage.
        }
        else if message is SBDAdminMessage {
            // Do something when the received message is an AdminMessage.
        }
    }
}

//MARK: - UITextViewDelegate
extension ChatSupportViewController : UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        
    }
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if textView.text == Constants.placeholderString {
            textView.text = ""
        }
        return true
    }
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        if textView.text == "" {
            textView.text = Constants.placeholderString
        }
        return true
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        self.adjustFrames()
        return true
    }
}

// MARK: - UIScrollViewDelegate

extension ChatSupportViewController : UIScrollViewDelegate{
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.stopMeasuringVelocity = false
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        self.stopMeasuringVelocity = true
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == self.tableView_chat {
            if self.stopMeasuringVelocity == false {
                let currentOffset = scrollView.contentOffset
                let currentTime = NSDate.timeIntervalSinceReferenceDate

                let timeDiff = currentTime - self.lastOffsetCapture
                if timeDiff > 0.1 {
                    let distance = currentOffset.y - self.lastOffset.y
                    let scrollSpeedNotAbs = distance * 10 / 1000
                    let scrollSpeed = abs(scrollSpeedNotAbs)
                    if scrollSpeed > 0.5 {
                        self.isScrollingFast = true
                    }
                    else {
                        self.isScrollingFast = false
                    }

                    self.lastOffset = currentOffset
                    self.lastOffsetCapture = currentTime
                }

                if self.isScrollingFast {
                        self.hideKeyboardWhenFastScrolling()
                }
            }

            if scrollView.contentOffset.y + scrollView.frame.size.height + self.lastMessageHeight < scrollView.contentSize.height {
                self.scrollLock = true
            }
            else {
                self.scrollLock = false
            }

            if scrollView.contentOffset.y == 0 {
                if self.previousMessages.count > 0 && self.initialLoading == false {
                      //  self.loadMoreMessage(view: self)
                }
            }
        }
        
    }
}
