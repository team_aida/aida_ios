//
//  NetworkHandler.swift
//  AI Projects
//
//  Created by Rajeev Lochan Ranga on 21/02/19.
//  Copyright © 2019 Rajeev Lochan Ranga. All rights reserved.
//

import Foundation
import Alamofire
import JWT
import CryptoSwift

class NetworkHandler  {
	
	class func postTranslationRequest(parameters: Parameters?, success: @escaping (Any) -> Void, failure: @escaping (NetworkError) -> Void) {

        if (Network.isAvailable) {
         
            
            Alamofire.request(URL(string: Constants.URL_getTranslationText)!, method: .post, parameters: parameters).validate().responseJSON { (response)  in
                print("Response : \(response)")
                if (response.result.isSuccess) {
                    
                    let UserRole = response.result.value as! NSDictionary
                    
                    let jwtToken = UserRole["jwt"] as! String
                    
                    do {
                        let claims: ClaimSet = try JWT.decode(jwtToken, algorithm: .hs256(jwtSecurityKey.data(using: .utf8)!))
                        
                        print("Claims Status : ",claims["status"] as! Int)
                        print("Claims Message : ",claims["translate"] as! String)
                        print("Claims Data : ",claims)
                        
                        if (claims["status"] as! Int == 1) {
                            if let translatedText = claims["translate"] as? String {
                                success(translatedText)
                            }
                        }
                        else {
                            let errorMessage = claims["messsage"] as! String
                            let networkError = NetworkError(status: 4004, message: errorMessage)
                            failure(networkError)
                        }
                        
                    }
                    catch {
                        print("Failed to decode JWT: \(error)")
                    }
                }
            }
        }
		else {
			let networkError = NetworkError(status: 4004, message: "Internet not available")
			failure(networkError)
		}
	}
    
    
	class func postRequest(url: String, parameters: Parameters?, success: @escaping (Any) -> Void, failure: @escaping (NetworkError) -> Void) {
		
		if Network.isAvailable {
			let headers: HTTPHeaders = ["x-app-id":"8c4b5baf",
										"x-app-key":"b2428f8e50fc0aa811f3fee3d7fe11dc"]
			let manager = Alamofire.SessionManager.default
			manager.session.configuration.timeoutIntervalForRequest = 20
			print(url)
			print(parameters!)
			manager.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200..<600).responseJSON { (response) -> Void in
				print(response)
				if let result = response.result.value as? [String:Any] {
					if let status = result["status"] as? Int {
						if status == 1 {
							success(result)
						}
						else {
							let networkError = NetworkError(status: status, message: result["message"] as! String)
							failure(networkError)
						}
					}
				}
			}
		}
		else {
			let networkError = NetworkError(status: 4004, message: "Internet not available")
			failure(networkError)
		}
	}
	
	class func postDetectLanguageRequest(text: String, success: @escaping (Any) -> Void, failure: @escaping (NetworkError) -> Void) {
		
		if Network.isAvailable {
			
			let range = text.rangeOfCharacter(from: CharacterSet.whitespacesAndNewlines)
			var urlString :String!
			
			if range != nil {
				urlString  = text.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
				
			}
			else {
				urlString = text
			}
			let url = "https://translation.googleapis.com/language/translate/v2/detect?q=" + urlString + "&key=AIzaSyBSYmq5qYmub4GH86n9LNLP4_7zng6mqZ0"
			let manager = Alamofire.SessionManager.default
			manager.session.configuration.timeoutIntervalForRequest = 20
			print(url)
			manager.request(url, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: nil).validate(statusCode: 200..<600).responseJSON { (response) -> Void in
				print(response)
				if let result = response.result.value as? [String:Any] {
					if let data = result["data"] as? [String:Any] {
						if let detections = data["detections"] as? [Any] {
							if	let arr1 = detections.first as? [Any] {
								if let arr2 = arr1.first as? [String:Any] {
									let languageCode = arr2["language"]
									success(languageCode!)

								}
							}
						}
					}
					else {
						let networkError = NetworkError(status: 403, message: result["message"] as! String)
						failure(networkError)
					}
				}
				
			}
		}
		else {
			let networkError = NetworkError(status: 4004, message: "Internet not available")
			failure(networkError)
		}
	}
	
	
	class func getRequest(url: String, success: @escaping (Any) -> Void, failure: @escaping (NetworkError) -> Void) {
		
		if Network.isAvailable {
			let headers: HTTPHeaders = ["x-app-id":"8c4b5baf",
										"x-app-key":"b2428f8e50fc0aa811f3fee3d7fe11dc"]
			let manager = Alamofire.SessionManager.default
			manager.session.configuration.timeoutIntervalForRequest = 20.0
			
			print(url)
			
			manager.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) -> Void in
				
				print(response)
				if let result = response.result.value as? [String:Any] {
					if let message = result["message"] as? String {
						if message.count != 0 {
							let networkError = NetworkError(status: 4005, message: result["message"] as! String)
							failure(networkError)
						}
					}
					else {
						success(result)
					}
				}
			}
		}
		else {
			let networkError = NetworkError(status: 4004, message: "Internet not available")
			failure(networkError)
		}
	}
    class func sendAdminMessage(message: String,channel_url: String, channel_type:String, onSuccess success: @escaping (Any) -> Void, onFailure failure: @escaping (NetworkError) -> Void) {
        if Network.isAvailable {
            let baseURL = "https://api.sendbird.com/v3/" + channel_type + "/" + channel_url + "/messages"
            
            let headers:HTTPHeaders = [
                "Content-Type": "application/json, charset=utf8",
                "Api-Token": "7834a151632fcb2cf03ca25aeb669edc05d23c46",
            ]
           
            let params: Parameters = [  "message_type": "ADMM",
                                        "message": message,
                                        "custom_type": "notice",
                                        "is_silent": true ]
            
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 20.0
            
            manager.request(baseURL, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) -> Void in
                
                print(response)
                if let result = response.result.value as? [String:Any] {
                    if (result["error"] as? Bool) ==  true {
                        let responseError = NetworkError(status: result["code"] as! Int, message: result["message"] as! String)
                        failure(responseError)
                    }
                    else {
                        success(result)
                    }
                }
            }
            
            
        }
        else {
            let networkError = NetworkError(status: 4004, message: "Internet not available")
            failure(networkError)
        }
        
    }
	class func getSignUpRequest(name: String,emailId: String, success: @escaping (Any) -> Void, failure: @escaping (NetworkError) -> Void) {
		
		if Network.isAvailable {
			
			let headers: HTTPHeaders = ["Content-Type":"application/json",//"application/x-www-form-urlencoded",//"application/json; charset=utf-8",
										"user":"96ca4e8786e6dd141495b6d16e7f5dbd-us20",
										"Authorization":"Basic dXNlcjo5NmNhNGU4Nzg2ZTZkZDE0MTQ5NWI2ZDE2ZTdmNWRiZC11czIw"]
			
			let params: Parameters = ["email_address":emailId,
									  "status":"subscribed",
									  "merge_fields": [
										"FNAME": name
				] ]
			
			let manager = Alamofire.SessionManager.default
			manager.session.configuration.timeoutIntervalForRequest = 20.0
			
			manager.request(Constants.URLRegister, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) -> Void in
				
				print(response)
				if let result = response.result.value as? [String:Any] {
					if let message = result["message"] as? String {
						if message.count != 0 {
							let networkError = NetworkError(status: 4005, message: result["message"] as! String)
							failure(networkError)
						}
					}
					else {
						success(result)
					}
				}
			}
		}
		else {
			let networkError = NetworkError(status: 4004, message: "Internet not available")
			failure(networkError)
		}
	}
	
	class func getMemberDetailsRequest(name: String,emailId: String, success: @escaping (Any) -> Void, failure: @escaping (NetworkError) -> Void) {
		
		if Network.isAvailable {
			
			let headers: HTTPHeaders = ["Content-Type":"application/json",//"application/x-www-form-urlencoded",//"application/json; charset=utf-8",
				"user":"96ca4e8786e6dd141495b6d16e7f5dbd-us20",
				"Authorization":"Basic dXNlcjo5NmNhNGU4Nzg2ZTZkZDE0MTQ5NWI2ZDE2ZTdmNWRiZC11czIw"]
			let subscriber_hash = emailId.md5()
			let api_url =  Constants.URLGetMemberDetails + "/" + subscriber_hash
			let params: Parameters = [:] //"email_address":emailId,
			//"status":"subscribed",// "merge_fields": [
			//"FNAME": name
			//]
			
			let manager = Alamofire.SessionManager.default
			manager.session.configuration.timeoutIntervalForRequest = 20.0
			
			manager.request(api_url, method: .get, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) -> Void in
				
				print(response)
				if let result = response.result.value as? [String:Any] {
					if let message = result["message"] as? String {
						if message.count != 0 {
							let networkError = NetworkError(status: 4005, message: result["message"] as! String)
							failure(networkError)
						}
					}
					else {
						success(result)
					}
				}
			}
		}
		else {
			let networkError = NetworkError(status: 4004, message: "Internet not available")
			failure(networkError)
		}
	}
}

struct NetworkError {
	var status: Int = 400
	var message: String = "Please Try Again"
}
