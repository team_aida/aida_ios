//
//  PredictionItem.swift
//  AI Projects
//
//  Created by Lions Of Mirzapur on 20/05/19.
//  Copyright © 2019 Rajeev Lochan Ranga. All rights reserved.
//

import Foundation

struct RectCordinates {
    var top = 0.0
    var bottom = 0.0
    var left = 0.0
    var right = 0.0
    }


class PredictionItem {
    var rectCordinates : RectCordinates
    var id :  String
    var name : String
    var description : String
    var manual : String
    var website_url : String
    var img_path : String
    
    init(dictionary: [String : Any]) {
        self.rectCordinates = RectCordinates()
        if let top_cordinate = dictionary[Constants.kPredictionItem_TopCordinate] as? Double {
            self.rectCordinates.top = top_cordinate
        }
        if let bottom_cordinate = dictionary[Constants.kPredictionItem_BottomCordinate] as? Double {
            self.rectCordinates.top = bottom_cordinate
        }
        if let left_cordinate = dictionary[Constants.kPredictionItem_LeftCordinate] as? Double {
            self.rectCordinates.top = left_cordinate
        }
        if let right_cordinate = dictionary[Constants.kPredictionItem_RightCordinate] as? Double {
            self.rectCordinates.top = right_cordinate
        }
        
        self.id = dictionary[Constants.kPredictionItemId] as! String
        self.name = dictionary[Constants.kPredictionItem_Name] as! String
        self.description = dictionary[Constants.kPredictionItem_description] as! String
        self.manual = dictionary[Constants.kPredictionItem_manual] as! String
        self.website_url = dictionary[Constants.kPredictionItem_website_url] as! String
        self.img_path = dictionary[Constants.kPredictionItem_img_path] as! String
    }
    
    func createFrame() -> CGRect {
        let screen_bounds = UIScreen.main.bounds
        let x_pos = CGFloat(self.rectCordinates.left) * screen_bounds.size.width
        let right_pos = CGFloat(self.rectCordinates.right) * screen_bounds.size.width
        let width = screen_bounds.size.width - x_pos - right_pos
        let y_pos = CGFloat(self.rectCordinates.top) * screen_bounds.size.height / 2
        var bottom_pos = CGFloat(self.rectCordinates.bottom) * screen_bounds.size.height * 2
        if bottom_pos > screen_bounds.size.height {
            bottom_pos = CGFloat(self.rectCordinates.bottom) * screen_bounds.size.height * 2
        }
        let height = screen_bounds.size.height - y_pos - bottom_pos
        let frame =  CGRect(x: x_pos, y: y_pos, width: width, height: height)
        return frame
    }
    
    func getCentrePoint() -> CGPoint {
        let itemFrame = self.createFrame()
        let x_pos = itemFrame.origin.x + itemFrame.size.width/2

        let y_pos = itemFrame.origin.y + itemFrame.size.height/2
        let centerPoint = CGPoint(x: x_pos, y: y_pos)
        return centerPoint
    }
}
