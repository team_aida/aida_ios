//
//  Extensions.swift
//  AR_AI_Demo
//
//  Created by offshore_mac_1 on 29/01/19.
//  Copyright © 2019 offshore_mac_1. All rights reserved.
//

import UIKit
import Photos
import Alamofire
import NVActivityIndicatorView

let MAINSCREEN_WIDTH = UIScreen.main.bounds.size.width
let MAINSCREEN_HEIGHT = UIScreen.main.bounds.size.height

let USDF = UserDefaults.standard

let LIGHT_WHITE_BACKGROUND = UIColor.init(red: 170/255.0, green: 170/255.0, blue: 170/255.0, alpha: 1.0)
let SPINNER_COLOR = UIColor.init(red: 255/255.0, green: 100/255.0, blue: 38/255.0, alpha: 1.0)
let SPINNER_COLOR_DARK = UIColor.init(red: 198/255.0, green: 87/255.0, blue: 38/255.0, alpha: 1.0)

let TEXT_ORANGE = UIColor.init(red: 209/255.0, green: 98/255.0, blue: 52/255.0, alpha: 1.0)
let GREY_SHADE_225 = UIColor.init(red: 225/255.0, green: 225/255.0, blue: 225/255.0, alpha: 1.0)


let activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: MAINSCREEN_WIDTH * 0.4, y: MAINSCREEN_WIDTH * 0.5, width: MAINSCREEN_WIDTH * 0.2, height: MAINSCREEN_WIDTH * 0.2), type:.ballSpinFadeLoader, color: SPINNER_COLOR_DARK, padding: MAINSCREEN_WIDTH * 0.05)
var spinner = UIActivityIndicatorView()
var StoreInfo = Dictionary<String, Any>()

enum ThrowError: Error {
	case noThumbnail
	case noImage
}

enum UIUserInterfaceIdiom : Int {
	case unspecified
	case phone
	case pad
}
let CURRENT_DEVICE = UIDevice.current.userInterfaceIdiom

let LanguageList = [
	"Afrikaans":"af",
	"Albanian":"sq",
	"Amharic":"am",
	"Arabic":"ar",
	"Armenian":"hy",
	"Azeerbaijani":"az",
	"Basque":"eu",
	"Bengali":"bn",
	"Bosnian":"bs",
	"Bulgarian":"bg",
	"Catalan":"ca",
	"Cebuano":"ceb",
	"Chinese (Simplified)":"zh-CN",
	"Chinese (Traditional)":"zh-TW",
	"Corsican":"co",
	"Croatian":"hr",
	"Czech":"cs",
	"Danish":"da",
	"Dutch":"nl",
	"English":"en",
	"Esperanto":"eo",
	"Estonian":"et",
	"Finnish":"fi",
	"French":"fr",
	"Frisian":"fy",
	"Galician":"gl",
	"Georgian":"ka",
	"German":"de",
	"Greek":"el",
	"Gujarati":"gu",
	"Haitian Creole":"ht",
	"Hausa":"ha",
	"Hawaiian":"haw",
	"Hebrew":"he",
	"Hindi":"hi",
	"Hmong":"hmn",
	"Hungarian":"hu",
	"Icelandic":"is",
	"Igbo":"ig",
	"Indonesian":"id",
	"Irish":"ga",
	"Italian":"it",
	"Japanese":"ja",
	"Javanese":"jw",
	"Kannada":"kn",
	"Kazakh":"kk",
	"Khmer":"km",
	"Korean":"ko",
	"Kurdish":"ku",
	"Lao":"lo",
	"Latvian":"lv",
	"Lithuanian":"lt",
	"Luxembourgish":"lb",
	"Macedonian":"mk",
	"Malagasy":"mg",
	"Malay":"ms",
	"Malayalam":"ml",
	"Maltese":"mt",
	"Maori":"mi",
	"Marathi":"mr",
	"Mongolian":"mn",
	"Nepali":"ne",
	"Norwegian":"no",
	"Nyanja (Chichewa)":"ny",
	"Pashto":"ps",
	"Persian":"fa",
	"Polish":"pl",
	"Portuguese (Portugal, Brazil)":"pt",
	"Punjabi":"pa",
	"Romanian":"ro",
	"Russian":"ru",
	"Samoan":"sm",
	"Scots Gaelic":"gd",
	"Serbian":"sr",
	"Sesotho":"st",
	"Shona":"sn",
	"Sindhi":"sd",
	"Sinhala (Sinhalese)":"si",
	"Slovak":"sk",
	"Slovenian":"sl",
	"Somali":"so",
	"Spanish":"es",
	"Swahili":"sw",
	"Swedish":"sv",
	"Tagalog (Filipino)":"tl",
	"Tajik":"tg",
	"Tamil":"ta",
	"Telugu":"te",
	"Thai":"th",
	"Turkish":"tr",
	"Ukrainian":"uk",
	"Urdu":"ur",
	"Uzbek":"uz",
	"Vietnamese":"vi",
	"Welsh":"cy",
	"Xhosa":"xh",
	"Yiddish":"yi",
	"Yoruba":"yo",
	"Zulu":"zu"
]

let LanguageNames = [
	"Afrikaans"
	,"Albanian"
	,"Amharic"
	,"Arabic"
	,"Armenian"
	,"Azeerbaijani"
	,"Basque"
	,"Bengali"
	,"Bosnian"
	,"Bulgarian"
	,"Catalan"
	,"Cebuano"
	,"Chinese (Simplified)"
	,"Chinese (Traditional)"
	,"Corsican"
	,"Croatian"
	,"Czech"
	,"Danish"
	,"Dutch"
	,"English"
	,"Esperanto"
	,"Estonian"
	,"Finnish"
	,"French"
	,"Frisian"
	,"Galician"
	,"Georgian"
	,"German"
	,"Greek"
	,"Gujarati"
	,"Haitian Creole"
	,"Hausa"
	,"Hawaiian"
	,"Hebrew"
	,"Hindi"
	,"Hmong"
	,"Hungarian"
	,"Icelandic"
	,"Igbo"
	,"Indonesian"
	,"Irish"
	,"Italian"
	,"Japanese"
	,"Javanese"
	,"Kannada"
	,"Kazakh"
	,"Khmer"
	,"Korean"
	,"Kurdish"
	,"Lao"
	,"Latvian"
	,"Lithuanian"
	,"Luxembourgish"
	,"Macedonian"
	,"Malagasy"
	,"Malay"
	,"Malayalam"
	,"Maltese"
	,"Maori"
	,"Marathi"
	,"Mongolian"
	,"Nepali"
	,"Norwegian"
	,"Nyanja (Chichewa)"
	,"Pashto"
	,"Persian"
	,"Polish"
	,"Portuguese (Portugal, Brazil)"
	,"Punjabi"
	,"Romanian"
	,"Russian"
	,"Samoan"
	,"Scots Gaelic"
	,"Serbian"
	,"Sesotho"
	,"Shona"
	,"Sindhi"
	,"Sinhala (Sinhalese)"
	,"Slovak"
	,"Slovenian"
	,"Somali"
	,"Spanish"
	,"Swahili"
	,"Swedish"
	,"Tagalog (Filipino)"
	,"Tajik"
	,"Tamil"
	,"Telugu"
	,"Thai"
	,"Turkish"
	,"Ukrainian"
	,"Urdu"
	,"Uzbek"
	,"Vietnamese"
	,"Welsh"
	,"Xhosa"
	,"Yiddish"
	,"Yoruba"
	,"Zulu"
]

let CountryCodes = [
	"ZA",
	"AL",
	"ET",
	"AE",
	"AM",
	"AZ",
	"ES",
	"BD",
    "BA",
	"BG",
	"ES",
	"PH",
	"CN",
	"CN",
	"FR",
	"HR",
	"CZ",
	"DK",
	"NL",
    "GB",
    "EO",
    "EE",
    "FI",
    "FR",
    "NL-FR",
	"ES",
	"GE",
	"DE",
	"GR",
    "IN",
    "HT",
    "NE",
    "HW",
    "IL",
    "IN",
	"CN",
	"HU",
	"IS",
	"NG",
	"ID",
	"IE",
	"IT",
	"JP",
	"ID",
    "IN",
	"KZ",
	"KH",
	"KR",
	"IQ",
	"LA",
	"LV",
	"LT",
	"LU",
	"MK",
	"MG",
    "MY",
    "IN",
	"MT",
	"NZ",
    "IN",
	"MN",
	"NP",
	"NO",
	"ZM",
	"AF",
	"IR",
	"PL",
	"PT",
	"RO",
	"RU",
	"WS",
	"GB",
	"RS",
	"LS",
	"ZW",
	"LK",
	"SK",
	"SI",
	"SO",
	"ES",
	"TZ",
	"SE",
	"PH",
	"TJ",
	"TH",
	"TR",
	"UA",
	"PK",
	"UZ",
	"VN",
	"GB",
	"ZA",
	"IL",
	"NG",
	"ZA",
]

let LanguageCodes = [
	"af"
	,"sq"
	,"am"
	,"ar"
	,"hy"
	,"az"
	,"eu"
	,"bn"
	,"bs"
	,"bg"
	,"ca"
	,"ceb"
	,"zh-CN"
	,"zh-TW"
	,"co"
	,"hr"
	,"cs"
	,"da"
	,"nl"
	,"en"
	,"eo"
	,"et"
	,"fi"
	,"fr"
	,"fy"
	,"gl"
	,"ka"
	,"de"
	,"el"
	,"gu"
	,"ht"
	,"ha"
	,"haw"
	,"he"
	,"hi"
	,"hmn"
	,"hu"
	,"is"
	,"ig"
	,"id"
	,"ga"
	,"it"
	,"ja"
	,"jw"
	,"kn"
	,"kk"
	,"km"
	,"ko"
	,"ku"
	,"lo"
	,"lv"
	,"lt"
	,"lb"
	,"mk"
	,"mg"
	,"ms"
	,"ml"
	,"mt"
	,"mi"
	,"mr"
	,"mn"
	,"ne"
	,"no"
	,"ny"
	,"ps"
	,"fa"
	,"pl"
	,"pt"
	,"pa"
	,"ro"
	,"ru"
	,"sm"
	,"gd"
	,"sr"
	,"st"
	,"sn"
	,"sd"
	,"si"
	,"sk"
	,"sl"
	,"so"
	,"es"
	,"sw"
	,"sv"
	,"tl"
	,"tg"
	,"ta"
	,"te"
	,"th"
	,"tr"
	,"uk"
	,"ur"
	,"uz"
	,"vi"
	,"cy"
	,"xh"
	,"yi"
	,"yo"
	,"zu"
]

class Extensions: NSObject, NVActivityIndicatorViewable {
	
}


extension String {
	var isValidURL: Bool {
		let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
		if let match = detector.firstMatch(in: self, options: [], range: NSRange(location: 0, length: self.endIndex.encodedOffset)) {
			// it is a link, if the match covers the whole string
			return match.range.length == self.endIndex.encodedOffset
		} else {
			return false
		}
	}
}

extension UIButton {
	
	func BlendWithBack (hide : Bool) {
		
		if hide {
			self.backgroundColor = UIColor.clear
			self.titleLabel?.textColor = UIColor.clear
			self.layer.borderColor = UIColor.clear.cgColor
			self.isEnabled = false
		}
		else {
			self.backgroundColor = GREY_SHADE_225
			self.titleLabel?.textColor = TEXT_ORANGE
			self.layer.borderColor = UIColor.lightGray.cgColor
			self.isEnabled = true
		}
		
	}
}


extension NSObject {
	
	func appBuildNumber() -> String? {
		var buildNumber: String? = nil
		if let object = Bundle.main.infoDictionary?[kCFBundleVersionKey as String] {
			buildNumber = "Version \(object)"
		}
		return buildNumber
	}
    
    func appVersionNumber() -> String? {
        var buildNumber: String? = nil
        if let object = Bundle.main.infoDictionary?["CFBundleShortVersionString"] {
            buildNumber = "Version \(object)"
        }
        return buildNumber
    }
	
	func RemoveStoredLanguageValues () {
		USDF.removeObject(forKey: "SelectedLanguageName")
		USDF.removeObject(forKey: "SelectedLanguage")
	}
	
	func CameraScreenTitle () -> String {
		switch SelectCameraMode {
		case .Prediction? :
			return "Object Recognition and Training"
			
		case .AddImage? :
			return "Click Image to Upload"
			
		case .EditImage? :
			return "Click image to upload"
			
		case .RipePrediction? :
			return "Click image to upload"
			
		default :
			return "Object Recognition"
		}
	}
	
	func PushViewController (toVC: String, mine: UIViewController) {
		let me = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: toVC) as UIViewController
		
		mine.navigationController?.pushViewController(me, animated: true)
	}
	
	func PopViewController (from : UIViewController) {
		from.navigationController?.popViewController(animated: true)
	}
	
	func PopMultipleViewControllers(_ nb: Int, from: UIViewController) {
		if let viewControllers: [UIViewController] = from.navigationController?.viewControllers {
			guard viewControllers.count < nb else {
				from.navigationController?.popToViewController(viewControllers[viewControllers.count - nb], animated: true)
				return
			}
		}
	}
	
	
	
	func CornerRadius (_ input : UIView, value : CGFloat) {
		input.layer.cornerRadius = input.frame.size.height * value
		input.layer.borderWidth = 0.5
		input.layer.borderColor = LIGHT_WHITE_BACKGROUND.cgColor
		input.layer.masksToBounds = true
	}

	func AddDropImage (_ input : UIView) {
		for view in input.subviews {
			if view .isKind(of: UIImageView.self) {
				view.removeFromSuperview()
			}
		}
		let downArrow = UIImageView()
		downArrow.frame = CGRect(x: input.frame.size.width * 0.9 - MAINSCREEN_WIDTH * 0.05, y: input.frame.size.height * 0.25, width: MAINSCREEN_WIDTH * 0.05, height: input.frame.size.height * 0.5)
		downArrow.image = UIImage(named: "select-down-arrow-small")
		downArrow.contentMode = .scaleAspectFit
		input.addSubview(downArrow)
	}
	
	
	func StartSpinner (_ response : Bool) {
		
		let backView = UIView()
		backView.frame = CGRect(x: 0, y: 0, width: MAINSCREEN_WIDTH, height: MAINSCREEN_HEIGHT)
		backView.backgroundColor = UIColor.black
		backView.alpha = 0.45
		
		spinner.frame = CGRect(x: MAINSCREEN_WIDTH * 0.45, y: MAINSCREEN_HEIGHT * 0.45, width: MAINSCREEN_WIDTH * 0.1, height: MAINSCREEN_WIDTH * 0.1)
		spinner.hidesWhenStopped = true
		spinner.style = .whiteLarge
		spinner.color = SPINNER_COLOR_DARK
		
		if response {
			UIApplication.shared.keyWindow?.addSubview(backView)
			UIApplication.shared.keyWindow?.addSubview(spinner)
			UIApplication.shared.keyWindow?.bringSubviewToFront(spinner)
			spinner.startAnimating()
			
		}
		else {
			spinner.stopAnimating()
			spinner.removeFromSuperview()
			backView.removeFromSuperview()
		}
	}
	
	//Language Selection
	func pickLanguage (_ key:String) -> String {
		return NSLocalizedString(key, comment: "Cancel")
	}
	
	//MARK:- Network Availability
	func NetworkAvailable() -> Bool {
		if (NetworkReachabilityManager.init()?.isReachable)! {
			return true
		}
		else {
			showAlert(withTitle: "Alert!", message: "Unable to connect to the internet. Check your connection and try again"/*pickLanguage("No Internet")*/, okButton: "OK", cancelButton: "") { (response) in
				
			}
			return false
		}
	}
	
	//MARK:- Image Encoding to Base64
	func base64EncodeImage(_ image: UIImage) -> String {
		var imagedata = image.pngData()
		
		// Resize the image if it exceeds the 2MB API limit
		if ((imagedata?.count)! > 2097152) {
			let oldSize: CGSize = image.size
			let newSize: CGSize = CGSize(width: 800, height: oldSize.height / oldSize.width * 800)
			imagedata = resizeImage(newSize, image: image)
		}
		
		return imagedata!.base64EncodedString(options: .endLineWithCarriageReturn)
	}
	
	func resizeImage(_ imageSize: CGSize, image: UIImage) -> Data {
		UIGraphicsBeginImageContext(imageSize)
		image.draw(in: CGRect(x: 0, y: 0, width: imageSize.width, height: imageSize.height))
		let newImage = UIGraphicsGetImageFromCurrentImageContext()
		let resizedImage = newImage!.pngData()
		UIGraphicsEndImageContext()
		return resizedImage!
	}
	
	//MARK:- Action Sheet
	func showActionSheet(_ title: String?, message: String?, optionsList: /*[Bool : String]*/[String]?, view: UIView, completion: @escaping (_ response: String?) -> Void) {
		
		let actionSheet = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
		
		for (/*active,*/string) in optionsList!  {
			let option = UIAlertAction(title: string, style: .default, handler: { action in
				completion(string)
			})
			//			option.isEnabled = active
			actionSheet.addAction(option)
		}
		
		let cancelOption = UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
			completion("Cancel")
		})
		actionSheet.addAction(cancelOption)
		
		if let popoverController = actionSheet.popoverPresentationController {
			popoverController.sourceView = view
			popoverController.sourceRect = CGRect(x: view.bounds.midX, y: view.bounds.midY, width: 0, height: 0)
			popoverController.permittedArrowDirections = []
		}
		UIApplication.shared.keyWindow?.rootViewController?.present(actionSheet, animated: true)
	}
	
	// MARK: - Alert Controller
	func showAlert(withTitle title: String?, message: String?, okButton okTitle: String?, cancelButton cancelTitle: String?, completion: @escaping (_ response: Bool) -> Void) {
		
		let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
		
		if !(okTitle?.isEqual("") ?? false) {
			let ok = UIAlertAction(title: okTitle, style: .default, handler: { action in
				completion(true)
			})
			alertController.addAction(ok)
		}
		
		if !(cancelTitle?.isEqual("") ?? false) {
			let cancel = UIAlertAction(title: cancelTitle, style: .default, handler: { action in
				completion(false)
			})
			alertController.addAction(cancel)
		}
		UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true)
	}
	
	//MARK:- Photo Library Authorization
	func checkPhotoLibraryAutorization (completion: @escaping (_ response: Bool) -> Void) {
		if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
			let status = PHPhotoLibrary.authorizationStatus()
			
			switch status {
			case .authorized:
				print("Photo Library Access is Authorized")
				completion(true)
				break
				
			case .denied:
				print("Photo Library Access is Denied")
				completion(false)
				break
				
			case .notDetermined:
				var newStatusReturn = Bool()
				PHPhotoLibrary.requestAuthorization { (newStatus) in
					if newStatus == PHAuthorizationStatus.authorized {
						newStatusReturn = true
					}
					else {
						newStatusReturn = false
					}
				}
				completion(newStatusReturn)
				break
				
			case .restricted:
				print("Photo Library Access is restricted")
				completion(false)
				break
			}
		}
		else {
			showAlert(withTitle: "Alert!", message: "Photo Library is not available", okButton: "OK", cancelButton: "") { (response) in
				
			}
		}
	}
	
	//MARK:- Camera Authorization
	func checkCameraAutorization (completion: @escaping (_ response: Bool) -> Void) {
		
		if UIImagePickerController.isSourceTypeAvailable(.camera) {
			let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
			
			switch status {
			case .authorized:
				print("Camera Access is authorized")
				completion(true)
				break
				
			case .denied:
				print("Camera Access is denied")
				completion(false)
				break
				
			case .notDetermined:
				var newStatusReturn = Bool()
				AVCaptureDevice.requestAccess(for: AVMediaType.video) { (newStatus) in
					if newStatus == true {
						print("Camera Access is granted")
						newStatusReturn = true
					}
					else {
						print("Camera Access is denied")
						newStatusReturn = false
					}
				}
				completion(newStatusReturn)
				break
				
			case .restricted:
				print("Camera Access is restricted")
				completion(false)
				break
			}
		}
		else {
			showAlert(withTitle: "Alert!", message: "Camera is not available on this device"/*pickLanguage("Camera not found")*/, okButton: "OK", cancelButton: "") { (response) in
				
			}
		}
	}
	
	func calculateLabelWidth(string: String?) -> CGFloat {
		var result: CGFloat
//		if IDIOM == IPAD {
//			result = string?.boundingRect(with: CGSize(width: 9999.0, height: 30.0), options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 18)], context: nil).size.width ?? 0.0
//		} else {
		result = string?.boundingRect(with: CGSize(width: 9999.0, height: 20.0), options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14.0, weight: .semibold)], context: nil).size.width ?? 0.0
//		}
		
		
		if (string?.count ?? 0) <= 3 {
			result = CGFloat((string?.count ?? 0) * 10)
		}
		return result
	}
	
}
