//
//  Webservices.swift
//  AR_AI_Demo
//
//  Created by offshore_mac_1 on 29/01/19.
//  Copyright © 2019 offshore_mac_1. All rights reserved.
//

import UIKit
import Alamofire
import JWT
import SwiftyJSON
import CryptoSwift



let DEV_URL = "http://35.173.5.170:8080/"//"http://35.173.5.170:8080/" //"http://192.168.1.15:8000/"  //http://192.168.1.22:300/"
let LIVE_URL = "http://52.7.27.60/"

let MODEL_BASE_URL = DEV_URL

let JWT_SecurityKey = "secret"

let AddProduct_URL = "\(MODEL_BASE_URL)addProduct"
let EditProduct_URL = "\(MODEL_BASE_URL)editProduct"
let ProductList_URL = "\(MODEL_BASE_URL)productList"
let AddImage_URL = "\(MODEL_BASE_URL)addImage"
let ImageList_URL = "\(MODEL_BASE_URL)imageList"
let EditImage_URL = "\(MODEL_BASE_URL)editImage"
let Predic_URL = "\(MODEL_BASE_URL)predect"
let SaveModelForStartTraining_URL = "\(MODEL_BASE_URL)saveModelmobilenet"
let SaveModel_URL = "\(MODEL_BASE_URL)saveModel" // saveModelmobilenet
let MultiObjectPredic_URL = "http://35.173.5.170:8080/api/mobile/apiv1s/Prediction" //\(MODEL_BASE_URL)predectModelmobilenet"

let RipePredict_URL = "\(LIVE_URL)tpredect"


enum NewModelScreen {
	case AddModel
	case EditModel
}

var ModelScreenMode : NewModelScreen? = NewModelScreen.AddModel

var GoingNewModel : Bool = false


enum CameraMode {
	case Prediction
	case AddImage
	case EditImage
	case FreshShopping
	case RipePrediction
	case Other
}
var SelectCameraMode : CameraMode? = CameraMode.Prediction


struct ProductDetail {
	var attachment : String?
	var description : String?
	var p_id : Int?
	var img_path : String?
	var manual : String?
	var name : String?
	var website_url : String?
	
	init(dictionary : [String:Any]) {
		self.attachment = dictionary["attachment"] as? String
		self.description = dictionary["description"] as? String
		self.p_id = dictionary["id"] as? Int
		self.img_path = dictionary["img_path"] as? String
		self.manual = dictionary["manual"] as? String
		self.name = dictionary["name"] as? String
		self.website_url = dictionary["website_url"] as? String
	}
	
	func setupProductDetail () -> [String : Any] {
		return ["attachment": self.attachment!,
				"description": self.description!,
				"id": self.p_id!,
				"img_path": self.img_path!,
				"manual": self.manual!,
				"name": self.name!,
				"website_url": self.website_url!]
	}
	
	func clearProductDetails () {
		Product = ProductDetail.init(dictionary: [:])
	}
	
	func setupDisplayDetail () -> ([String : Any], [String], [String]) {
		
		var outputDict = [String : Any]()
		var outputValues = [String]()
		var outputNames = [String]()
		if self.name != "" {
			outputDict["name"] = self.name!
			outputValues.append(self.name!)
			outputNames.append("Product Name")
		}
		if self.description != "" {
			outputDict["description"] = self.description!
			outputValues.append(self.description!)
			outputNames.append("Description")
		}
		if self.manual != "" {
			outputDict["manual"] = self.manual!
			outputValues.append(self.manual!)
			outputNames.append("Product Manual")
		}
		if self.website_url != "" {
			outputDict["website_url"] = self.website_url!
			outputValues.append(self.website_url!)
			outputNames.append("Reference URL")
		}
		return (outputDict, outputNames, outputValues)
	}
}
var Product : ProductDetail?


struct ImageDetail {
	var img_id : Int?
	var img_path : String?
	var image_name : String?
	var product_id : String?
	
	init(dictionary : [String:Any]) {
		self.img_id = dictionary["id"] as? Int
		self.img_path = dictionary["img_path"] as? String
		self.image_name = dictionary["image_name"] as? String
		self.product_id = dictionary["product_id"] as? String
	}
	
	func setupImageDetail () -> [String : Any] {
		return ["id": self.img_id!,
				"img_path": self.img_path!,
				"image_name": self.image_name!,
				"product_id": self.product_id!]
	}
}
var ProductImage : ImageDetail?

struct ProductImageList {
	var ProductImages : [ImageDetail]?
	
	init(array : [ImageDetail]) {
		self.ProductImages = array
	}
	
	func setupProductImageList () -> [ImageDetail] {
		return self.ProductImages!
	}
}




class Webservices: NSObject {
	
	class var shared: Webservices {
		struct Static {
			static let instance: Webservices = Webservices()
		}
		return Static.instance
	}
	//MARK:- Object Recognition and Training
	
	func Alamofire_POST (Location: String, param: [String:Any], completion:@escaping(_ status: Int,_ message: String,_ result: ClaimSet) -> Void) {
		if NetworkAvailable() {
			
			Alamofire.request(URL(string: Location)!, method: .post, parameters: param).validate().responseJSON { (response) in
				
				if response.result.isSuccess {
					let UserRole = response.result.value as! NSDictionary
					print("Success Response : ", UserRole)
					if UserRole["status"] as! Int == 200 {
						let jwtToken = UserRole["data"] as! String
						let result = String(jwtToken.dropFirst(2))
						let result2 = String(result.dropLast())
						do {
							let claims : ClaimSet = try JWT.decode(result2, algorithm: .hs256(JWT_SecurityKey.data(using: .utf8)!))
							print("Claims Data : ", claims)
							completion(1, UserRole["msg"] as! String, claims)
						}
						catch {
							print("Failed to decode JWT: \(error)")
							completion(0, "\(error)", ClaimSet.init())
						}
					}
					else {
                        if let message = UserRole["msg"] as? String{
                            completion(0,message, ClaimSet.init())
                        }
                        else  if let data = UserRole["data"] as? String{
                            completion(0,data, ClaimSet.init())
                        }
					}
					
				}
				else {
					//					let UserRole = response.result.value as! NSDictionary
					print("Failure Response : ", response)
					completion(0, /*UserRole["msg"] as! String*/"", ClaimSet.init())
				}
			}
		}
	}
	
	func AlamofireWithImage_POST (Location: String, param: [String:Any], imageFile : Data?, completion:@escaping(_ status: Int,_ message: String,_ result: Any) -> Void) {
		if NetworkAvailable() {
           
			Alamofire.upload(multipartFormData: { (multipartFormData) in
				if let data = imageFile {
					
					multipartFormData.append(data, withName: "file", fileName: "Image.jpeg", mimeType: "image/jgeg")
				}
				for (key, value) in param {
					multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
				}
				print("Multi Form Data : \(multipartFormData.contentType)")
			}, to: Location)
			{ (result) in
				
				//				print("Image POST Result :", result)
				switch result {
				case .success(let upload, _, _):
					
					upload.uploadProgress(closure: { (progress) in
					})
					
					upload.responseJSON { response in
						
						print("Image POST Success Response : \(response)")
						
						if response.result.isSuccess {
							let UserRole = response.result.value as! NSDictionary
							print("Success Response : ", UserRole)
							if UserRole["status"] as! Int == 200 {
                                if let result = UserRole["data"] as? [[String :Any]] {
                                    completion(1, UserRole["msg"] as! String, result)
                                }
                                
                                if let jwtToken = UserRole["data"] as? String {
                                    do {
                                                                    let claims : ClaimSet = try JWT.decode(jwtToken, algorithm: .hs256(JWT_SecurityKey.data(using: .utf8)!))
                                                                    print("Claims Data : ", claims)
                                                                    completion(1, UserRole["msg"] as! String, claims)
                                                                }
                                                                catch {
                                                                    print("Failed to decode JWT: \(error)")
                                                                    completion(0, "\(error)", ClaimSet.init())
                                                                }
                                }
							}
							else if UserRole["status"] as! Int == 202 {
								completion(2, UserRole["msg"] as! String, ClaimSet.init())
							}
							else {
								completion(0, UserRole["msg"] as! String, ClaimSet.init())
							}
						}
						else {
							completion(0, "Server Error Encountered. Please try again", ClaimSet.init())
						}
                    }
				case .failure(let encodingError):
					print(encodingError.localizedDescription)
					completion(0, "\(encodingError.localizedDescription)", ClaimSet.init())
					break
				}
			}
		}
	}
	
	func AlamofireWithPDF_POST (Location: String, param: [String:Any], pdfFile: String, completion:@escaping(_ status: Int,_ message: String,_ result: ClaimSet) -> Void) {
		if NetworkAvailable() {
			Alamofire.upload(multipartFormData: { (multipartFormData) in
				if !pdfFile.isEmpty {
					
					let pdfData = try! Data(contentsOf: pdfFile.asURL())
					let data : Data = pdfData
					
					multipartFormData.append(data, withName: "file", fileName: "FileDocument.pdf", mimeType: "application/pdf")
				}
				for (key, value) in param {
					multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
				}
				print("Multi Form Data : \(multipartFormData.contentType)")
			}, to: Location)
			{ (result) in
				
				//				print("Image POST Result :", result)
				switch result {
				case .success(let upload, _, _):
					
					upload.uploadProgress(closure: { (progress) in
					})
					
					upload.responseJSON { response in
						
						print("PDF POST Success Response : \(response)")
						
						if response.result.isSuccess {
							let UserRole = response.result.value as! NSDictionary
							print("Success Response : ", UserRole)
							if UserRole["status"] as! Int == 200 {
								let jwtToken = UserRole["data"] as! String
								let result = String(jwtToken.dropFirst(2))
								let result2 = String(result.dropLast())
								do {
									let claims : ClaimSet = try JWT.decode(result2, algorithm: .hs256(JWT_SecurityKey.data(using: .utf8)!))
									print("Claims Data : ", claims)
									completion(1, UserRole["msg"] as! String, claims)
								}
								catch {
									print("Failed to decode JWT: \(error)")
									completion(0, "\(error)", ClaimSet.init())
								}
							}
							else {
								completion(0, UserRole["msg"] as! String, ClaimSet.init())
							}
						}
						else {
							completion(0, "Server Error Encountered. Please try again", ClaimSet.init())
						}
					}
					
					
				case .failure(let encodingError):
					print(encodingError.localizedDescription)
					completion(0, "\(encodingError.localizedDescription)", ClaimSet.init())
					break
				}
			}
		}
	}
	
	//MARK:- Model Listing Methods
	func ModelProductList(limit: Int, offset: Int, completion: @escaping(_ ResponseStatus: Int,_ ResponseData: Array<Any>) -> Void) {
		let params = ["limit":limit, "offset":offset]
		Webservices.shared.Alamofire_POST(Location: ProductList_URL, param: params) { (status, Message, Response) in
			if status == 1 {
				let result = Response["Product_List"] as! Array<Any>
				completion(status, result)
			}
			else {
				print("Show Alert Message for Zero Status :", Message)
				completion(status, [])
			}
		}
	}
	
	func ModelImageList(limit: Int, offset: Int, modelId: String, completion: @escaping(_ ResponseStatus: Int,_ ResponseData: Array<Any>) -> Void) {
		let params = ["limit":limit, "offset":offset, "id": modelId] as [String : Any]
		Webservices.shared.Alamofire_POST(Location: ImageList_URL, param: params) { (status, Message, Response) in
			if status == 1 {
				let result = Response["image_List"] as! Array<Any>
				completion(status, result)
			}
			else {
				print("Show Alert Message for Zero Status :", Message)
				completion(status, [])
			}
		}
	}
	
	//MARK:- Add Product Method
	func ModelAddProduct(name: String, manual: String, description: String, website_url: String, completion: @escaping(_ ResponseStatus: Int,_ ResponseData: Array<Any>) -> Void) {
		
		let params = ["name": name, "manual": manual, "description": description, "website_url": website_url]
		//        let imageFileData = image.jpegData(compressionQuality: 0.6)
		Webservices.shared.Alamofire_POST(Location: AddProduct_URL, param: params) { (Status, Message, Result) in
			if Status == 1 {
				print("Add Product Result : ", Result)
				completion(Status, [])
			}
			else {
				completion(Status, [])
			}
		}
	}
	
    func ModelAddProductWithPDF(name: String, manual: String, description: String, website_url: String, pdfFile: String, completion: @escaping(_ ResponseStatus: Int,_ ResponseData: [String:Any]) -> Void) {
		
		let params = ["name": name, "manual": manual, "description": description, "website_url": website_url]
		//        let imageFileData = image.jpegData(compressionQuality: 0.6)
		Webservices.shared.AlamofireWithPDF_POST(Location: AddProduct_URL, param: params, pdfFile: pdfFile) { (Status, Message, Result) in
			if Status == 1 {
				print("Add Product Result : ", Result)
                
                //Add Product Result :  ClaimSet(claims: ["attachment": , "manual": , "website_url": https://www.pen.com, "id": 11, "pen": pen, "description": ])

                var result = [String:Any]()
                result["id"] = Result["id"]
                result["attachment"] = Result["attachment"]
                result["manual"] = Result["manual"]
                result["website_url"] = Result["website_url"]
                result["name"] = name
                result["description"] = Result["description"]
                completion(Status, result)
			}
			else {
                completion(Status, [:])
			}
		}
		
	}
	
	//MARK:- Edit Product Method
	func ModelEditProduct(name: String, manual: String, description: String, id: String, website_url: String, completion: @escaping(_ ResponseStatus: Int,_ ResponseData: Array<Any>) -> Void) {
		
		let params = ["name": name, "manual": manual, "description": description, "website_url": website_url, "id": id]
		//        let imageFileData = image.jpegData(compressionQuality: 0.6)
		Webservices.shared.Alamofire_POST(Location: EditProduct_URL, param: params) { (Status, Message, Result) in
			if Status == 1 {
				print("Edit Product Result : ", Result)
				completion(Status, [])
			}
			else {
				completion(Status, [])
			}
		}
	}
	
	func ModelEditProductWithPDF(name: String, manual: String, description: String, id: String, website_url: String, pdfFile: String, completion: @escaping(_ ResponseStatus: Int,_ ResponseData: Array<Any>) -> Void) {
		
		let params = ["name": name, "manual": manual, "description": description, "website_url": website_url, "id": id]
		//        let imageFileData = image.jpegData(compressionQuality: 0.6)
		Webservices.shared.AlamofireWithPDF_POST(Location: EditProduct_URL, param: params, pdfFile: pdfFile) { (Status, Message, Result) in
			if Status == 1 {
				print("Edit Product Result : ", Result)
				completion(Status, [])
			}
			else {
				completion(Status, [])
			}
		}
		
	}
	
	//MARK:- Image Uploading Methods
	func ModelAddImage(ImageName: String, productId: String, imageFile: UIImage, completion: @escaping(_ ResponseStatus: Int,_ ResponseData: Array<Any>) -> Void) {
		let params = ["image_name": ImageName, "product_id": productId]
		let imageFileData = imageFile.jpegData(compressionQuality: 0.6)
		Webservices.shared.AlamofireWithImage_POST(Location: AddImage_URL, param: params, imageFile: imageFileData) { (Status, Message, Result) in
			if Status == 1 {
				print("Add Image Result : ", Result)
				completion(Status, [])
				//				let result = Result["result"] as! [[String: Any]]
				//				print("Model Predict Result : ",result)
				//				if result.count == 0 {
				//					completion(0, [[:]])
				//				}
				//				else {
				//					completion(Status, result)
				//				}
			}
			else {
				completion(Status, [])
			}
		}
	}
	
	func ModelEditImage(ImageName: String, imageId: String, imageFile: UIImage, completion: @escaping(_ ResponseStatus: Int,_ ResponseData: Array<Any>) -> Void) {
		let params = ["image_name": ImageName, "id": imageId]
		let imageFileData = imageFile.jpegData(compressionQuality: 0.6)
		Webservices.shared.AlamofireWithImage_POST(Location: EditImage_URL, param: params, imageFile: imageFileData) { (Status, Message, Result) in
			if Status == 1 {
				print("Edit Image Result : ", Result)
				completion(Status, [])
			}
			else {
				completion(Status, [])
			}
		}
	}
	
	//MARK:- Model Prediction
	func ModelPredict (location : String,imageFile : UIImage, completion:@escaping(_ status: Int,_ message : String,_ result: [[String:Any]]) -> Void) {
		let imageFileData = imageFile.jpegData(compressionQuality: 0.6)
        let parameter  = ["api_key":"YmY1S1pEZUR5MUFhR2llTWNsL0FiT0pzaVFSWitFN1YvRE9va1N0ME9UZz06Ojsm2Ybj7DOMmRUQS9SqXvA="] //:"aGRwRG1WeEhTbHFDd1VnU2hJaExRTDZkT3l2aGpiNVJXUGZtV0VjK01Qcz06OhkeSsQSOKUPCOv3XnZJl/g="]
        
        //YmY1S1pEZUR5MUFhR2llTWNsL0FiT0pzaVFSWitFN1YvRE9va1N0ME9UZz06Ojsm2Ybj7DOMmRUQS9SqXvA=
		Webservices.shared.AlamofireWithImage_POST(Location: location, param: parameter, imageFile: imageFileData) { (Status, Message, Result) in
			if Status == 1 {
                if location == MultiObjectPredic_URL {
                    print("Model Multi object Predict Result : ", Result)
                    
                    if let result = Result as? [[String: Any]] {
                        print("Model Predict Result : ",result)
                                               if result.count == 0 {
                                                   completion(0, Message, [[:]])
                                               }
                                               else {
                                                   completion(Status, Message, result)
                                               }
                    }
                }
                else {
                    let result = Result as! [[String: Any]]
                    print("Model Predict Result : ",result)
                    if result.count == 0 {
                        completion(0, Message, [[:]])
                    }
                    else {
                        completion(Status, Message, result)
                    }
                }
			}
			else if Status == 2 {
				completion(Status, Message, [[:]])
			}
			else {
				print("Show Alert Message for Zero Status :", Message)
				completion(Status, Message, [[:]])
			}
		}
	}
	
	func ModelRipePredict (imageFile : UIImage, completion: @escaping(_ status: Int,_ result: [[String:Any]]) -> Void) {
		let imageFileData = imageFile.jpegData(compressionQuality: 0.6)
		Webservices.shared.AlamofireWithImage_POST(Location: RipePredict_URL, param: [:], imageFile: imageFileData) { (Status, Message, Result) in
			if Status == 1 {
				let result = Result as! [[String: Any]]
				print("Model Predict Result : ",result)
				if result.count == 0 {
					completion(0, [[:]])
				}
				else {
					completion(Status, result)
				}
			}
			else {
				completion(Status, [[:]])
			}
		}
	}
	
	
	//MARK:- Object Recognition (Cloud Vision)
	func ThuisProductSearch (products : String, page_offset : Int, language : String, completion:@escaping(_ complete : Bool,_ ResultData : Dictionary<String, Any>) -> Void) {
		//AlamofireRequest_POST
		if NetworkAvailable() {
			let param = [
				"product_search" : products,
				"limit" : 10,
				"offset" : page_offset,
				"target_lang" : language
				] as [String : Any]
			
			Alamofire.request(URL(string: ThuisProductSearch_URL)!, method: .post, parameters: param).validate().responseJSON { (response)  in
				print("Alamofire Request POST - Response : \(response)")
				if (response.result.isSuccess) {
					
					let UserRole = response.result.value as! NSDictionary
					
					let jwtToken = UserRole["jwt"] as! String
					
					do {
						let claims: ClaimSet = try JWT.decode(jwtToken, algorithm: .hs256(jwtSecurityKey.data(using: .utf8)!))
						
						print("Claims Status : ",claims["status"] as! Int)
						print("Claims Message : ",claims["translate"] as! String)
						print("Claims Data : ",claims)
						
						if (claims["status"] as! Int == 1) {
							completion(true, ["Status":claims["status"] as! Int, "Translate":claims["translate"] as! String, "Labels": products, "Data":claims["data"] as! Array<Any>])
						}
						else {
							completion(false, ["Status":claims["status"] as! Int, "Translate":claims["translate"] as! String, "Labels": products, "Data":[]])
						}
						
					}
					catch {
						print("Failed to decode JWT: \(error)")
						completion(false, ["Status":50])
					}
				}
				else {
					completion(false, ["Status":99])
				}
			}
		}
	}
	
	
	func CloudVisionRequest_POST(imageBase64: String, completion:@escaping(_ complete : Bool, _ result : Data)-> Void) {
		
		if NetworkAvailable() {
			// Create our request URL
			var request = URLRequest(url: URL(string: GoogleCloudVision_URL)!)
			request.httpMethod = "POST"
			request.addValue("application/json", forHTTPHeaderField: "Content-Type")
			request.addValue(Bundle.main.bundleIdentifier ?? "", forHTTPHeaderField: "X-Ios-Bundle-Identifier")
			
			// Build our API request
			let jsonRequest = [
				"requests": [
					"image": [
						"content": imageBase64
					],
					"features": [
						[
							"type": "LABEL_DETECTION",
							"maxResults": 10
						]/*,
						[
						"type": "FACE_DETECTION",
						"maxResults": 10
						]*/
					]
				]
			]
			let jsonObject = JSON(jsonRequest)
			
			guard let data = try? jsonObject.rawData() else {
				return
			}
			request.httpBody = data
			
			// Run the request on a background thread
			DispatchQueue.global().async {
				let task: URLSessionDataTask = URLSession.shared.dataTask(with: request) { (data, response, error) in
					guard let data = data, error == nil else {
						print(error?.localizedDescription ?? "")
						return
					}
					completion(true, data)
				}
				task.resume()
			}
		}
	}
	
	func GoogleCustomSearch (paramString : String, completion:@escaping(_ complete : Bool, _ result : [Any])->Void) {
		
		if NetworkAvailable() {
			
			let param = [
				"key":"\(VisionAPIKey)",
				"cx":"017567005641165239815:tlstso0odrs",
				"q":paramString
			] as [String: Any]
			
			Alamofire.request(URL(string: "\(GoogleCustomSearch_URL)")!, method: .get, parameters: param).validate().responseJSON { (response)  in
//				print("Google Custom Search - Response : ", response)
				if (response.result.isSuccess) {
					
					let Result = response.result.value as! NSDictionary
					let ResultArray = Result["items"] as! [Any]
					
//					var SearchresultArray = [Any]()
//
//					for index in ResultArray {
//
//						var SearchResultDict = [String: Any]()
//						let indexDict = index as! [String: Any]
//						let images = indexDict["pagemap"] as! [String: Any]
//						let imagesTN = images["cse_thumbnail"] as! [Any]
//						let imagesTNI = imagesTN[0] as! [String: Any]
//						let imagesTNIsrc = imagesTNI["src"] as! String
//						let thumbnailImage = imagesTNIsrc//((images["cse_thumbnail"] as! [Any])[0] as! [String: Any])["src"] as! String
//						let fullImage = ((images["cse_image"] as! [Any])[0] as! [String: Any])["src"] as! String
//
//						SearchResultDict.updateValue(indexDict["title"] as! String, forKey: "Title")
//						SearchResultDict.updateValue(indexDict["snippet"] as! String, forKey: "Snippet")
//						SearchResultDict.updateValue(thumbnailImage, forKey: "ThumbnailImage")
//						SearchResultDict.updateValue(fullImage, forKey: "FullImage")
//
//						SearchresultArray.append(SearchResultDict)
//					}
//
//					for i in 0..<SearchresultArray.count {
//						print("Index : \(i) ", SearchresultArray[i])
//					}
					
					completion(true, ResultArray)
				}
				else {
					completion(false, [])
				}
			}
		}
	}
	
}

extension NSObject {
	
	func AnalyzeCloudVisionResults(_ dataToParse: Data, completion:@escaping(_ responseStatus : Bool, _ message : String, _ visionData : JSON)->Void) {
		
		DispatchQueue.main.async(execute: {
			
			// Use SwiftyJSON to parse results
			let json = try! JSON(data: dataToParse)
			let errorObj: JSON = json["error"]
			
			if (errorObj.dictionaryValue != [:]) {
				let mesge = "Error code \(errorObj["code"]): \(errorObj["message"])"
				completion(false, mesge, [:])
			}
			else {
				
				print("Analyze Results : ",json)
				completion(true, "", json["responses"][0])
				
			}
		})
	}
	
	func AnalyzeLabelAnnotations (response : JSON, completion:@escaping(_ resultResponse: Bool,_ resultString: String,_ resultData: [String]) -> Void) {
		
		// Get label annotations
		let labelAnnotations: JSON = response["labelAnnotations"]
		let numLabels: Int = labelAnnotations.count
		var labels: Array<String> = []
		if numLabels > 0 {
			var labelResultsText:String = "Labels found: "
			for index in 0..<numLabels {
				let label = labelAnnotations[index]["description"].stringValue
				labels.append(label)
			}
			for label in labels {
				// if it's not the last item add a comma
				if labels[labels.count - 1] != label {
					labelResultsText += "\(label), "
				} else {
					labelResultsText += "\(label)"
				}
			}
			completion(true, labelResultsText, labels)
			
		}
		else {
			
			completion(false, "No labels found", [])
		}
	}
	
	func AnalyzeFaceAnnotations (response : JSON, completion:@escaping(_ resultResponse: Bool,_ resultData: String) -> Void) {
		
		// Get face annotations
		let faceAnnotations: JSON = response["faceAnnotations"]
		if faceAnnotations.count != 0 {
			let emotions: Array<String> = ["joy", "sorrow", "surprise", "anger"]
			
			let numPeopleDetected:Int = faceAnnotations.count
			
//			self.faceResults.text = "People detected: \(numPeopleDetected)\n\nEmotions detected:\n"
			
			var emotionTotals: [String: Double] = ["sorrow": 0, "joy": 0, "surprise": 0, "anger": 0]
			var emotionLikelihoods: [String: Double] = ["VERY_LIKELY": 0.9, "LIKELY": 0.75, "POSSIBLE": 0.5, "UNLIKELY":0.25, "VERY_UNLIKELY": 0.0]
			
			for index in 0..<numPeopleDetected {
				let personData:JSON = faceAnnotations[index]
				
				// Sum all the detected emotions
				for emotion in emotions {
					let lookup = emotion + "Likelihood"
					let result:String = personData[lookup].stringValue
					emotionTotals[emotion]! += emotionLikelihoods[result]!
				}
			}
			var resulttext = ""
			// Get emotion likelihood as a % and display in UI
			for (emotion, total) in emotionTotals {
				let likelihood:Double = total / Double(numPeopleDetected)
				let percent = String(format: "%.0f", likelihood * 100)
				//				self.faceResults.text! += "\(emotion): \(percent)%\n"
				resulttext += "\(emotion): \(percent)%\n"
			}
			completion(true, resulttext)
		}
		else {
			completion(false, "No faces found")
		}
	}
	
	func getProductDetail (_ input : [Any], ThuisSearch : Bool) -> [Any] {
		var DetailResult = [Any]()
		if ThuisSearch {
			for index in input {
				var SearchResultDict = [String: Any]()
				let indexDict = index as! [String: Any]
				
				let image = "\(BASE_IMAGE_URL)\(indexDict["meal_image"] as! String)"
				let title = indexDict["meal_name_ar"] as! String
				let description = indexDict["meal_description_ar"] as! String
				
				SearchResultDict.updateValue(title, forKey: "Title")
				SearchResultDict.updateValue(description, forKey: "Snippet")
				SearchResultDict.updateValue(image, forKey: "ThumbnailImage")
				SearchResultDict.updateValue(image, forKey: "FullImage")
				
				DetailResult.append(SearchResultDict)
			}
		}
		else {
			for i in 0..<input.count {
				
//				var SearchResultDict = [String: Any]()
//				let indexDict = index as! [String: Any]
//				let images = indexDict["pagemap"] as! [String: Any]
//				let thumbnailImage = ((images["cse_thumbnail"] as! [Any])[0] as! [String: Any])["src"] as! String
//				let fullImage = ((images["cse_image"] as! [Any])[0] as! [String: Any])["src"] as! String
				
				var SearchResultDict = [String: Any]()
				let indexDict = input[i] as! [String: Any]
				
				print("Index : \(i)")
				let images = indexDict["pagemap"] as! [String: Any]
				var thumbnailImage = ""
				var fullImage = ""
				if images["cse_thumbnail"] != nil {
//					let imagesTNI = imagesTN[0] as! [String: Any]
//					let imagesTNIsrc = imagesTNI["src"] as! String
					//				let thumbnailImage = imagesTNIsrc
					thumbnailImage = ((images["cse_thumbnail"] as! [Any])[0] as! [String: Any])["src"] as! String
				}
				
				if images["cse_image"] != nil {
					fullImage = ((images["cse_image"] as! [Any])[0] as! [String: Any])["src"] as! String
				}
				
				SearchResultDict.updateValue(indexDict["title"] as! String, forKey: "Title")
				SearchResultDict.updateValue(indexDict["snippet"] as! String, forKey: "Snippet")
				SearchResultDict.updateValue(thumbnailImage, forKey: "ThumbnailImage")
				SearchResultDict.updateValue(fullImage, forKey: "FullImage")
				
				DetailResult.append(SearchResultDict)
			}
		}
		return DetailResult
	}
	
}
