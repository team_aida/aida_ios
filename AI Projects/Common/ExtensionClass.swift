//
//  ExtensionClass.swift
//  AR_AI_Demo
//
//  Created by offshore_mac_1 on 04/02/19.
//  Copyright © 2019 offshore_mac_1. All rights reserved.
//

import UIKit
import Foundation
import QuartzCore


class ActionButton : UIButton {
	
	override func awakeFromNib() {
		
		super.awakeFromNib()
		
	}
	
	@IBInspectable
	public var screenFont : Bool = false {
		didSet {
			if screenFont {
				let pSize = (self.titleLabel?.font.pointSize)!/568//self.font.pointSize/667
				self.titleLabel?.font = (self.titleLabel?.font.withSize(MAINSCREEN_HEIGHT * pSize))!
			}
		}
	}
	
	@IBInspectable
	public var ApplyBorder : Bool = false {
		didSet {
			if ApplyBorder {
				self.layer.borderWidth = 0.5
				self.layer.borderColor = UIColor.lightGray.cgColor
			}
		}
	}
	
	@IBInspectable
	public var ApplyInsets : Bool = false {
		
		didSet {
			if ApplyInsets {
				self.contentEdgeInsets = UIEdgeInsets(top: 0, left: MAINSCREEN_WIDTH * 0.06, bottom: 0, right: 0)
			}
		}
	}
	
	
	@IBInspectable
	public var ApplyImageInsets : Bool = false {
		
		didSet {
			if ApplyImageInsets {
				self.imageView?.contentMode = .scaleAspectFit
				self.contentEdgeInsets = UIEdgeInsets(top: 0, left: self.frame.size.width * 0.2, bottom: 0, right: self.frame.size.width * 0.2)
			}
		}
	}
	
	
}

//MARK: - Text Field Class
class TextField: UITextField {
	
	
	let padding = UIEdgeInsets(top: 0, left: MAINSCREEN_WIDTH * 0.05, bottom: 0, right: 20)
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
	override open func leftViewRect(forBounds bounds: CGRect) -> CGRect {
		return bounds.inset(by: padding)
	}
	
	override open func textRect(forBounds bounds: CGRect) -> CGRect {
		return bounds.inset(by: padding)
	}
	
	override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
		return bounds.inset(by: padding)
	}
	
	override open func editingRect(forBounds bounds: CGRect) -> CGRect {
		return bounds.inset(by: padding)
	}
}

class TextView : UITextView {
	
	let pad = MAINSCREEN_WIDTH * 0.025
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.textContainerInset = UIEdgeInsets(top: pad, left: pad, bottom: 5, right: pad)
	}
}
