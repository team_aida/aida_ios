//
//  PredictModelController.swift
//  AI Model Training
//
//  Created by Rajeev Lochan Ranga on 05/03/19.
//  Copyright © 2019 Offshore. All rights reserved.
//

import UIKit

class PredictModelController: BaseController {
    
    var ModelPredictData = [[String:Any]]()

    @IBOutlet weak var MainView: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var TryAgainButton: UIButton!
    @IBOutlet weak var InfoView: UIView!
    @IBOutlet weak var DescriptionLabel: UILabel!
    @IBOutlet weak var TrainModelButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.definesPresentationContext = true
    }
    
//    func showData () {
//        for productData in ModelPredictData {
//            PredictModelImage.sd_setImage(with: URL(string: "\(MODEL_BASE_URL)\(productData["img_path"] ?? "")"), placeholderImage: UIImage(named: ""))
//            PredictModelName.text = productData["name"] as? String
//        }
//    }
    
    override func viewDidLayoutSubviews() {
        self.MainView.layer.cornerRadius = 12
        
    }
    
    @IBAction func action_TryAgain(_ sender: UIButton) {
        DismissPresentedView()
    }
    
	@IBAction func action_TrainModel(_ sender: UIButton) {
		
		DismissPresentedView()
		NotificationCenter.default.post(name: Notification.Name("OpenModelTraining"), object: nil)
		
	}
    
	@IBAction func swipeDown_action(_ sender: UISwipeGestureRecognizer) {
		DismissPresentedView()
		
	}
	@IBAction func action_dismissView(_ sender: UIButton) {
		
		DismissPresentedView()
    }
	
	func DismissPresentedView () {
		self.dismiss(animated: true, completion: nil)
		NotificationCenter.default.post(name: Notification.Name("RemovePredictItemsButtons"), object: nil)
        NotificationCenter.default.post(name: Notification.Name("StartObjectDetection"), object: nil)

        
	}
    
}
extension PredictModelController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ModelPredictData.count

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PredictProductTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PredictProductTableViewCell") as! PredictProductTableViewCell
        
        let productData = ModelPredictData[indexPath.row]
        if let img_path = productData["img_path"] as? String {
            cell.PredictModelImage.sd_setImage(with: URL(string: img_path), placeholderImage: UIImage(named: ""))
        }
       // cell.PredictModelImage.sd_setImage(with: URL(string: "\(MODEL_BASE_URL)\(productData["img_path"] ?? "")"), placeholderImage: UIImage(named: ""))
        cell.PredictModelImage.layer.cornerRadius = cell.PredictModelImage.frame.size.width / 2
         cell.PredictModelImage.clipsToBounds = true
        cell.PredictModelName.text = productData["name"] as? String
        return cell
    }
}


