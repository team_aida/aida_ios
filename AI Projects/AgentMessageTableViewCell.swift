//
//  AgentMessageTableViewCell.swift
//  AI Projects
//
//  Created by Rajeev Lochan Ranga on 22/03/19.
//  Copyright © 2019 Rajeev Lochan Ranga. All rights reserved.
//

import UIKit

class AgentMessageTableViewCell: UITableViewCell {

    @IBOutlet weak var chatContainerView: UIView!
    @IBOutlet weak var lbl_message: UILabel!
    @IBOutlet weak var lbl_timeStamp: UILabel!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var lbl_MessageDate: UILabel!
    @IBOutlet weak var dateView_heightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
